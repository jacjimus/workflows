INSTALLATION GUIDE.
(Please note anything that we need set/changed on production mode)

1. Change the constant YII_DEBUG to false on production at /bootstrap.php
2. Change the "hostInfo" and "baseUrl" at /app/config/console.php: This will enable console apps create valid urls. 

```
#!php
.....
'request' => array(
            'hostInfo' => 'http://localhost',
            'baseUrl' => '/chimesgreen',
            'scriptUrl' => 'index.php',
        ),
....
```
3.If PHP is not installed in the default path or you want to use a different version of PHP for console apps then change the PHP_COMMAND at /app/config/params.php:
```
#!php

return array(
    'PHP_COMMAND' => 'php',
);
```