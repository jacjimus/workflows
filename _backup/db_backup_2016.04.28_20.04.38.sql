-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `delivery`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_id` int(11) unsigned DEFAULT NULL,
  `notification_id` int(11) unsigned NOT NULL,
  `priority` int(11) unsigned NOT NULL,
  `from_credentials` text,
  `from_credentials_hash` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `delivery_attachment`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery_attachment`;
CREATE TABLE IF NOT EXISTS `delivery_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `delivery_recipient`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery_recipient`;
CREATE TABLE IF NOT EXISTS `delivery_recipient` (
  `delivery_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `channel` int(11) unsigned NOT NULL DEFAULT '1',
  `send_time` timestamp NULL DEFAULT NULL,
  `credentials` text,
  `credentials_hash` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_id`,`credentials_hash`,`channel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `delivery_reject`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery_reject`;
CREATE TABLE IF NOT EXISTS `delivery_reject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_address` varchar(255) DEFAULT NULL,
  `channel_type` smallint(5) unsigned NOT NULL,
  `notification_type` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_address_index` (`recipient_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `doc`
-- -------------------------------------------
DROP TABLE IF EXISTS `doc`;
CREATE TABLE IF NOT EXISTS `doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `doc_type_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `doc_file` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type_id` (`doc_type_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `doc_types`
-- -------------------------------------------
DROP TABLE IF EXISTS `doc_types`;
CREATE TABLE IF NOT EXISTS `doc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

