-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `claims_claims_details`
-- -------------------------------------------
DROP TABLE IF EXISTS `claims_claims_details`;
CREATE TABLE IF NOT EXISTS `claims_claims_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_header_id` int(11) DEFAULT NULL,
  `claim_type_id` int(10) unsigned DEFAULT NULL,
  `file_name` varchar(246) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `qty` decimal(18,2) NOT NULL,
  `unitprice` decimal(18,2) NOT NULL,
  `total_amount` decimal(18,2) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `claims_claims_header`
-- -------------------------------------------
DROP TABLE IF EXISTS `claims_claims_header`;
CREATE TABLE IF NOT EXISTS `claims_claims_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `project_claim` tinyint(11) NOT NULL DEFAULT '0',
  `amount` decimal(18,2) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `claim_date_from` date NOT NULL,
  `claim_date_to` date NOT NULL,
  `comments` text,
  `submit` tinyint(4) NOT NULL DEFAULT '0',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `claims_expense_types`
-- -------------------------------------------
DROP TABLE IF EXISTS `claims_expense_types`;
CREATE TABLE IF NOT EXISTS `claims_expense_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_assignment_deliverables`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_assignment_deliverables`;
CREATE TABLE IF NOT EXISTS `con_asc_assignment_deliverables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_consultant_specializations`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_consultant_specializations`;
CREATE TABLE IF NOT EXISTS `con_asc_consultant_specializations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consultant_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_contract_prerequisites`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_contract_prerequisites`;
CREATE TABLE IF NOT EXISTS `con_asc_contract_prerequisites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_contract_roles`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_contract_roles`;
CREATE TABLE IF NOT EXISTS `con_asc_contract_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_contracts`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_contracts`;
CREATE TABLE IF NOT EXISTS `con_asc_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `payment_mode` int(11) DEFAULT NULL,
  `withholding_tax` int(1) DEFAULT '0',
  `tax_percent` double DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_invoices`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_invoices`;
CREATE TABLE IF NOT EXISTS `con_asc_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_assignment_id` int(11) NOT NULL,
  `invoice_no` varchar(32) NOT NULL,
  `invoice_date` date NOT NULL,
  `no_of_days` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_job_assignments`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_job_assignments`;
CREATE TABLE IF NOT EXISTS `con_asc_job_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consultant_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_type_id` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `invoice_status` int(1) NOT NULL DEFAULT '0',
  `payment_status` int(1) NOT NULL DEFAULT '0',
  `contract_status` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_job_reports`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_job_reports`;
CREATE TABLE IF NOT EXISTS `con_asc_job_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_assignment_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_job_types`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_job_types`;
CREATE TABLE IF NOT EXISTS `con_asc_job_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `pay_per_day` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_jobs`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_jobs`;
CREATE TABLE IF NOT EXISTS `con_asc_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_initiated` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `supervisor_id` int(11) NOT NULL,
  `location` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_asc_payments`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_asc_payments`;
CREATE TABLE IF NOT EXISTS `con_asc_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `payment_mode` int(11) NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `ref_no` varchar(20) NOT NULL,
  `payment_date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_assignment_reports`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_assignment_reports`;
CREATE TABLE IF NOT EXISTS `con_assignment_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_engagement_tasks`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_engagement_tasks`;
CREATE TABLE IF NOT EXISTS `con_engagement_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engagement_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_engagements`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_engagements`;
CREATE TABLE IF NOT EXISTS `con_engagements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opportunity_id` int(11) NOT NULL,
  `job_title` varchar(128) NOT NULL,
  `start_date` date NOT NULL,
  `expected_end_date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `end_date` date NOT NULL,
  `description` text NOT NULL,
  `amount_payable` decimal(18,4) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `assigned` int(1) NOT NULL DEFAULT '0',
  `invoiced` int(11) NOT NULL DEFAULT '0',
  `paid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_invoices`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_invoices`;
CREATE TABLE IF NOT EXISTS `con_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engagement_id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `amount` decimal(18,4) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_opportunities`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_opportunities`;
CREATE TABLE IF NOT EXISTS `con_opportunities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(128) NOT NULL,
  `job_location` varchar(64) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `deadline_date` date NOT NULL,
  `posted_by_id` int(11) NOT NULL,
  `source` varchar(30) NOT NULL,
  `source_description` text NOT NULL,
  `proposal` varchar(50) DEFAULT NULL,
  `contact_person` varchar(128) NOT NULL,
  `contact_email` varchar(128) NOT NULL,
  `contact_phone` varchar(20) NOT NULL,
  `person_following_up_id` int(11) NOT NULL,
  `responded_to` int(1) NOT NULL DEFAULT '0',
  `response_successful` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_opportunity_responses`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_opportunity_responses`;
CREATE TABLE IF NOT EXISTS `con_opportunity_responses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opportunity_id` int(11) NOT NULL,
  `response_by_id` int(11) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `response_successful` int(1) NOT NULL DEFAULT '0',
  `response_date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_payments`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_payments`;
CREATE TABLE IF NOT EXISTS `con_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `receipt_no` varchar(50) DEFAULT NULL,
  `amount` decimal(18,4) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `ref_no` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `con_task_assinments`
-- -------------------------------------------
DROP TABLE IF EXISTS `con_task_assinments`;
CREATE TABLE IF NOT EXISTS `con_task_assinments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `engagement_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `tasks` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `delivery`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_id` int(11) unsigned DEFAULT NULL,
  `notification_id` int(11) unsigned NOT NULL,
  `priority` int(11) unsigned NOT NULL,
  `from_credentials` text,
  `from_credentials_hash` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `delivery_attachment`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery_attachment`;
CREATE TABLE IF NOT EXISTS `delivery_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `delivery_recipient`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery_recipient`;
CREATE TABLE IF NOT EXISTS `delivery_recipient` (
  `delivery_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `channel` int(11) unsigned NOT NULL DEFAULT '1',
  `send_time` timestamp NULL DEFAULT NULL,
  `credentials` text,
  `credentials_hash` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_id`,`credentials_hash`,`channel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `delivery_reject`
-- -------------------------------------------
DROP TABLE IF EXISTS `delivery_reject`;
CREATE TABLE IF NOT EXISTS `delivery_reject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_address` varchar(255) DEFAULT NULL,
  `channel_type` smallint(5) unsigned NOT NULL,
  `notification_type` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_address_index` (`recipient_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `doc`
-- -------------------------------------------
DROP TABLE IF EXISTS `doc`;
CREATE TABLE IF NOT EXISTS `doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `doc_type_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `doc_file` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type_id` (`doc_type_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `doc_types`
-- -------------------------------------------
DROP TABLE IF EXISTS `doc_types`;
CREATE TABLE IF NOT EXISTS `doc_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

