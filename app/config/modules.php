<?php

/**
 * stores the modules configurations
 * @author Fredrick <mconyango@gmail.com>
 */
return array(
    'gii' => array(
        'class' => 'system.gii.GiiModule',
        'password' => 'root',
        'ipFilters' => array('127.0.0.1', '::1'),
    ),
    'manager',
    'reports',
    'backup',
    'members',
    'admin',
    'settings',
    'users',
    'msg',
    'help',
    'employees',
    'timesheets',
    'payroll',
    'hr',
    'projects',
    'auth',
    'workflow',
    'proc',
    'inv',
    'me',
    'asset',
    'fleet',
    'cons',
    'notif',
    'req',
    'doc',
    'performance',
    'leave',
    'rec',
    'claims',
    'advance',
    'calendar',
    'consultancy',
    'payment',
    'procure',
    'tenders',
    
);
