<?php

/**
 * stores all the site-wide configurations
 * @author Fredrick <mconyango@gmail.com>
 * @version 1.0
 */
// Yii::setPathOfAlias('local','path/to/local-folder');
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Embassy of the Federal Republic of Somalia',
    'theme' => 'smartadmin',
    'language' => 'en',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => require(dirname(__FILE__) . '/import.php'),
    //available modules
    'modules' => require(dirname(__FILE__) . '/modules.php'),
    'defaultController' => 'default',
    // application components
    'components' => array(
        'clientScript' => array(
            'class' => 'CClientScript',
            'scriptMap' => array(
                'jquery.js' => false,
                'jquery.min.js' => false,
            ),
            'coreScriptPosition' => CClientScript::POS_END,
        ),
        'cache' => array(
            'class' => 'system.caching.CFileCache',
            'enabled' => TRUE,
        ),
        'pdfFactory'=>array(
            'class'=>'ext.pdffactory.EPdfFactory',
            
 
            'tcpdfPath'=>'ext.tcpdf.classes', //=default: the path to the tcpdf library
            'fpdiPath'=>'ext.pdffactory.vendors.fpdi', //=default: the path to the fpdi library
 
            //the cache duration
            'cacheHours'=>-1, //-1 = cache disabled, 0 = never expires, hours if >0
 
             //The alias path to the directory, where the pdf files should be created
            'pdfPath'=>'application.runtime.pdf',
 
            //The alias path to the *.pdf template files
            //'templatesPath'=>'application.pdf.templates', //= default
 
            //the params for the constructor of the TCPDF class  
            // see: http://www.tcpdf.org/doc/code/classTCPDF.html 
            'tcpdfOptions'=>array(
                  /* default values
                    'format'=>'A4',
                    'orientation'=>'P', //=Portrait or 'L' = landscape
                    'unit'=>'mm', //measure unit: mm, cm, inch, or point
                    'unicode'=>true,
                    'encoding'=>'UTF-8',
                    'diskcache'=>false,
                    'pdfa'=>false,
                   */
            )
        ),
        'swSource' => array(
            'class' => 'application.extensions.simpleWorkflow.SWPhpWorkflowSource',
        ),
        'settings' => array(
            'class' => 'CmsSettings',
            'cacheComponentId' => 'cache',
            'cacheId' => 'global_website_settings',
            'cacheTime' => 84000,
            'tableName' => '{{settings}}',
            'dbComponentId' => 'db',
            'createTable' => false,
            'dbEngine' => 'InnoDB',
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendors.mpdf.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class' => 'mpdf',
                ))),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('auth/default/login'),
            'autoRenewCookie' => false,
            'authTimeout' => 31557600,
        ),
        'urlManager' => array(
            'class' => 'UrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => true,
            'urlSuffix' => '', //md5('chimes132!!!*&%'),
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'
                => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'
                => '<controller>/<action>',
            ),
        ),
        //database settings
        'db' => require(dirname(__FILE__) . '/db.php'),
        'errorHandler' => array(
            // use 'error/index' action to display errors
            'errorAction' => 'error/index',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => YII_DEBUG ? 'error,info' : 'error, warning',
                ),
                array(
                    'class' => 'CWebLogRoute',
                    'enabled' => FALSE,
                ),
            ),
        ),
        'session' => array(
            'class' => 'CCacheHttpSession',
        ),
        'request' => array(
            'enableCsrfValidation' => false,
        ),
        'easyImage' => array(
            'class' => 'application.extensions.easyimage.EasyImage',
            'driver' => 'GD',
            'quality' => 100,
            'cachePath' => '/assets/easyimage/',
            'cacheTime' => 2592000,
            'retinaSupport' => false,
        ),
        'localtime' => array(
            'class' => 'application.modules.settings.components.LocalTime',
        ),
        'mailer' => require(dirname(__FILE__) . '/email.php'),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => require(dirname(__FILE__) . '/params.php'),
);
