<?php

/**
 * Description of RenderHeader
 *
 * @author Fred <mconyango@gmail.com>
 */
class LineItemHead extends CWidget {

    /**
     *
     * @var type
     */
    public $model;

    /**
     *
     * @var type
     */
    public $content;

    /**
     *
     * @var type
     */
    public $itemsTableHtml;

    /**
     *
     * @var type
     */
    public $itemsTableHtmlOptions = array('class' => 'table table-bordered');

    /**
     *
     * @var type
     */
    public $formContainer;

    /**
     *
     * @var type
     */
    public $cancelUrl;

    /**
     *
     * @var type
     */
    public $finishUrl;

    /**
     *
     * @var type
     */
    public $makeorderUrl;

    /**
     *
     * @var type
     */
    public $addRowUrl;

    /**
     *
     * @var type
     */
    public $itemsTitle;

    /**
     *
     * @var type
     */
    public $formHtmlOptions = array('class' => 'form-horizontal', 'role' => 'form');

    /**
     *
     * @var type
     */
    public $autoAddNewRowOnSave = true;

    /**
     *
     * @var type
     */
    public $contentWrapperHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $primaryKeyField = 'id';

    /**
     *
     * @var type
     */
    public $template = '{{notif}}{{content}}{{buttons}}';

    /**
     *
     * @var type
     */
    public $notifHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $showPanel = true;

    /**
     *
     * @var type
     */
    public $showHead = true;

    /**
     *
     * @var type
     */
    public $showButtons = true;

    /**
     *
     * @var type
     */
    public $itemsTemplate;

    /**
     *
     * @var type
     */
    public $itemsContainerHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $newRowLinkHtmlOptions = array('class' => '');

    /**
     *
     * @var type
     */
    public $addNewRowLabel;

    /**
     *
     * @var type
     */
    public $autoAddHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $autoAddLabel;

    /**
     *
     * @var type
     */
    public $buttonsWrapperHtmlOptions = array('class' => 'well well-sm clearfix');

    /**
     *
     * @var type
     */
    public $buttonsTemplate;

    /**
     *
     * @var type
     */
    public $finishButtonHtmlOptions = array('class' => 'btn btn-primary');

    /**
     *
     * @var type
     */
    public $makeorderButtonHtmlOptions = array('class' => 'btn btn-primary');

    /**
     *
     * @var type
     */
    public $finishButtonLabel;

    /**
     *
     * @var type
     */
    public $makeorderButtonLabel;

    /**
     *
     * @var type
     */
    public $cancelButtonHtmlOptions = array('class' => 'btn btn-default');

    /**
     *
     * @var type
     */
    public $cancelButtonLabel;

    /**
     *
     * @var type
     */
    private $model_class_name;

    /**
     *
     * @var type
     */
    public $beforeFinish;

    /**
     *
     * @var type
     */
    public $beforeMakeorder;

    /**
     *
     * @var type
     */
    public $afterFinish;

    /**
     *
     * @var type
     */
    public $afterMakeorder;

    public function init() {
        if (empty($this->formContainer))
            $this->formContainer = 'line_item_form_wrapper';

        if (empty($this->formHtmlOptions['id']))
            $this->formHtmlOptions['id'] = 'line_items_form';
        $this->formHtmlOptions['method'] = 'post';
        if (!empty($this->model))
            $this->model_class_name = get_class($this->model);
        parent::init();
    }

    public function run() {
        $inner_html = $this->processTemplate();
        if (empty($this->formHtmlOptions['action']))
            $this->formHtmlOptions['action'] = $this->finishUrl;
        echo CHtml::tag('form', $this->formHtmlOptions, $inner_html);
        $this->registerScripts();
    }

    protected function processTemplate() {
        if (empty($this->template))
            $this->template = '{{notif}}{{content}}{{buttons}}';
        $html = strtr($this->template, array(
            '{{notif}}' => $this->getNotifHtml(),
            '{{content}}' => $this->getContentHtml(),
            '{{buttons}}' => ($this->showButtons) ? $this->getButtonsHtml() : '',
        ));
        return $html;
    }

    protected function getContentHtml() {
        if (empty($this->content))
            $this->content = '{{items}}';
        $content = strtr($this->content, array(
            '{{items}}' => $this->getItemsHtml(),
        ));
        $hidden_fields = $this->getHeadHiddenFieldHtml();
        $inner_html = $content . $hidden_fields;
        return CHtml::tag('div', $this->contentWrapperHtmlOptions, $inner_html);
    }

    protected function getButtonsHtml() {
        if (empty($this->model))
            return "";

        if (null === $this->buttonsTemplate) {
            $this->buttonsTemplate = '<div class="pull-right">{{finish}}&nbsp;&nbsp;{{makeorder}}&nbsp;&nbsp;{{cancel}}</div>';
        }
        $this->finishButtonHtmlOptions['type'] = 'button';
        if (empty($this->finishButtonHtmlOptions['id']))
            $this->finishButtonHtmlOptions['id'] = 'finish_line_item_button';
        if (empty($this->finishUrl))
            $this->finishUrl = $this->getOwner()->createUrl('finish');
        $this->finishButtonHtmlOptions['data-ajax-url'] = $this->finishUrl;
        if (empty($this->finishButtonLabel))
            $this->finishButtonLabel = Lang::t('Submit for Approval');


        $this->makeorderButtonHtmlOptions['type'] = 'button';
        if (empty($this->makeorderButtonHtmlOptions['id']))
            $this->makeorderButtonHtmlOptions['id'] = 'makeorder_line_item_button';
        if (empty($this->makeorderUrl))
            $this->makeorderUrl = $this->getOwner()->createUrl('makeorder');
        $this->makeorderButtonHtmlOptions['data-ajax-url'] = $this->makeorderUrl;
        if (empty($this->makeorderButtonLabel))
            $this->makeorderButtonLabel = Lang::t('Make Order');


        $this->cancelButtonHtmlOptions['type'] = 'button';
        if (empty($this->cancelButtonHtmlOptions['id']))
            $this->cancelButtonHtmlOptions['id'] = 'cancel_line_item_button';
        if (empty($this->cancelUrl))
            $this->cancelUrl = $this->getOwner()->createUrl('cancel');
        $this->cancelButtonHtmlOptions['data-ajax-url'] = $this->cancelUrl;
        if (empty($this->cancelButtonLabel))
            $this->cancelButtonLabel = Lang::t('Cancel');


        $inner_html = strtr($this->buttonsTemplate, array(
            '{{finish}}' => CHtml::tag('button', $this->finishButtonHtmlOptions, $this->finishButtonLabel),
            '{{makeorder}}' => CHtml::tag('button', $this->makeorderButtonHtmlOptions, $this->makeorderButtonLabel),
            '{{cancel}}' => CHtml::tag('button', $this->cancelButtonHtmlOptions, $this->cancelButtonLabel),
        ));


        return CHtml::tag('div', $this->buttonsWrapperHtmlOptions, $inner_html);
    }

    protected function getHeadHiddenFieldHtml() {
        if (!empty($this->model)) {
            if (empty($this->primaryKeyField))
                $this->primaryKeyField = 'id';
            return CHtml::activeHiddenField($this->model, $this->primaryKeyField);
        } else
            return "";
    }

    protected function getItemsHtml() {
        $this->itemsContainerHtmlOptions['class'] = 'panel panel-default';
        if (null === $this->itemsTemplate && $this->showPanel) {
            $this->itemsTemplate = '<div class="panel-heading"><h3 class="panel-title">{{items_title}}</h3></div>'
                    . '{{items_table}}'
                    . '<div class="panel-footer clearfix"><ul class="list-inline" style="text-align:right;"><li>{{auto_add_checkbox}}</li><li>&nbsp;&nbsp;&nbsp;</li><li>{{new_row_link}}</li></ul></div>';
        }
        $auto_add_checkbox = $this->getAutoAddCheckBoxHtml();
        $new_row_link = $this->getNewRowLinkHtml();
        if (empty($this->itemsTableHtmlOptions['id']))
            $this->itemsTableHtmlOptions['id'] = 'line_items_table';
        $inner_html = strtr($this->itemsTemplate, array(
            '{{items_title}}' => $this->itemsTitle,
            '{{items_table}}' => CHtml::tag('table', $this->itemsTableHtmlOptions, $this->itemsTableHtml),
            '{{auto_add_checkbox}}' => $auto_add_checkbox,
            '{{new_row_link}}' => $new_row_link,
        ));
        return CHtml::tag('div', $this->itemsContainerHtmlOptions, $inner_html);
    }

    protected function getAutoAddCheckBoxHtml() {
        if (!$this->autoAddNewRowOnSave)
            return "";
        if (empty($this->autoAddLabel))
            $this->autoAddLabel = Lang::t('Auto add new row on save');
        $template = '{{checkbox}}&nbsp;&nbsp;<span class="text-muted">{{label}}</span>';
        if (empty($this->autoAddHtmlOptions['id']))
            $this->autoAddHtmlOptions['id'] = 'auto_add_new_row_on_save';
        $checkbox = CHtml::checkBox($this->autoAddHtmlOptions['id'], $this->autoAddNewRowOnSave, $this->autoAddHtmlOptions);
        return strtr($template, array(
            '{{checkbox}}' => $checkbox,
            '{{label}}' => $this->autoAddLabel,
        ));
    }

    protected function getNewRowLinkHtml() {
        if (empty($this->newRowLinkHtmlOptions['id']))
            $this->newRowLinkHtmlOptions['id'] = 'add_new_item_line';
        if (empty($this->addNewRowLabel))
            $this->addNewRowLabel = Lang::t('Add New Row') . ' <i class="fa fa-level-up"></i>';
        if (empty($this->addRowUrl))
            $this->addRowUrl = $this->getOwner()->createUrl('addRow');
        $this->newRowLinkHtmlOptions['data-ajax-url'] = $this->addRowUrl;

        return CHtml::link($this->addNewRowLabel, 'javascript:void(0);', $this->newRowLinkHtmlOptions);
    }

    protected function getNotifHtml() {
        if (empty($this->notifHtmlOptions['id']))
            $this->notifHtmlOptions['id'] = 'line_items_notif_wrapper';
        return CHtml::tag('div', $this->notifHtmlOptions, "");
    }

    protected function registerScripts() {

        $options = array(
            'formContainer' => $this->formContainer,
            'itemsTableCssId' => !empty($this->itemsTableHtmlOptions['id']) ? $this->itemsTableHtmlOptions['id'] : null,
            'headIdInputCssId' => !empty($this->model_class_name) ? $this->model_class_name . '_' . $this->primaryKeyField : null,
            'addNewRowSelector' => !empty($this->newRowLinkHtmlOptions['id']) ? '#' . $this->newRowLinkHtmlOptions['id'] : null,
            'cancelSelector' => !empty($this->cancelButtonHtmlOptions['id']) ? '#' . $this->cancelButtonHtmlOptions['id'] : null,
            'submitSelector' => !empty($this->finishButtonHtmlOptions['id']) ? '#' . $this->finishButtonHtmlOptions['id'] : null,
            'submitorderSelector' => !empty($this->makeorderButtonHtmlOptions['id']) ? '#' . $this->makeorderButtonHtmlOptions['id'] : null,
            'notifSelector' => !empty($this->notifHtmlOptions['id']) ? '#' . $this->notifHtmlOptions['id'] : null,
            'autoAddRowOnSaveSelector' => !empty($this->autoAddHtmlOptions['id']) ? '#' . $this->autoAddHtmlOptions['id'] : null,
        );
        foreach (array('beforeFinish', 'afterFinish', 'beforeMakeorder', 'afterMakeorder') as $event) {
            if ($this->$event !== null) {
                if ($this->$event instanceof CJavaScriptExpression)
                    $options[$event] = $this->$event;
                else
                    $options[$event] = new CJavaScriptExpression($this->$event);
            }
        }

        Yii::app()->clientScript->registerScript('LineItem.LineItemHead', "MyLineItem.Head.init(" . CJavaScript::encode($options) . ")");
    }

}
