<?php

/**
 * Smart DropDown extension
 *
 * @author Fred <mconyango@gmail.com>
 */
class SmartDropDown extends CWidget
{

    public $model;
    public $attribute;
    public $attributeHtmlOptions = array('class' => 'form-control');
    public $showAttributeLabel = true;
    public $attributeLabelHtmlOptions = array('class' => 'col-md-3 control-label');
    public $attributeWrapperHtmlOptions = array('class' => 'col-md-6');
    public $attributeWrapperTemplate;
    public $listData = array();
    public $template;
    //list item input
    public $listItemInputModel;
    public $listItemInputHtmlOptions = array('class' => 'form-control margin-top-10');
    public $listItemInputAttribute;
    private $listItemInputWrapperHtmlOptions = array();
    //list item link
    public $listItemLinkLabel = '<i class="fa fa-2x fa-plus-circle"></i>';
    public $listIitemLinkHtmlOptions = array('title' => 'Click to add new');
    public $listItemUrl;
    public $listItemLinkWrapperHtmlOptions = array('class' => 'col-md-1');
    public $listItemLinkTemplate;

    public function init()
    {
        if (empty($this->template)) {
            $this->template = '<div class="form-group">'
                    . '{{label}}'
                    . '{{attribute_wrapper}}'
                    . '{{list_item_link_wrapper}}'
                    . '</div>';
        }
        if (empty($this->attributeWrapperTemplate)) {
            $this->attributeWrapperTemplate = '{{select_list}}'
                    . '{{list_item_input}}';
        }
        $this->listItemInputWrapperHtmlOptions = array(
            'id' => get_class($this->listItemInputModel) . '_' . $this->listItemInputAttribute . '_wrapper',
            'style' => 'display:none;',
        );
        $this->listItemLinkWrapperHtmlOptions['id'] = get_class($this->listItemInputModel) . '_' . $this->listItemInputAttribute . '_create';
        if (empty($this->listItemLinkTemplate)) {
            $this->listItemLinkTemplate = '<p>{{link}}</p><p>{{spinner}}</p>';
        }

        $this->attributeHtmlOptions['id'] = get_class($this->model) . '_' . $this->attribute;
        $this->listItemInputHtmlOptions['id'] = get_class($this->listItemInputModel) . '_' . $this->listItemInputAttribute;
        parent::init();
    }

    public function run()
    {
        echo $this->processTemplate();
        $this->registerAssets();
    }

    protected function processTemplate()
    {
        return strtr($this->template, array(
            '{{label}}' => $this->generateLabelHtml(),
            '{{attribute_wrapper}}' => $this->generateAttributeWrapperHtml(),
            '{{list_item_link_wrapper}}' => $this->generateListItemCreateLink(),
        ));
    }

    protected function generateLabelHtml()
    {
        $label = "";
        if ($this->showAttributeLabel) {
            $label = CHtml::activeLabelEx($this->model, $this->attribute, $this->attributeLabelHtmlOptions);
        }
        return $label;
    }

    protected function generateAttributeWrapperHtml()
    {
        $select_list = CHtml::activeDropDownList($this->model, $this->attribute, $this->listData, $this->attributeHtmlOptions);
        $list_item_input = CHtml::activeTextField($this->listItemInputModel, $this->listItemInputAttribute, $this->listItemInputHtmlOptions);
        $error = '<p class="errorMessage" style="display:none;"></p>';
        $list_item_input_wrapper = CHtml::tag('div', $this->listItemInputWrapperHtmlOptions, $list_item_input . $error);
        $inner_hml = strtr($this->attributeWrapperTemplate, array(
            '{{select_list}}' => $select_list,
            '{{list_item_input}}' => $list_item_input_wrapper,
        ));
        return CHtml::tag('div', $this->attributeWrapperHtmlOptions, $inner_hml);
    }

    protected function generateListItemCreateLink()
    {
        $this->listIitemLinkHtmlOptions['data-ajax-url'] = $this->listItemUrl;
        $link = CHtml::link($this->listItemLinkLabel, 'javascript:void(0);', $this->listIitemLinkHtmlOptions);
        $spinner = '<i class="fa fa-2x fa-spinner fa-spin" style="display: none"></i>';
        $inner_html = strtr($this->listItemLinkTemplate, array(
            '{{link}}' => $link,
            '{{spinner}}' => $spinner,
        ));

        return CHtml::tag('div', $this->listItemLinkWrapperHtmlOptions, $inner_html);
    }

    protected function registerAssets()
    {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $assetUrl = Yii::app()->assetManager->publish($assets, false, -1, YII_DEBUG ? true : null);
        $cs = Yii::app()->clientScript
                ->registerScriptFile($assetUrl . '/smart-dropdown.js', CClientScript::POS_END);

        $options = array(
            'select_id' => $this->attributeHtmlOptions['id'],
            'link_wrapper_id' => $this->listItemLinkWrapperHtmlOptions['id'],
            'input_wrapper_id' => $this->listItemInputWrapperHtmlOptions['id'],
            'input_id' => $this->listItemInputHtmlOptions['id'],
        );
        $js = 'MySmartDropDown.init(' . CJavaScript::encode($options) . ');';
        $cs->registerScript('smartdropdown_' . $options['select_id'], $js);
    }

}
