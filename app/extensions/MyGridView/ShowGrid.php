<?php

/**
 * Renders all expects of a data grid
 * @author Fred <mconyango@gmail.com>
 */
class ShowGrid extends CWidget {

    /**
     * The cgridview properties
     * @var array
     */
    public $grid = array();
    private $gridHtml;

    /**
     * grid title
     * @var type
     */
    public $title;

    /**
     * Grid title icon
     * @var type
     */
    public $titleIcon = false;

    /**
     * Whether to show a search form
     * @var type
     */
    public $showSearch = true;

    /**
     * The search form html. If left NULL the default will be used
     * @var type
     */
    public $searchHtml = null;

    /**
     * Toolbar links
     * @var type
     */
    public $toolbarButtons = array();
    private $toolbarHtml;

    /**
     * Whether to show export button
     * @var type
     */
    public $showExportButton = false;
    private $exportButtonHtml;

    /**
     * Whether to show the refresh button
     * @var type
     */
    public $showRefreshButton = true;
    public $refreshButtonHtmlOptions = array('class' => 'btn btn-default');

    /**
     * The template for displaying the grid
     * @var type
     */
    public $template = null;

    /**
     * Create button options
     * @var type
     */
    public $createButton = array('visible' => true, 'label' => null, 'url' => null, 'htmlOptions' => array(), 'modal' => false);

    /**
     * Panel options
     * @var type
     */
    public $panel = array();

    /**
     *
     * @var type
     */
    public $exportRoute = null;

    /**
     *
     * @var type
     */
    public $exportActionParams = array();

    /**
     * Date range
     * @var type
     */
    public $showDataRange = false;
    public $dateRangeAttribute;
    public $dateRangeFormHtmlOptions = array('class' => 'form-inline',);
    public $dateRangeAttributeHtmlOptions = array('class' => 'form-control');
    public $dateRangeValue = null;
    public $dateRangeButtonLabel;
    public $dateRangeButtonHtmlOptions = array('class' => 'btn btn-default');
    public $dateRangeTemplate;
    private $dataRangeHtml;

    public function init() {
        $this->setDefaults();
        $this->registerAssets();
        parent::init();
    }

    public function run() {
        echo strtr($this->template, array(
            '{{type}}' => $this->panel['type'],
            '{{title}}' => $this->title,
            '{{date_range}}' => $this->dataRangeHtml,
            '{{search}}' => $this->searchHtml,
            '{{toolbar}}' => $this->toolbarHtml,
            '{{export}}' => $this->exportButtonHtml,
            '{{grid}}' => $this->gridHtml,
        ));
    }

    protected function setDefaults() {
        $this->setTitle();
        if ($this->showSearch && empty($this->searchHtml)) {
            $this->setSearchFormHtml();
        }
        $this->setPanel();
        $this->setCreateButton();
        $this->setToolbarHtml();
        $this->setGridHtml();
        $this->setExportButtonHtml();
        $this->setDateRangeHtml();
        $this->setTemplate();
    }

    protected function setSearchFormHtml() {
        $this->searchHtml = $this->getOwner()->widget('ext.activeSearch.AjaxSearch', array(
            'gridID' => $this->grid['id'],
            'model' => $this->grid['model'],
                ), true);
    }

    protected function setPanel() {
        $panel_type = ArrayHelper::getValue($this->panel, 'type', 'default');
        $this->panel['type'] = $panel_type;
    }

    protected function setCreateButton() {
        //create button
        $this->createButton['visible'] = ArrayHelper::getValue($this->createButton, 'visible', true);
        if ($this->createButton['visible']) {
            $create_button_url = ArrayHelper::getValue($this->createButton, 'url', $this->getOwner()->createUrl('create', $this->getOwner()->actionParams));
            $create_button_label = ArrayHelper::getValue($this->createButton, 'label', '<i class="fa fa-plus-circle"></i> ' . Lang::t(Constants::LABEL_CREATE . ' ' . $this->getOwner()->resourceLabel));
            $create_button_html_options = ArrayHelper::getValue($this->createButton, 'htmlOptions', array('class' => 'btn btn-success'));
            $create_button_modal = ArrayHelper::getValue($this->createButton, 'modal', false);
            if ($create_button_modal) {
                $new_class = 'show_modal_form';
                if (empty($create_button_html_options['class']))
                    $create_button_html_options['class'] = $new_class;
                else
                    $create_button_html_options['class'].=' ' . $new_class;
                $create_button_html_options['data-grid-id'] = $this->grid['id'];
            }
            $this->createButton = array('visible' => true, 'url' => $create_button_url, 'label' => $create_button_label, 'htmlOptions' => $create_button_html_options, 'modal' => $create_button_modal);
        }
    }

    protected function setTitle() {
        if (empty($this->title))
            $this->title = CHtml::encode($this->getOwner()->getPageTitle());
        if (!empty($this->titleIcon)) {
            $this->title = $this->titleIcon . '&nbsp;' . $this->title;
        }
    }

    protected function setExportButtonHtml() {
        if ($this->showExportButton) {
            $route = !empty($this->exportRoute) ? $this->exportRoute : $this->getOwner()->getRoute();
            $params = !empty($this->exportActionParams) ? $this->exportActionParams : $this->getOwner()->getActionParams();
            $this->exportButtonHtml = '<div class="btn-group">'
                    . '<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">'
                    . '<span>' . Lang::t('Export to') . '&nbsp;&nbsp;</span>'
                    . '<span class="caret"></span>'
                    . '</button>'
                    . '<ul class="dropdown-menu" role="menu">'
                    . '<li><a href="' . Yii::app()->createUrl($route, array_merge($params, array('exportType' => 'Excel2007', 'grid_mode' => 'export'))) . '">Excel (*.xlsx)</a></li>'
                    . '<li><a href="' . Yii::app()->createUrl($route, array_merge($params, array('exportType' => 'CSV', 'grid_mode' => 'export'))) . '">CSV (*.csv)</a></li>'
                    . '<li><a href="' . Yii::app()->createUrl($route, array_merge($params, array('exportType' => 'HTML', 'grid_mode' => 'export'))) . '">HTML (*.html)</a></li>'
                    . '<li><a class="print-mygrid" href="javascript:void(0)" data-grid-id="' . $this->grid['id'] . '">Print <i class="fa fa-print"></i></a></li>'
                    . '</ul>'
                    . ' </div>';
        }
    }

    protected function setGridHtml() {
        $this->grid['panel'] = $this->panel;
        $this->gridHtml = $this->getOwner()->widget('ext.MyGridView.MyGridView', $this->grid, true);
    }

    protected function setToolbarHtml() {
        if ($this->createButton['visible']) {
            $this->toolbarHtml = CHtml::tag('div', array('class' => 'btn-group'), CHtml::link($this->createButton['label'], $this->createButton['url'], $this->createButton['htmlOptions']));
        }
        if ($this->showRefreshButton) {
            $new_class = 'refresh-grid';
            if (empty($this->refreshButtonHtmlOptions['class']))
                $this->refreshButtonHtmlOptions['class'] = $new_class;
            else
                $this->refreshButtonHtmlOptions['class'].=' ' . $new_class;
            $this->refreshButtonHtmlOptions['data-grid-id'] = $this->grid['id'];
            $this->toolbarHtml.=CHtml::tag('div', array('class' => 'btn-group'), CHtml::link('<i class="fa fa-refresh"></i> ' . Lang::t('Refresh'), 'javascript:void(0)', $this->refreshButtonHtmlOptions));
        }
        if (!empty($this->toolbarButtons)) {
            foreach ($this->toolbarButtons as $button) {
                $this->toolbarHtml .= CHtml::tag('div', array('class' => 'btn-group'), $button);
            }
        }
    }

    protected function setDateRangeHtml() {
        if ($this->showDataRange) {
            if (empty($this->dateRangeTemplate)) {
                $this->dateRangeTemplate = '<div class="input-group">'
                        . '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>'
                        . '{{input}}<span class="input-group-btn">{{button}}</span></div>';
            }
            if (!isset($this->dateRangeAttributeHtmlOptions['method'])) {
                $this->dateRangeFormHtmlOptions['method'] = 'GET';
            }
            if (empty($this->dateRangeAttribute)) {
                $this->dateRangeAttribute = BsDateRangePicker::DATE_RANGE_GET_PARAM;
            }
            if ($value = filter_input(INPUT_GET, $this->dateRangeAttribute)) {
                $this->dateRangeValue = $value;
            }
            if (empty($this->dateRangeValue)) {
                $this->dateRangeValue = BsDateRangePicker::getDateRange('Y/m/d', 12);
            }

            $inputs = '';
            foreach ($this->getOwner()->getActionParams() as $k => $v) {
                if ($k !== $this->dateRangeAttribute && !is_array($v))
                    $inputs .=CHtml::hiddenField($k, $v);
            }
            $inputs .= CHtml::textField($this->dateRangeAttribute, $this->dateRangeValue, $this->dateRangeAttributeHtmlOptions);
            if (empty($this->dateRangeButtonLabel))
                $this->dateRangeButtonLabel = Lang::t('Go');
            if (empty($this->dateRangeButtonHtmlOptions['type']))
                $this->dateRangeButtonHtmlOptions['type'] = 'submit';
            $button = CHtml::tag('button', $this->dateRangeButtonHtmlOptions, $this->dateRangeButtonLabel);

            $innerHtml = strtr($this->dateRangeTemplate, array(
                '{{input}}' => $inputs,
                '{{button}}' => $button,
            ));
            $this->dateRangeFormHtmlOptions['action'] = Yii::app()->createUrl($this->getOwner()->getRoute(), $this->getOwner()->getActionParams());
            $this->dataRangeHtml = CHtml::tag('form', $this->dateRangeFormHtmlOptions, $innerHtml);
        }
    }

    protected function setTemplate() {
        if (empty($this->template)) {
            $this->template = '<div class="panel panel-{{type}}">'
                    . '<div class="panel-heading">'
                    . '<h3 class="panel-title">{{title}}</h3>'
                    . '</div>';
            if (!empty($this->toolbarButtons) || $this->showSearch || $this->showRefreshButton || $this->showExportButton || $this->createButton['visible'] || $this->showDataRange) {
                if ($this->showDataRange) {
                    $this->template.='<div class="kv-panel-before">'
                            . '<div class="row">'
                            . '<div class="col-md-6">'
                            . '<div class="row">'
                            . '<div class="col-md-6">{{date_range}}</div><div class="col-md-6">{{search}}</div>'
                            . '</div>'
                            . '</div>'
                            . '<div class="col-md-6"><div class="pull-right">{{toolbar}} {{export}}</div></div>'
                            . '</div>'
                            . '</div>';
                } else {
                    $this->template.='<div class="kv-panel-before">'
                            . '<div class="row">'
                            . '<div class="col-md-4">'
                            . '{{search}}'
                            . '</div>'
                            . '<div class="col-md-8"><div class="pull-right">{{toolbar}} {{export}}</div></div>'
                            . '</div>'
                            . '</div>';
                }
            }
            $this->template.='{{grid}}'
                    . '</div>';
        }
    }

    protected function registerAssets() {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $assets_url = Yii::app()->assetManager->publish($assets);
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($assets_url . '/js/custom.js', CClientScript::POS_END);
        if ($this->showDataRange) {
            $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/bootstrap-daterangepicker/moment.min.js', CClientScript::POS_END)
                    ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/bootstrap-daterangepicker/daterangepicker.min.js', CClientScript::POS_END)
                    ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/bootstrap-daterangepicker/daterangepicker-bs3.min.css');
        }
        $options = array(
            'grid_id' => $this->grid['id'],
            'showDateRange' => $this->showDataRange,
            'dateRangeId' => $this->dateRangeAttribute,
        );
        $js = 'MyGridView.init(' . CJSON::encode($options) . ');';
        $cs->registerScript($this->grid['id'] . '-show-grid', $js);
    }

}
