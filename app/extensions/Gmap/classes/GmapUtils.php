<?php

/**
 * Google map utility functions
 * @author Fred <mconyango@gmail.com>
 */
class GmapUtils
{

    const MAP_TYPE_ROAD = 'ROADMAP';
    const MAP_TYPE_TERRAIN = 'TERRAIN';
    const MAP_TYPE_SATELLITE = 'SATELLITE';
    const MAP_TYPE_HYBRID = 'HYBRID';

    public static function geoCode($address)
    {
        $base_url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false';
        $request_url = $base_url . "&address=" . urlencode($address);
        $response = self::sendRequest($request_url);
        if ($response['status'] != 'OK')
            return false;
        return $response['results'];
    }

    private static function sendRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseBody = CJSON::decode(curl_exec($ch));
        $request_info = curl_getinfo($ch);
        curl_close($ch);
        if ($request_info['http_code'] != 200)
            return false;
        return $responseBody;
    }

    public static function mapTypeOptions()
    {
        return array(
            self::MAP_TYPE_ROAD => self::MAP_TYPE_ROAD,
            self::MAP_TYPE_TERRAIN => self::MAP_TYPE_TERRAIN,
            self::MAP_TYPE_SATELLITE => self::MAP_TYPE_SATELLITE,
            self::MAP_TYPE_HYBRID => self::MAP_TYPE_HYBRID,
        );
    }

    public static function zoomOptions()
    {
        $options = array();
        for ($i = 4; $i <= 24; $i++) {
            $options[$i] = $i;
        }
        return $options;
    }

}
