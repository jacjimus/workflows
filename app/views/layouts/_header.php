<?php
$logo = null;
switch ($this->settings[SettingsModuleConstants::SETTINGS_THEME]) {
    case GeneralSettings::THEME2:
    case GeneralSettings::THEME3:
        $logo = 'logo2.png';
        break;
    case GeneralSettings::THEME4:
        $logo = 'logo3.png';
        break;
    default :
        $logo = 'logo1.png';
}
?>
<!-- HEADER -->
<header id="header">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"> 
    <div id="logo-group">
        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"><img src="<?php echo Yii::app()->theme->baseUrl . '/img/' . $logo ?>" alt="<?php //echo $this->settings[SettingsModuleConstants::SETTINGS_APP_NAME] ?>"></span>
        <!--span id="logo"><img src="<?php echo Yii::app()->theme->baseUrl . '/img/rwandalogo.png'?>" alt="<?php echo $this->settings[SettingsModuleConstants::SETTINGS_APP_NAME] ?>"></span-->
        <!--<span id="logo" class="logo-text"><?php //echo $this->settings[SettingsModuleConstants::SETTINGS_APP_NAME] ?></span>-->
<!--        <span id="logo" class="logo-text">COINSPENCE Peers</span>-->
       <!--<span id="logo"><img src="img/logo.png" alt="SmartAdmin"> </span>-->
        <!-- END LOGO PLACEHOLDER -->
        <?php $this->renderPartial('notif.views.layouts.notif') ?>
    </div>
    <!-- pulled right: nav area -->
    <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo Yii::app()->createUrl('auth/default/logout') ?>" title="<?php echo Lang::t('Sign Out') ?>"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->

        <!-- input: search field -->
        <form action="#search.html" class="header-search pull-right form-inline">
            <input type="text" placeholder="Search ..." id="search-fld">
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
            <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
        </form>
        <!-- end input: search field -->
    </div>
    <!-- end pulled right: nav area -->
</header>
<!-- END HEADER -->
