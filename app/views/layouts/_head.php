<head>
    <meta charset="utf-8">
    <title><?php echo $this->settings[SettingsModuleConstants::SETTINGS_APP_NAME]; ?> - <?php echo CHtml::encode($this->pageTitle) ?></title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <!-- FAVICONS -->
    <!--  <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">-->
    <!--  <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">-->

    <!-- GOOGLE FONT -->
    <!--<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">-->
    <!-- theme settings js -->
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/theme-settings.js"></script>
    <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo Yii::app()->theme->baseUrl ?>/js/plugin/pace/pace.min.js"></script>

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <?php if (!YII_DEBUG): ?>
            <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
    <?php endif; ?>
    <script>
        if (!window.jQuery) {
            document.write('<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/libs/jquery-2.0.2.min.js"><\/script>');
        }
    </script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/libs/jquery.cookie.min.js"></script>
    <?php if (!YII_DEBUG): ?>
                                        <!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->
    <?php endif; ?>
    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
        }
    </script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/plugin/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/my-utils.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/js/my-modal-form.js"></script>
</head>