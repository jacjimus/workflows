<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">
    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as it -->
            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <?php echo CHtml::image(PersonImages::model()->getProfileImageUrl(Yii::app()->user->id, 48, 48), Lang::t('Me'), array('class' => 'online')); ?>
                <span><?php echo CHtml::encode(Yii::app()->user->name) ?></span>
                <i class="fa fa-angle-down"></i>
            </a>
        </span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>
            <!-- NOTE: Notice the gaps after each icon usage <i></i>..
            Please note that these links work a bit different than
            traditional hre="" links. See documentation for details.
        -->
        <?php if(Users::model()->get(Yii::app()->user->id , 'user_to_change_password') == 1):?>
        
        <?php else: ?>
        <ul>
            <li>
                <a href="<?php echo Yii::app()->createUrl('default/index') ?>"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"><?php echo Lang::t('Dashboard') ?></span></a>
            </li>
            <?php $this->renderPartial('hr.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('tenders.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('manager.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('timesheets.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('projects.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('workflow.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('users.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('settings.views.layouts._sideLinks'); ?>
	   <?php $this->renderPartial('reports.views.layouts._sideLinks'); ?>
            <?php $this->renderPartial('backup.views.layouts._sideLinks'); ?>
        </ul>
        <?php endif; ?>
    </nav>
    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>
<!-- END NAVIGATION -->
