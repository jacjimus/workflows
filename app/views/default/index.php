<?php
$this->breadcrumbs = array(
    "Summary Reports"
);
?>
<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= 10?></h3>

              <p>Active Tenders</p>
            </div>
            <div class="icon">
              <i class="ion ion-compose"></i>
            </div>
            <?=CHtml::link("More info <i class='fa fa-arrow-circle-right'></i>", Yii::app()->urlManager->createUrl("manager/default/self") , ["class"=>"small-box-footer"]) ?>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?=  14?></h3>

              <p>Ongoing-Projects</p>
            </div>
            <div class="icon">
              <i class="ion ion-document-text"></i>
            </div>
            <?=CHtml::link("More info <i class='fa fa-arrow-circle-right'></i>", Yii::app()->urlManager->createUrl("manager/default/") , ["class"=>"small-box-footer"]) ?>
         
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?=5?></h3>

              <p>Todo list</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
             <?=
        $this->showLink(UsersModuleConstants::RES_USERS) ? CHtml::link("More info <i class='fa fa-arrow-circle-right'></i>", Yii::app()->urlManager->createUrl("manager/default/self") , ["class"=>"small-box-footer"]) : "" ?>
         
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Completed Tasks</p>
            </div>
            <div class="icon">
              <i class="ion ion-document-text"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="pull-left header"><i class="fa fa-briefcase"></i> Projects Summary</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 400px;">
<?php
$categ = [];
$categories = Yii::app()->db->createCommand()
        ->select('id, name')
        ->from(Doc::model()->tableName())
        ->order('name')
        ->queryAll();
foreach ($categories As $cat):
    $categ[$cat['id']] = $cat['name'];
endforeach;
// Get count for 
$uploaded_file_data = [];
$generated_file_data = [];
$total_file_data = [];
foreach (array_keys($categ) As $u_cat):
    $uploaded_file_data_categs = (int)Yii::app()->db->createCommand()->select('COUNT(id) as num')->from(Doc::model()->tableName())->where('doc_type_id = 2')->andWhere('id=:temp' , [':temp' =>$u_cat ])->queryRow()['num'];
    $generated_file_data_categs = (int)Yii::app()->db->createCommand()->select('COUNT(DocID) as num')->from(Newdoc::model()->tableName() . " n")->join(Doc::model()->tableName() ." d" , "n.Doc_template = d.id")->where('d.doc_type_id = 1')->andWhere('Doc_template=:temp' , [':temp' =>$u_cat ])->queryRow()['num'];
    $uploaded_file_data[] = $uploaded_file_data_categs;
    $generated_file_data[] = $generated_file_data_categs;
    $total_file_data[] = $generated_file_data_categs + $uploaded_file_data_categs;
endforeach;

 $this->widget('ext.highcharts.HighchartsWidget', array(
    'scripts' => array(
        'modules/exporting',
        'themes/grid-light',
    ),
    'options' => array(
        'title' => array(
            'text' => 'Projects Implementation progress Reports',
        ),
        'xAxis' => array(
            'categories' => array_values($categ),
        ),
        'labels' => array(
            'items' => array(
                array(
                    'html' => 'Total no of Projects',
                    'style' => array(
                        'left' => '50px',
                        'top' => '18px',
                        'color' => 'js:(Highcharts.theme && Highcharts.theme.textColor) || \'black\'',
                    ),
                ),
            ),
        ),
        'series' => array(
            array(
                'type' => 'column',
                'name' => 'Ongoing Projects',
                'data' => $generated_file_data,
            ),
            array(
                'type' => 'column',
                'name' => 'Completed Projects',
                'data' => $uploaded_file_data,
           ),
            
            array(
                'type' => 'spline',
                'name' => 'Total projects',
                'data' => $total_file_data,
                'marker' => array(
                    'lineWidth' => 2,
                    'lineColor' => 'js:Highcharts.getOptions().colors[3]',
                    'fillColor' => 'white',
                ),
            ),
            array(
                'type' => 'pie',
                'name' => 'Total Projects',
                'data' =>
                    $this->getPieData($categ),
                 
                'center' => array(100, 80),
                'size' => 100,
                'showInLegend' => false,
                'dataLabels' => array(
                    'enabled' => false,
                ),
            ),
        ),
    )
));
 
 
          ?>
          </div>
              <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
               


              </div>
            </div>
          </div>
          
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
    
    <?php
    $theme = Yii::app()->baseUrl;
   Yii::app()->clientScript->registerCssFile($theme.' /css/AdminLTE.min.css');
   Yii::app()->clientScript->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');
   Yii::app()->clientScript->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
    