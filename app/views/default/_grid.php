<?php
$grid_id = 'rq_approval_list-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-<?php echo $grid_id ?>" data-widget-editbutton="false" data-widget-deletebutton="false">
    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2><?php echo Lang::t('Requisitions Pending My approval') ?> </h2>
    </header>
    <!-- widget div-->
    <div>
        <!-- widget content -->
        <div class="widget-body no-padding">
            <div class="dataTables_wrapper form-inline">

                <div class="dt-top-row">

                    <div class="dataTables_filter">
                        <?php
                        /* $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                          'gridID' => $grid_id,
                          'formID' => $search_form_id,
                          'model' => $model,
                          'action' => Yii::app()->createUrl($this->route, $this->actionParams),
                          ));
                          ?>
                          <?php $this->endWidget();
                         *  ?>
                         */
                        ?>
                    </div>
                </div>
                <?php
                $this->widget('application.components.widgets.GridView', array(
                    'id' => $grid_id,
                    'dataProvider' => $model->search(),
                    'enablePagination' => $model->enablePagination,
                    'enableSummary' => '',
                    'columns' => array(
                        'item_description',
                        'programname',
                        'reqestorname',
                        array(
                            'class' => 'ButtonColumn',
                            'template' => '{approve}',
                            'htmlOptions' => array('style' => 'width: 100px;'),
                            'buttons' => array(
                                'approve' => array(
                                    'imageUrl' => false,
                                    'label' => '<i class="fa fa-cogs fa-1x "></i> ' . Lang::t(Constants::LABEL_ACTION),
                                    'url' => 'Yii::app()->controller->createUrl("/req/requisitionHead/myactions",array("id"=>$data->id))',
                                    'visible' => '$this->grid->owner->showLink("' . ReqModuleConstants::RES_REQUISITION . '","' . Acl::ACTION_UPDATE . '")?true:false',
                                    'options' => array(
                                        'class' => ' show_modal_form',
                                        'title' => Lang::t(Constants::LABEL_ACTION),
                                    ),
                                ),
                            )
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>