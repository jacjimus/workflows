<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
<?php $this->renderPartial('_tab'); ?>

    <div class="col-md-12">
	 <div class="well well-lights">
	
                <div class="row">
                        <?php $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
        <div class="row">
        <?php 
		if(isset($posted)&&$posted==1)
		$this->renderPartial('_grid', array('model' => $models)); ?>
    </div>
    </div>
	</div>
</div>

