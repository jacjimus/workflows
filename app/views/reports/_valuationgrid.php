<?php
$grid_id = 'rq_approval_list-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="jarviswidget jarviswidget-color-darken" id="wid-id-<?php echo $grid_id ?>" data-widget-editbutton="false" data-widget-deletebutton="false">
    <header>
        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
        <h2><?php echo Lang::t('Valuation Pending My approval') ?> </h2>
    </header>
    <!-- widget div-->
    <div>
        <!-- widget content -->
        <div class="widget-body no-padding">
            <div class="dataTables_wrapper form-inline">

                <div class="dt-top-row">

                    <div class="dataTables_filter">


                    </div>
                </div>
                <?php
                $this->widget('application.components.widgets.GridView', array(
                    'id' => $grid_id,
                    'dataProvider' => $model->search(),
                    'enablePagination' => $model->enablePagination,
                    'enableSummary' => '',
                    'columns' => array(
                        'id',
                        array(
                            'name' => 'rfq_id',
                            'value' => 'Rfq::model()->get($data->rfq_id,"ref_no")'
                        ),
                        'valuation_ref',
                        array(
                            'class' => 'ButtonColumn',
                            'template' => '{download}{approve}',
                            'htmlOptions' => array('style' => 'width: 150px;'),
                            'buttons' => array(
                                'download' => array(
                                    'imageUrl' => false,
                                    'label' => '<i class="fa fa-download fa-2x yellow text-success"></i>',
                                    'url' => 'Yii::app()->controller->createUrl("/proc/rfqValuationdataHeader/download",array("id"=>$data->id))',
                                    'visible' => '$data->docExists()?true:false',
                                    'options' => array(
                                        'class' => '',
                                        'title' => Lang::t('Download'),
                                    ),
                                ),
                                'approve' => array(
                                    'imageUrl' => false,
                                    'label' => '<i class="fa fa-cogs fa-1x "></i> ' . Lang::t(Constants::LABEL_ACTION),
                                    'url' => 'Yii::app()->controller->createUrl("/proc/rfqValuationdataHeader/myactions",array("id"=>$data->id))',
                                    'visible' => '$this->grid->owner->showLink("' . ProcModuleConstants::RES_RFQ_VALUATION_DATA . '","' . Acl::ACTION_UPDATE . '")?true:false',
                                    'options' => array(
                                        'class' => ' show_modal_form',
                                        'title' => Lang::t(Constants::LABEL_ACTION),
                                    ),
                                ),
                            )
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>