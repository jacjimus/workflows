<?php

$grid_id = 'employee-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-truck"></i>',
    'showExportButton' => true,
    'showSearch' => false,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            'id',
            array(
                'header' => Lang::t('Name'),
                'name' => 'Name',
                'value' => '$data->empname',
            ),
            'email',
            'mobile',
            array(
                'name' => 'birthdate',
                'value' => 'Common::formatDate($data->birthdate,"d-m-Y")',
            ),
			array(
                'name' => 'hire_date',
                'value' => 'Common::formatDate($data->hire_date,"d-m-Y")',
            ),
            'gender',
         
        ),
    )
));
?>
