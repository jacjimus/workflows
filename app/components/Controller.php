<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * Stores commonly used system settings
     * @uses {@link CmsSettings}
     * @var array
     */
    public $settings = array();

    /**
     *
     * @var type
     */
    public $activeTab = 1;

    /**
     *
     * @var type
     */
    public $activeMenu = 1;

    /**
     *
     * @var type
     */
    public $package = array();

    /**
     *
     * @var type
     */
    public $package_name = 'smart_admin';

    /**
     * The module's published assets base url
     * @var type
     */
    public $package_base_url;

    /**
     * The base title for a controller
     * @var type
     */
    public $resourceLabel;

    /**
     * Stores the user's privileges
     * @var type
     */
    public $privileges;

    /**
     *
     * @var type
     */
    public $resource;

    /**
     *
     * @var type
     */
    public $module_assets_url;

    /**
     *
     * @var type
     */
    public $module_package;

    public function init() {
        parent::init();
        if (!Yii::app()->request->isAjaxRequest) {
            $this->registerPackage();
            $this->registerModulePackage();
        }
        $this->setSettings();

        //form error css
        CHtml::$errorCss = 'my-form-error';
        CHtml::$errorSummaryCss = 'alert alert-danger errorSummary';

        if (!Yii::app()->user->isGuest) {
            Users::setSessionVariables();
        }
        $this->setPrivileges();
    }

    protected function registerPackage() {
        $this->setPackage();

        $clientScript = Yii::app()->getClientScript();
        $this->package_base_url = $clientScript
                ->addPackage($this->package_name, $this->package)
                ->registerPackage($this->package_name)
                ->getPackageBaseUrl($this->package_name);
    }

    public function getPackageBaseUrl() {
        return $this->package_base_url;
    }

    public function setPackage() {
        //register commonly used assets.
        $this->package = array(
            'baseUrl' => Yii::app()->theme->baseUrl,
            'js' => array(
                'js/bootstrap/bootstrap.min.js',
                'js/app.config.min.js',
                'js/smartwidgets/jarvis.widget.min.js',
                'js/plugin/sparkline/jquery.sparkline.min.js',
                'js/plugin/masked-input/jquery.maskedinput.min.js',
                'js/plugin/select2/select2.min.js',
                'js/plugin/bootstrap-slider/bootstrap-slider.min.js',
                'js/plugin/date-time/bootstrap-timepicker.min.js',
                'js/plugin/msie-fix/jquery.mb.browser.min.js',
                'js/plugin/fastclick/fastclick.js',
                'js/plugin/blockui/jquery.blockUI.min.js',
                'js/plugin/typeahead/typeahead-bs2.min.js',
                'js/plugin/date-time/jquery.timeago.min.js',
                'js/plugin/handlebars/handlebars.min.js',
                'js/plugin/bootstrap-dialog/js/bootstrap-dialog.min.js',
                'js/app.min.js',
                'js/line-item.min.js',
                'js/notif.js',
                'js/printThis.js',
                'js/custom.min.js',
               
            ),
            'css' => array(
                'css/bootstrap.min.css',
                'css/font-awesome.min.css',
                'css/smartadmin-production.min.css',
                'css/smartadmin-skins.min.css',
                'js/plugin/bootstrap-dialog/css/bootstrap-dialog.min.css',
                'js/plugin/date-time/bootstrap-timepicker.css',
                'css/custom.css',
            )
        );
    }

    /**
     * Module package should be overriden by the module controller
     */
    public function setModulePackage() {
        $this->module_package = NULL;
    }

    /**
     * Register module specific packages if not NULL
     */
    protected function registerModulePackage() {
        if ($this->publishModuleAssets()) {
            $this->setModulePackage();

            if (!empty($this->module_package)) {
                $package_name = $this->getModuleName() . '_module';
                Yii::app()->getClientScript()
                        ->addPackage($package_name, $this->module_package)
                        ->registerPackage($package_name);
            }
        }
    }

    /**
     * Publish module assets. This only happens if there is "assets" directory in the module root.
     */
    protected function publishModuleAssets() {
        $module_name = $this->getModuleName();
        if (!$module_name)
            return FALSE;
        $path = Yii::getPathOfAlias($module_name . '.assets');
        if (!is_dir($path))
            return FALSE;

        $this->module_assets_url = Yii::app()->assetManager->publish($path, false, -1, YII_DEBUG ? true : null);
        return TRUE;
    }

    /**
     * Returns the url of the published module assets.
     * @return type
     */
    public function getModuleAssetsUrl() {
        return $this->module_assets_url;
    }

    protected function setSettings() {
        if (YII_DEBUG)
            Yii::app()->settings->deleteCache(SettingsModuleConstants::SETTINGS_GENERAL);

        //get some commonly used settings
        $this->settings = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, array(
            SettingsModuleConstants::SETTINGS_COMPANY_NAME,
            SettingsModuleConstants::SETTINGS_APP_NAME,
            SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE,
            SettingsModuleConstants::SETTINGS_THEME,
        ));
    }

    /**
     * Returns the module of the current controller
     * @return mixed module name or false if the controller does not belong to a module
     */
    public function getModuleName() {
        $module = $this->getModule();
        if ($module !== null)
            return $module->getName();
        return FALSE;
    }

    protected function setPrivileges() {
        //get the user's privileges only when a user is logged in
        if (!Yii::app()->user->isGuest) {
            if (!YII_DEBUG) {
                // keep the value in cache for at most (60*60 sec) 1 hr
                $id = md5(Yii::app()->user->id . '_privileges');
                $this->privileges = Yii::app()->cache->get($id);
                if ($this->privileges === FALSE) {
                    $this->privileges = Acl::getPrivileges();
                    Yii::app()->cache->set($id, $this->privileges, 60);
                }
            } else
                $this->privileges = Acl::getPrivileges();
        }
    }

    /**
     * Whether to show link or not
     * @param string $resource
     * @param string $action default to "view"
     * @return boolean  True or False
     */
    public function showLink($resource = null, $action = null) {
        if ($resource === null)
            $resource = $this->resource;

        if ($action === NULL)
            $action = Acl::ACTION_VIEW;
        return Acl::hasPrivilege($this->privileges, $resource, $action, FALSE);
    }

    /**
     * Should be called before any action the require ACL
     * @param string $action
     */
    public function hasPrivilege($action = NULL) {
        if (NULL === $action)
            $action = Acl::ACTION_VIEW;
        Acl::hasPrivilege($this->privileges, $this->resource, $action);
    }

}
