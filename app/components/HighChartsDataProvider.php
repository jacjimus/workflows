<?php

/**
 *
 */
class HighChartsDataProvider
{

    /**
     * The data source class name
     * @var string
     */
    public $dataSource;

    /**
     * SQL query options
     * @var type
     */
    public $queryOptions = array(
        'filters' => array(), //Any $key=>$value table filters where $key is a column name and $value is the columns value. This will lead to a series of AND conditions
        'condition' => '', //Any condition that must be passed to all querys. This value is only necessary when passing other conditions which are not "AND". for conditions with "AND" use table_filters instead. e.g  "(`org_id`='3' OR `org_id`='6')",
        'date_field' => 'date_created',
        'sum' => false, //If this value is FALSE then COUNT(*) will be applied. If you want to get the SUM(`colum_name`) then pass the column_name e.g "sum"=>"column_name"
    );

    /**
     * Options for the display of the graph
     * @var type
     */
    public $graphOptions = array(
        'graphType' => null,
        'title' => 'Report',
        'subtitle' => NULL,
        'y_axis_label' => NULL,
        'default_series_name' => NULL, //this will be used as piechart series name
        'y_axis_min' => 0,
    );

    /**
     * The hicharts series
     * {@link  http://api.highcharts.com/highcharts#series}
     * @var type
     */
    private $series = array();

    /**
     * @see {HighCharts::getXAxisParams()}
     * @var type
     */
    private $xAxisParams = array();

    /**
     *
     * @var type
     */
    protected $graphType;

    /**
     * Get graph data e.g line graph,bar graph etc
     * @param string $data_source The model class name providing the data
     * @param array $query_options {@see $this->query_options}
     * @param array $graph_options {@see $this->graph_options}
     * @param integer $max_labels Maximum labels of x,y axis graph . Default is 12
     * @param array $series_options_method_name
     * @return array $data
     */
    public static function getData($data_source, array $query_options = array(), array $graph_options = array(), $max_labels = 12, $series_options_method_name = null)
    {
        $dataProvider = new HighChartsDataProvider();
        $data = array();
        $dataProvider->dataSource = $data_source;
        $dataProvider->setVariables($query_options, $graph_options, $series_options_method_name);
        if ($dataProvider->graphType !== HighChartsHelper::GRAPH_PIE) {
            //get other graph data(with x and y axis)
            $dataProvider->xAxisParams = HighChartsHelper::getXAxisParams($max_labels);
            $data = $dataProvider->getGraphData();
        } else {
            $data = $dataProvider->getPieData();
        }

        if (isset($_GET[HighChartsHelper::GET_PARAM_HIGHCHART_FLAG])) {
            echo json_encode($data);
            Yii::app()->end();
        } else
            return $data;
    }

    /**
     * Get x,y axis graph data
     * @return type
     */
    protected function getGraphData()
    {
        $dataSource = $this->dataSource;
        $sum = $this->queryOptions['sum'];
        $dates = $this->xAxisParams['dates'];
        $from = current($dates);
        $to = end($dates);
        $x_labels = array();
        $date_type = $this->xAxisParams['date_type'];
        $query = $this->prepareQuery($from['date'], $to['date']);
        $base_condition = $query['condition'];
        $base_params = $query['params'];
        $series = !empty($this->series) ? $this->series : array();
        $date_field = $this->queryOptions['date_field'];

        foreach ($dates as $date) {
            $x_labels[] = $date['label'];
            $condition = !empty($base_condition) ? $base_condition . ' AND ' : $base_condition;
            $params = $base_params;
            if ($date_type === HighChartsHelper::DATE_TYPE_DAY) {
                $condition .= "(DATE(`{$date_field}`)=DATE(:{$date_field}))";
            } else if ($date_type === HighChartsHelper::DATE_TYPE_MONTH) {
                $condition .= "(MONTH(`{$date_field}`)=MONTH(:{$date_field}) AND YEAR(`{$date_field}`)=YEAR(:{$date_field}))";
            } else {
                $condition .= "(YEAR(`{$date_field}`)=YEAR(:{$date_field}))";
            }
            $params[":{$date_field}"] = $date['date'];

            foreach ($series as $k => $element) {
                $final_condition = !empty($element['condition']) ? $element['condition'] . ' AND ' . $condition : $condition;
                $final_params = !empty($element['params']) ? array_merge($element['params'], $params) : $params;
                $sum = isset($element['sum']) ? $element['sum'] : $sum;
                $data = $sum ? $dataSource::model()->getSum($sum, $final_condition, $final_params) : $dataSource::model()->getTotals($final_condition, $final_params);
                $series[$k]['data'][] = (float) round($data, 2);
            }
        }

        $series_colors = array();
        $new_series = array();
        foreach ($series as $k => $element) {
            $element['type'] = $this->graphType;
            if (isset($element['condition']))
                unset($element['condition']);
            if (isset($element['params']))
                unset($element['params']);
            if (isset($element['sum']))
                unset($element['sum']);
            if (isset($element['color'])) {
                $series_colors[] = $element['color'];
                unset($element['color']);
            }
            array_push($new_series, $element);
        }

        return array(
            'graphType' => $this->graphType,
            'series' => $new_series,
            'x_labels' => $x_labels,
            'subtitle' => !empty($this->graphOptions['subtitle']) ? $this->graphOptions['subtitle'] : HighChartsHelper::getDateRange(),
            'y_axis_title' => $this->graphOptions['y_axis_label'],
            'y_axis_min' => $this->graphOptions['y_axis_min'],
            'step' => $this->xAxisParams['step'],
            'title' => $this->graphOptions['title'],
            'colors' => $series_colors,
        );
    }

    /**
     * Get Pie chart data (non x,y axis)
     */
    public function getPieData()
    {
        $dataSource = $this->dataSource;
        $sum = $this->queryOptions['sum'];
        $date_range = HighChartsHelper::explodeDateRange();
        $query = $this->prepareQuery($date_range['from'], $date_range['to']);
        $base_condition = $query['condition'];
        $base_params = $query['params'];
        $series = $this->series;
        $series_colors = array();
        $data = isset($series[0]['data']) ? $series[0]['data'] : array();
        $series[0]['type'] = $this->graphType;
        $chart_title = $this->graphOptions['title'];
        $series[0]['name'] = !empty($this->graphOptions['default_series_name']) ? $this->graphOptions['default_series_name'] : $chart_title;

        foreach ($data as $k => $element) {
            $condition = !empty($element['condition']) ? $element['condition'] . ' AND ' . $base_condition : $base_condition;
            $params = !empty($element['params']) ? array_merge($element['params'], $base_params) : $base_params;
            $sum = isset($series[0]['data'][$k]['sum']) ? $series[0]['data'][$k]['sum'] : $sum;
            $data = $sum ? $dataSource::model()->getSum($sum, $condition, $params) : $dataSource::model()->getTotals($condition, $params);
            $series[0]['data'][$k]['y'] = (float) round($data, 2);
            if (isset($series[0]['data'][$k]['condition']))
                unset($series[0]['data'][$k]['condition']);
            if (isset($series[0]['data'][$k]['params']))
                unset($series[0]['data'][$k]['params']);
            if (isset($series[0]['data'][$k]['sum']))
                unset($series[0]['data'][$k]['sum']);
            if (isset($series[0]['data'][$k]['color'])) {
                $series_colors[] = $series[0]['data'][$k]['color'];
                unset($series[0]['data'][$k]['color']);
            }
        }

        return array(
            'graphType' => $this->graphType,
            'series' => $series,
            'subtitle' => !empty($this->graphOptions['subtitle']) ? $this->graphOptions['subtitle'] : HighChartsHelper::getDateRange(),
            'title' => $chart_title,
            'colors' => $series_colors,
        );
    }

    /**
     * pepare a hichart data query
     * @param type $from_date
     * @param type $to_date
     * @return type
     */
    protected function prepareQuery($from_date = null, $to_date = null)
    {
        $dataSource = $this->dataSource;
        $params = array();
        //filters
        $condition = $this->queryOptions['condition'];
        $table_filters = $this->queryOptions['filters'];
        $date_field = $this->queryOptions['date_field'];

        foreach ($table_filters as $k => $v) {
            if (!empty($v) && $dataSource::model()->hasAttribute($k)) {
                $condition.=!empty($condition) ? ' AND ' : '';
                $condition .= "(`{$k}`=:{$k})";
                $params[":{$k}"] = $v;
            }
        }
        //date boundary
        if (!empty($from_date) && !empty($to_date)) {
            //make sure that data falls between "from date" and "to date"
            $condition.=!empty($condition) ? ' AND ' : '';
            $condition .= "(DATE(`{$date_field}`)>=DATE(:t1_from) AND DATE(`{$date_field}`)<=DATE(:t2_to))";
            $params[':t1_from'] = $from_date;
            $params[':t2_to'] = $to_date;
        }
        return array(
            'condition' => $condition,
            'params' => $params,
        );
    }

    /**
     * Initialize the properties
     * @param array $query_options
     * @param array $graph_options
     * @param array $series_method_name
     */
    protected function setVariables(array $query_options, array $graph_options, $series_method_name)
    {
        $dataSource = $this->dataSource;
        if (!empty($query_options))
            $this->queryOptions = ArrayHelper::extend($this->queryOptions, $query_options);
        if (!empty($graph_options))
            $this->graphOptions = ArrayHelper::extend($this->graphOptions, $graph_options);
        if (empty($this->graphOptions['y_axis_label']))
            $this->graphOptions['y_axis_label'] = $this->graphOptions['title'];

        HighChartsHelper::init();

        if (!empty($this->graphOptions['graphType']))
            HighChartsHelper::setGraphType($this->graphOptions['graphType']);
        $this->graphType = HighChartsHelper::getGraphType();
        $this->series = !empty($series_method_name) ? $dataSource::model()->$series_method_name($this->graphType) : $dataSource::model()->highChartsSeriesOptions($this->graphType);
    }

}
