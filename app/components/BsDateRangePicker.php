<?php

/**
 * Helper function for great  Bootstrap DateRangePicker
 * @link https://github.com/dangrossman/bootstrap-daterangepicker
 *
 * @author Fred <mconyango@gmail.com>
 * Timestamp May 26 2014 at 06:05 am
 */
class BsDateRangePicker
{

    const DATE_RANGE_GET_PARAM = 'bs_d_r';

    /**
     * Get default date range for bootstrap daterangepicker
     * @param string $format date format
     * @param int $interval interval in months e.g 6
     * @return string
     */
    public static function getDateRange($format = 'Y/m/d', $interval = 6)
    {
        //past 6 months
        $from = Common::addDate(date('Y-m-d'), -$interval, 'month', $format);
        $to = date($format, time());
        $date_range = $from . ' - ' . $to;
        return $date_range;
    }

    /**
     * Format daterange into from and to array values
     * @param type $dataRange
     * @param type $format
     * @return type
     */
    public static function explodeDateRange($dataRange, $format = 'Y-m-d')
    {
        $date_range = explode('-', $dataRange);
        $from = Common::formatDate(trim($date_range[0]), $format);
        $to = isset($date_range[1]) ? Common::formatDate(trim($date_range[1]), $format) : NULL;
        return array(
            'from' => trim($from),
            'to' => trim($to),
        );
    }

    /**
     * Get grid Search Model condition
     * @param type $date_attribute
     * @return type
     */
    public static function getGridSearchModelCondition($date_attribute)
    {
        $condition = '';
        if ($date_range = filter_input(INPUT_GET, self::DATE_RANGE_GET_PARAM)) {
            $date_range = self::explodeDateRange($date_range);
            $condition = "DATE(`{$date_attribute}`) >=DATE('{$date_range['from']}')";
            if (!empty($date_range['to'])) {
                $condition.=" AND DATE(`{$date_attribute}`) <=DATE('{$date_range['to']}')";
            }
        }
        return $condition;
    }

}
