<?php

/**
 * Common  behaviors
 * Converts a local time to UTC format
 * @author Fred <mconyango@gmail.com>
 */
class MyModelBehavior extends CActiveRecordBehavior
{

    public function beforeSave($event)
    {

        $owner = $this->getOwner();
        foreach ($owner->dateTimeFields() as $dateTimeField) {
            if ($owner->hasAttribute($dateTimeField) && !empty($owner->{$dateTimeField})) {
                $owner->{$dateTimeField} = Yii::app()->localtime->fromLocalDateTime($owner->{$dateTimeField});
            }
        }

        //add created by
        if ($owner->hasAttribute('created_by') && empty($owner->created_by) && !Yii::app() instanceof CConsoleApplication && !empty(Yii::app()->user->id))
            $owner->created_by = Yii::app()->user->id;


        return parent::beforeSave($event);
    }

    public function afterFind($event)
    {
        $owner = $this->getOwner();
        $owner->dateTimeFields();
        foreach ($owner->dateTimeFields() as $dateTimeField) {
            if ($owner->hasAttribute($dateTimeField) && !empty($owner->{$dateTimeField}))
                $owner->{$dateTimeField} = Yii::app()->localtime->toLocalDateTime($owner->{$dateTimeField}, 'Y-m-d H:i:s');
        }

        foreach ($owner->dateFields() as $dateField) {
            if ($owner->hasAttribute($dateField) && !empty($owner->{$dateField}))
                $owner->{$dateField} = Yii::app()->localtime->toLocalDateTime($owner->{$dateField}, 'Y-m-d');
        }

        return (parent::afterFind($event));
    }

}
