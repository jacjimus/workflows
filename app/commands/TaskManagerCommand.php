<?php

/**
 * All cron jobs actions are defined here.
 * @author Fred  <mconyango@gmail.com>
 */
class TaskManagerCommand extends CConsoleCommand {

      public function actionSendEmail(array $args) {
            $this->startTask(QueueTasks::TASK_SEND_EMAIL);
      }

      protected function sendEmail() {
            MsgEmail::model()->processQueues();
            print_r('Mails Sent at ' . time());
      }

      public function actionSendSms(array $args) {
            $this->startTask(QueueTasks::TASK_SEND_SMS);
      }

      protected function sendSms() {
            SmsHandler::processSmsQueues();
            print_r('SMS Sent');
      }

      public function actionCleanUp() {
            $this->startTask(QueueTasks::TASK_CLEAN_UP);
      }

      protected function cleanUp() {
            MyYiiUtils::clearTempFiles();
      }

      public function actionNotificationManager() {
            $this->startTask(Notif::TASK_NOTIFICATION_MANAGER);
      }

      protected function notificationManager() {
            Notif::model()->updateNotification();
      }

      /**
       * Start a Task
       * @param type $task_id
       * @return type
       */
      protected function startTask($task_id) {
            try {
                  $task = QueueTasks::model()->findByPk($task_id);
                  if (NULL === $task)
                        throw new Exception("{$task_id} does not exist.");
                  if ($task->status !== QueueTasks::STATUS_ACTIVE)
                        return FALSE;
                  //non continuous execution type
                  if ($task->execution_type === QueueTasks::EXEC_TYPE_CRON) {
                        $this->{$task_id}();
                        return QueueTasks::model()->updateLastRun($task->id);
                  }

                  $error = FALSE;
                  if ($task->threads >= $task->max_threads)
                        $error = TRUE;
                  if ($error === TRUE) {
                        $has_timed_out = QueueTasks::model()->hasTimedOut($task->id);
                        if ($has_timed_out) {
                              $error = FALSE;
                              $task->threads = 1;
                        }
                  } else
                        $task->threads++;

                  if ($error === FALSE) {
                        $task->save(false);
                        while ($task->status === QueueTasks::STATUS_ACTIVE) {
                              if (Yii::app()->db->getActive() === FALSE)
                                    Yii::app()->db->setActive(TRUE); //open new connection
                              $this->{$task_id}();
                              $sleep_seconds = $task->sleep;
                              if ($sleep_seconds < 1)
                                    $sleep_seconds = 1;
                              QueueTasks::model()->updateLastRun($task->id);
                              $task->refresh();
                              Yii::app()->db->setActive(FALSE); //close db connection on sleep mode.
                              sleep((int) $sleep_seconds);
                        }
                  }
            } catch (Exception $exc) {
                  Yii::log($exc->getMessage(), CLogger::LEVEL_ERROR);
            }
      }

}
