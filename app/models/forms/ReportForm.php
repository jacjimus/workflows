<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReportForm extends FormModel {

        public $name;
        public $email;
        public $subject;
        public $body;
        public $verifyCode;
        public $recaptcha;
		public $date_from;
		public $date_to;
		public $dob_from;
		public $dob_to;
		public $gender;
		public $country_id;
		public $skill_id;

        /**
         * Declares the validation rules.
         */
        public function rules()
        {
                return array(
                    // name, email, subject and body are required
                    array('name, email, body', 'required'),
                    array('subject,gender,dob_to,dob_from,date_to,date_from,country_id,skill_id', 'safe'),
                    // email has to be a valid email address
                    array('email', 'email'),
                    // verifyCode needs to be entered correctly
                    //array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
					//array(),
                    //array('recaptcha', 'application.extensions.recaptcha.EReCaptchaValidator', 'privateKey' => Yii::app()->params['recaptcha']['privatekey']),
                );
        }
		
		
		/**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'country' => array(self::BELONGS_TO, 'MeCountry', 'country_id'),
            'skill' => array(self::BELONGS_TO, 'HrSkills', 'skill_id'),
        );
    }

        /**
         * Declares customized attribute labels.
         * If not declared here, an attribute would have a label that is
         * the same as its name with the first letter in upper case.
         */
        public function attributeLabels()
        {
                return array(
                    'verifyCode' => 'Verification Code',
                    'date_from' => 'Employment Date From',
                    'date_to' => 'Employment Date To',
					'dob_from' => 'DOB Date From',
                    'dob_to' => 'DOB Date To',
                    'gender' => 'Gender',
					'country_id'=>'Country of Training',
					'skill_id'=>'Skill'
                );
        }
		
		public function genderOptions(){
			return ['Female'=>'Female','Male'=>'Male'];
		}

}

