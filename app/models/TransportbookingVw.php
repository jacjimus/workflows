<?php

/**
 * This is the model class for table "transportbooking_vw".
 *
 * The followings are the available columns in table 'transportbooking_vw':
 * @property integer $id
 * @property string $empname
 * @property string $project
 * @property string $notes
 * @property string $departure_date
 * @property string $departure_time
 * @property string $destination
 * @property string $return_date
 * @property string $return_time
 * @property integer $approved
 * @property integer $submit
 * @property integer $rejected
 * @property integer $cancelled
 * @property integer $current_action
 * @property integer $created_by
 * @property string $date_created
 * @property integer $funds_allocated
 * @property integer $vehicle_allocated
 */
class TransportbookingVw extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TransportbookingVw the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transportbooking_vw';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project, notes, departure_date, departure_time, destination, return_date, return_time, created_by, date_created', 'required'),
			array('id, approved, submit, rejected, cancelled, current_action, created_by, funds_allocated, vehicle_allocated', 'numerical', 'integerOnly'=>true),
			array('empname', 'length', 'max'=>257),
			array('project', 'length', 'max'=>200),
			array('destination', 'length', 'max'=>100),
			array('return_time', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, empname, project, notes, departure_date, departure_time, destination, return_date, return_time, approved, submit, rejected, cancelled, current_action, created_by, date_created, funds_allocated, vehicle_allocated', 'safe', 'on'=>'search'),
                        array('id,'.self::SEARCH_FIELD, 'safe', 'on'=>  self::SEARCH_SCENARIO),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'empname' => 'Empname',
			'project' => 'Project',
			'notes' => 'Notes',
			'departure_date' => 'Departure Date',
			'departure_time' => 'Departure Time',
			'destination' => 'Destination',
			'return_date' => 'Return Date',
			'return_time' => 'Return Time',
			'approved' => 'Approved',
			'submit' => 'Submit',
			'rejected' => 'Rejected',
			'cancelled' => 'Cancelled',
			'current_action' => 'Current Action',
			'created_by' => 'Created By',
			'date_created' => 'Date Created',
			'funds_allocated' => 'Funds Allocated',
			'vehicle_allocated' => 'Vehicle Allocated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search45()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('empname',$this->empname,true);
		$criteria->compare('project',$this->project,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('departure_date',$this->departure_date,true);
		$criteria->compare('departure_time',$this->departure_time,true);
		$criteria->compare('destination',$this->destination,true);
		$criteria->compare('return_date',$this->return_date,true);
		$criteria->compare('return_time',$this->return_time,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('submit',$this->submit);
		$criteria->compare('rejected',$this->rejected);
		$criteria->compare('cancelled',$this->cancelled);
		$criteria->compare('current_action',$this->current_action);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('funds_allocated',$this->funds_allocated);
		$criteria->compare('vehicle_allocated',$this->vehicle_allocated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchParams()
        {
            return array(
                 array('id', self::SEARCH_FIELD,true,'OR'),
                 array('empname', self::SEARCH_FIELD,true,'OR'), 
		 array('project', self::SEARCH_FIELD,true,'OR'), 
                array('destination', self::SEARCH_FIELD,true,'OR'),                                 
				'notes', 
				'departure_date', 
				'departure_time', 
				'return_date', 
				'return_time', 
				'approved', 
				'rejected', 
				'submit', 
				'cancelled', 
				'current_action', 
				'created_by', 
				'date_created',                            
                'id',
            );
        }
}