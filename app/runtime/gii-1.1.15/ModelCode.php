<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => '',
  'modelPath' => 'application.modules.projects.models',
  'baseClass' => 'ActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
