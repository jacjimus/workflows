<?php

/**

 * Home controller

 * @author Fred<mconyango@gmail.com>

 */

class DefaultController extends Controller {



    public function init() {

        parent::init();

    }



    /**

     * @return array action filters

     */

    public function filters() {

        return array(

            'accessControl', // perform access control for CRUD operations

        );

    }



    /**

     * Specifies the access control rules.

     * This method is used by the 'accessControl' filter.

     * @return array access control rules

     */

    public function accessRules() {

        return array(

            array('allow',

                'actions' => array('index'),

                'users' => array('@'),

            ),

            array('deny', // deny all users

                'users' => array('*'),

            ),

        );

    }



    /**

     * Declares class-based actions.

     */

    public function actions() {

        return array(

            // captcha action renders the CAPTCHA image displayed on the contact page

            'captcha' => array(

                'class' => 'CCaptchaAction',

                'backColor' => 0xFFFFFF,

            ),

            // page action renders "static" pages stored under 'protected/views/site/pages'

            // They can be accessed via: index.php?r=site/page&view=FileName

            'page' => array(

                'class' => 'CViewAction',

            ),

        );

    }



    /**

     * This is the default 'index' action that is invoked

     * when an action is not explicitly requested by users.

     */

    public function actionIndex() {

        //$this->redirect(array('inv/default/index'));

        $this->pageTitle = Lang::t('Dashboard');





        //Valuation Ends

        $this->render('index', array());

    }
    
    public function getPieData($arr)
    {
        $uploaded_file = (int)Yii::app()->db->createCommand()->select('COUNT(id) as num')->from(Doc::model()->tableName())->where('doc_type_id = 2')->queryRow()['num'];
         $generated_files = (int)Yii::app()->db->createCommand()->select('COUNT(DocID) as num')->from(Newdoc::model()->tableName() . " n")->join(Doc::model()->tableName() ." d" , "n.Doc_template = d.id")->where('d.doc_type_id = 1')->queryRow()['num'];
   
        
        return array(
        array(
                        'name' => 'Completed Projects',
                        'y' => $uploaded_file,
                        'color' => 'js:Highcharts.getOptions().colors[1]', // Jane's color
                    ),
                    array(
                        'name' => 'Ongoing Projects',
                        'y' => $generated_files,
                        'color' => 'js:Highcharts.getOptions().colors[0]', // John's color
                    ),
                   
            );
    }



}

