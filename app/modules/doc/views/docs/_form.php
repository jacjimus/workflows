<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'category_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                       <?php echo CHtml::activeDropDownList($model, 'category_id', DocCategories::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
            
                </div>
        </div>
       <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'doc_type_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'doc_type_id', [1 => 'Template document' , 2 => 'Embassy upload document'], array('prompt' => 'Select',  'class' => 'form-control' , 'id' => 'doc_type_id')); ?>
                   
                </div>
        </div>
        
        

        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'maxlength' => 255, 'rows' => 3)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'approval', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'approval', array()); ?>
                </div>
        </div>
        <?php $this->renderPartial('_file_field', array('model' => $model)); ?>
</div>

<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); 

//Yii::app()->clientScript->registerScript('doctype', '
//    if($(\'#doc_type_id\').val() == "1")
//     $(\'#file-field\').hide(100);
//    $(\'#doc_type_id\').change(function(e){
//    if($(\'#doc_type_id\').val() == "2")
//        $(\'#file-field\').show(1000);
//        else
//        $(\'#file-field\').hide(1000);
//    })
//');