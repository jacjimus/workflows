<div class="row">
    <div class="col-md-3">
        <?php $this->renderPartial('doc.views.docTypes._tab') ?>
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-fw fa-file"></i>
                    <?php echo CHtml::encode($this->pageTitle); ?>
                    <?php if (!empty($doc_type_model)): ?>
                        &nbsp;
                        <small>
                            <?php if ($this->showLink(DocModuleConstants::RES_COMPANY_DOCS, Acl::ACTION_DELETE) && $doc_type_model->canDelete()): ?>
                                <a href="javascript:void(0)" class="text-danger ajax-delete" data-ajax-url="<?php echo $this->createUrl('docTypes/delete', array('id' => $doc_type_model->id)) ?>" data-confirm-message="<?php echo Lang::t('DELETE_CONFIRM') ?>"><i class="fa fa-trash-o"></i> <?php echo Lang::t(Constants::LABEL_DELETE) ?></a>
                            <?php endif; ?>
                            |
                            <?php if ($this->showLink(DocModuleConstants::RES_COMPANY_DOCS, Acl::ACTION_UPDATE)): ?>
                                <a href="<?php echo $this->createUrl('docTypes/update', array('id' => $doc_type_model->id)) ?>" class="text-success show_modal_form"><i class="fa fa-edit"></i> <?php echo Lang::t(Constants::LABEL_UPDATE) ?></a>
                            <?php endif; ?>
                        </small>
                    <?php endif; ?>
                </h1> 
            </div>
        </div>
        <?php $this->renderPartial('_grid', array('model' => $model)); ?>
    </div>
</div>