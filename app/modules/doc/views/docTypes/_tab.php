<?php
$data = DocCategories::model()->getData();
?>
<?php if ($this->showLink(DocModuleConstants::RES_COMPANY_DOCS, Acl::ACTION_CREATE)): ?>
    <p><a class="btn btn-sm btn-default btn-block show_modal_form" href="<?php echo $this->createUrl('docCategories/create') ?>"><i class="fa fa-play-circle-o"></i> <?php echo Lang::t('Add document category') ?></a></p>
<?php endif; ?>
<div class="list-group my-list-group">
    <a href="<?php echo $this->createUrl('docs/index') ?>" class="list-group-item">
        <h4 class="list-group-item-heading"><?php echo Lang::t('All Documents') ?> <span class="badge"><?php echo Doc::model()->getDocCount() ?></span></h4>
        <p class="list-group-item-text"><?php echo Lang::t('Browse all documents'); ?></p>
    </a>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $row): ?>
            <a href="<?php echo $this->createUrl('docs/index', array('category_id' => $row['id'])) ?>" class="list-group-item">
                <h4 class="list-group-item-heading"><?php echo CHtml::encode($row['name']) ?>  <span class="badge"><?php echo Doc::model()->getDocCount($row['id']) ?></span></h4>
                <p class="list-group-item-text"><?php echo CHtml::encode($row['description']) ?></p>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>