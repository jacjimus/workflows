<?php

/**
 * This is the model class for table "doc".
 *
 * The followings are the available columns in table 'doc':
 * @property string $id
 * @property string $doc_type_id
 * @property string $name
 * @property string $doc_file
 * @property string $description
 * @property string $location_id
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property DocTypes $docType
 * @property SettingsOrg $branch
 * @property SettingsDepartment $dept
 */
class Doc extends ActiveRecord implements IMyActiveSearch {

      const BASE_DIR = 'docs';

      /**
       * Fine uploader logic
       * @var type
       */
      public $temp_doc_file;

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'doc';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('doc_type_id, name', 'required'),
                array('doc_file', 'required', 'message' => 'Please upload a pdf file template'),
                array('doc_type_id, location_id, category_id, created_by', 'length', 'max' => 11),
                array('name,doc_file', 'length', 'max' => 128),
                array('description', 'length', 'max' => 255),
                array('approval', 'boolean', 'allowEmpty'=>true),
                array('temp_doc_file, category_id', 'safe'),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
                'docCat' => array(self::BELONGS_TO, 'docCategories', 'category_id'),
                'location' => array(self::BELONGS_TO, 'SettingsLocation', 'location_id'),
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'doc_type_id' => Lang::t('Doc Type'),
                'name' => Lang::t('Document title'),
                'doc_file' => Lang::t('File'),
                'doc_alias' => Lang::t('Doc Alias'),
                'temp_doc_file' => Lang::t('File'),
                'approval' => Lang::t('Require approval?'),
                'description' => Lang::t('Description'),
                'location_id' => Lang::t('Branch'),
                'date_created' => Lang::t('Date Added'),
                'created_by' => Lang::t('Added By'),
                'category_id' => Lang::t('Document Category'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return Doc the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }
      
      public function beforeSave() {
          $this->doc_alias = strtolower(str_replace(" ","_", $this->name));
          return parent::beforeSave();
      }

      public function beforeValidate() {
            $this->setDocFile();
            return parent::beforeValidate();
      }

      public function afterDelete() {
            $dir = $this->getDir();
            $file = $dir . DS . $this->doc_file;
            if (file_exists($file))
                  @unlink($file);

            return parent::afterDelete();
      }

      public function searchParams() {
            return array(
                array('name', self::SEARCH_FIELD, true, 'OR'),
                array('description', self::SEARCH_FIELD, true, 'OR'),
                'doc_type_id',
                'location_id',
                'category_id',
            );
      }

      /**
       * Get doc count
       * @param type $doc_type_id
       * @return int
       */
      public function getDocCount($category_id = NUll) {
            $conditions = "";
            $params = array();
            if (!empty($category_id)) {
                  $conditions.='`category_id`=:category_id';
                  $params[':category_id'] = $category_id;
            }
            
            return $this->getTotals($conditions, $params);
      }

      /**
       * Get the dir of a user
       * @param string $doc_type_id
       */
      public function getDir($doc_type_id = NULL) {
            if (!empty($this->doc_type_id))
                  $doc_type_id = $this->doc_type_id;
            return Common::createDir($this->getBaseDir() . DS . $doc_type_id);
      }

      public function getBaseDir() {
            return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
      }

      protected function setDocFile() {
            //using fineuploader
            if (!empty($this->temp_doc_file)) {
                  $temp_file = Common::parseFilePath($this->temp_doc_file);
                  $image_name = $temp_file['name'];
                  $temp_dir = $temp_file['dir'];
                  $new_path = $this->getDir() . DS . $image_name;
                  if (copy($this->temp_doc_file, $new_path)) {
                        if (!empty($temp_dir))
                              Common::deleteDir($temp_dir);

                        $this->doc_file = $image_name;
                        $this->temp_doc_file = NULL;
                  }
            }
      }

      /**
       * Get file path
       * @return type
       */
      public function getFilePath() {
            return $this->getDir() . DS . $this->doc_file;
      }

      /**
       * Check of doc exists
       * @return type
       */
      public function docExists() {
          if($this->doc_type_id == 2){
            $file = $this->getFilePath();
            return file_exists($file);
          }
          return true;
      }

}
