<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class DocModuleConstants {

      const MOD_COMPANY_DOCS = 'company_docs';
      //resources constants
      const RES_COMPANY_DOCS = 'COMPANY_DOCS';
      //menu constants
      const MENU_DOC = 'MENU_DOC';

}
