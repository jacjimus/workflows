<?php if (ModulesEnabled::model()->isModuleEnabled(HrModuleConstants::MOD_HR)): ?>

        <?php if ($this->showLink(EmployeesModuleConstants::RES_EMPLOYEE_LIST)): ?>
           
            <li class="<?php echo $this->activeMenu === EmployeesModuleConstants::RES_EMPLOYEE_LIST ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('employees/employees/index') ?>"><i class="fa fa-lg fa-fw fa-male"></i> <span class="menu-item-parent"><?php echo Lang::t('Staff Information') ?></span></a>
      </li>
            <?php endif; ?>
        
    
<?php endif; ?>


