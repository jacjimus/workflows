<?php
/* @var $this HolidaysController */
/* @var $data Holidays */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('holiday_name')); ?>:</b>
    <?php echo CHtml::encode($data->holiday_name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('hol_day')); ?>:</b>
    <?php echo CHtml::encode($data->hol_day); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('hol_month')); ?>:</b>
    <?php echo CHtml::encode($data->hol_month); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('hol_year')); ?>:</b>
    <?php echo CHtml::encode($data->hol_year); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('recurring')); ?>:</b>
    <?php echo CHtml::encode($data->recurring); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
    <?php echo CHtml::encode($data->date_created); ?>
    <br />

    <?php /*
      <b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
      <?php echo CHtml::encode($data->created_by); ?>
      <br />

     */ ?>

</div>