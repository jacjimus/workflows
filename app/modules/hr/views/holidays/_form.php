<?php
/* @var $this HolidaysController */
/* @var $model Holidays */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'holidays-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'holiday_name'); ?>
		<?php echo $form->textField($model,'holiday_name',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'holiday_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hol_day'); ?>
		<?php echo $form->textField($model,'hol_day'); ?>
		<?php echo $form->error($model,'hol_day'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hol_month'); ?>
		<?php echo $form->textField($model,'hol_month'); ?>
		<?php echo $form->error($model,'hol_month'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hol_year'); ?>
		<?php echo $form->textField($model,'hol_year'); ?>
		<?php echo $form->error($model,'hol_year'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recurring'); ?>
		<?php echo $form->textField($model,'recurring'); ?>
		<?php echo $form->error($model,'recurring'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
		<?php echo $form->error($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->