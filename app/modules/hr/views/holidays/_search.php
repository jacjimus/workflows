<?php
/* @var $this HolidaysController */
/* @var $model Holidays */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'holiday_name'); ?>
		<?php echo $form->textField($model,'holiday_name',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hol_day'); ?>
		<?php echo $form->textField($model,'hol_day'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hol_month'); ?>
		<?php echo $form->textField($model,'hol_month'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hol_year'); ?>
		<?php echo $form->textField($model,'hol_year'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recurring'); ?>
		<?php echo $form->textField($model,'recurring'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->