<?php

/**
 * This is the model class for table "hr_relations".
 *
 * The followings are the available columns in table 'hr_relations':
 * @property integer $id
 * @property string $rel_name
 *
 * The followings are the available model relations:
 * @property Dependants[] $dependants
 */
class Relations extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Relations the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_relations';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('rel_name', 'required'),
            array('rel_name', 'length', 'max' => 145),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, rel_name', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'dependants' => array(self::HAS_MANY, 'Dependants', 'relationship_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'rel_name' => Lang::t('Rel Name'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function getSearchParams() {
        return array(
            array('rel_name', self::SEARCH_FIELD, true, 'OR'),
            'id',
        );
    }

}
