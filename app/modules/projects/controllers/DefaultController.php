<?php

class DefaultController extends ProjectsModuleController {

    public function init() {
        parent::init();
        $this->resourceLabel = Lang::t('Documents Manager');
        $this->resource = ManagerModuleConstants::RES_DOC;
        $this->activeTab = ManagerModuleConstants::TAB_GENERAL;
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'visarequest', 'immigration', 'forwardnote', 'noobjection', 'create', 'index', 'update', 'approve', 'loadform' , 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' Document');


        $doc = new Newdoc();
        $model_class_name = $doc->getClassName();

        if (isset($_POST[$model_class_name])) {
            $doc->attributes = $_POST[$model_class_name];
            if ($doc->Doc_template == 4)
                $doc->scenario = Newdoc::SCENARIO_VISA_REQUEST;
            if ($doc->Doc_template == 5):
                $doc->scenario = Newdoc::SCENARIO_IMMIGRATION;

                foreach ($_POST['Immigrant']['name'] As $item):
                    if (empty($item))
                        $doc->addError('name', 'Please enter Immigrant name');
                    if (empty($_POST['Immigrant']['td_no'][array_search($item, $_POST['Immigrant']['name'])]))
                        $doc->addError('td_no', 'Please enter Immigrant TD No');
                endforeach;
            endif;
            if ($doc->Doc_template == 6)
                $doc->scenario = Newdoc::SCENARIO_FORWARD_NOTE;

            if ($doc->Doc_template == 7)
                $doc->scenario = Newdoc::SCENARIO_NO_OBJECTION;

            $error_message = CActiveForm::validate($doc);

            $error_message_decoded = CJSON::decode($error_message);
            if (isset($_POST['forward']))
                $doc->Status = Newdoc::STATUS_PENDING;
            else
                $doc->Status = Newdoc::STATUS_SAVED;
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $doc->save(FALSE);
                if ($doc->Doc_template == 5):
                    $data = [];
                    foreach ($_POST['Immigrant']['name'] As $item):
                        array_push($data, array('name' => $item, 'td_no' => $_POST['Immigrant']['td_no'][array_search($item, $_POST['Immigrant']['name'])], 'doc_id' => $doc->DocID));
                    endforeach;

                    Yii::app()->db->schema->commandBuilder->createMultipleInsertCommand('tbl_immigrants', $data)->execute();
                endif;
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('default/index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $doc,
                ), FALSE, TRUE);
    }

    public function actionLoadform() {
        $type = Yii::app()->request->getPost('doc_type');
        $id = Doc::model()->getScalar("id", "doc_alias LIKE '$type'");
        $this->renderPartial("ajax/" . $type, array('model' => new Newdoc, 'id' => $id), false, true);
    }

    public function actionImmigration($id) {

        $this->hasPrivilege();
        $model = Doc::model()->loadModel(Newdoc::model()->getScalar('Doc_template', "DocID = $id"));
        $data = Newdoc::model()->loadModel($id);
        $this->pageTitle = $data->Reference;
        $file = $model->getFilePath();
        //.. get the content of the requested file
        //$content = file_get_contents($file);
        $file_name = Common::cleanString($model->doc_file);

        $pdf = Yii::app()->pdfFactory->getFPDI(); //other options like above  
        $pdf->SetCreator(PDF_CREATOR);
        $name = Person::model()->getScalar('full_names', "id = " . Yii::app()->user->id);
        $pdf->SetAuthor($name);

        //import the template
        $pdf->setSourceFile($file);
        $tplidx = $pdf->importPage(1);
        $pdf->addPage();
        $pdf->useTemplate($tplidx, 10, 10, 200);
        $this->header($pdf);
        $pdf->SetFont('Times', "", "11");

        //Reference
        $pdf->SetXY(39, 11);
        $pdf->SetTextColor(37, 32, 97);
        $pdf->write(105, $data->Reference);
        // Date
        $pdf->SetXY(147, 11);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, date('dS, F Y', strtotime($data->CreateDate)));

        $immigrants = Yii::app()->db->createCommand()
                ->select('name,td_no,doc_id')
                ->from('tbl_immigrants')
                ->where('doc_id = :id', array(':id' => $data->DocID))
                ->queryAll();
        $j = 0;
        $i = 1;
        foreach ($immigrants as $imm):
            $pdf->SetXY(40, (69 + $j));
            $pdf->write(105, $i . ". " . ucfirst($imm['name']));
            $pdf->SetXY(90, (69 + $j));
            $pdf->write(105, "TDR NO: " . $imm['td_no']);
            $j+=4;
            $i++;
        endforeach;


        $pdf->Output();
        //.. send appropriate headers
        header("Content-type: application/pdf");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file);
        //echo $content;
    }

    public function actionVisaRequest($id) {

        $this->hasPrivilege();
        $model = Doc::model()->loadModel(Newdoc::model()->getScalar('Doc_template', "DocID = $id"));
        $data = Newdoc::model()->loadModel($id);
        $this->pageTitle = $data->Reference;

        $file = $model->getFilePath();
        //.. get the content of the requested file
        //$content = file_get_contents($file);
        $file_name = Common::cleanString($model->doc_file);

        $pdf = Yii::app()->pdfFactory->getFPDI(); //other options like above  
        $pdf->SetCreator(PDF_CREATOR);
        $name = Person::model()->getScalar('full_names', "id = " . Yii::app()->user->id);
        $pdf->SetAuthor($name);

        //import the template
        $pdf->setSourceFile($file);
        $tplidx = $pdf->importPage(1);
        $pdf->addPage();
        $pdf->useTemplate($tplidx, 10, 10, 200);
        $this->header($pdf);
        $pdf->SetFont('Times', "", "11");

        //Reference
        $pdf->SetXY(42, 5);
        $pdf->SetTextColor(37, 32, 97);
        $pdf->write(105, $data->Reference);
        // Embassy name
        $pdf->SetXY(76, 20);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Embassy_name);
        // Visa type
        $pdf->SetXY(40, 36);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Type);
        // Salutation
        $pdf->SetXY(75, 36);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Salutation);
        // Applicant Name
        $pdf->SetXY(85, 36);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Applicant_name);
        // Passport no
        $pdf->SetXY(85, 44);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Passport_no);
        // Occupation
        $pdf->SetXY(40, 52);
        $pdf->write(105, $data->Occupation);
        // Salutation
        $pdf->SetXY(40, 60);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Salutation);
        // Applicant Name
        $pdf->SetXY(50, 60);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Applicant_name);
        // Destination
        $pdf->SetXY(40, 67);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Destination);
        // Purpose
        $pdf->SetXY(37, 132);
        //$pdf->SetTextColor(255, 0, 0);
        //$pdf->write(105, strip_tags(str_replace("\n"," ", $data->Purpose)));

        $pdf->MultiCell(140, 5, $this->getPurpose(strip_tags(str_replace("\n", " ", $data->Purpose))), 0, 'L', false, 0, '', '', true, 0, false, true, 0, 'M', true);
        //$pdf->Ln(10);
        // Date of Entry/Exit
        $pdf->SetXY(55, 99);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Date_departure);
        // Embassy name
        $pdf->SetXY(55, 145);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Embassy_name);
        // Date
        $pdf->SetXY(45, 169);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, date('dS, F Y', strtotime($data->CreateDate)));
        // Organization Name
        $pdf->SetXY(110, 169);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COMPANY_NAME));


        $pdf->Output();
        //.. send appropriate headers
        header("Content-type: application/pdf");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file);
        //echo $content;
    }

    public function actionNoobjection($id) {

        $this->hasPrivilege();
        $model = Doc::model()->loadModel(Newdoc::model()->getScalar('Doc_template', "DocID = $id"));
        $data = Newdoc::model()->loadModel($id);
        $this->pageTitle = $data->Reference;

        $file = $model->getFilePath();
        //.. get the content of the requested file
        //$content = file_get_contents($file);
        $file_name = Common::cleanString($model->doc_file);
       // $file_name = Common::cleanString($model->doc_file);

        $pdf = Yii::app()->pdfFactory->getFPDI(); //other options like above  
        $pdf->SetCreator(PDF_CREATOR);
        $name = Person::model()->getScalar('full_names', "id = " . Yii::app()->user->id);
        $pdf->SetAuthor($name);

        //import the template
        $pdf->setSourceFile($file);
        $tplidx = $pdf->importPage(1);
        $pdf->addPage();
        $pdf->useTemplate($tplidx, 0, 0, 200);
        
        $this->header($pdf);
        // Embassy name
         $pdf->SetFont('Times', "", "11");
        //Reference
        $pdf->SetXY(42, 59);
        $pdf->SetTextColor(37, 32, 97);
        $pdf->write(0, $data->Reference);
        // Embassy name
        $pdf->SetXY(85,77);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, $data->Embassy_name);

        // Salutation
        $pdf->SetXY(75, 88);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, $data->Salutation);
        // Applicant Name
        $pdf->SetXY(82, 88);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, $data->Applicant_name);
        // Passport no
        $pdf->SetXY(40, 95);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, $data->Passport_no);
        // Destination
        $pdf->SetXY(125, 95);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, $data->Destination);
        // College name
        $pdf->SetXY(69, 100);
        $pdf->write(0, $data->School_name);

        // Course type
        $pdf->SetXY(69, 106);
        $pdf->write(0, $data->Degree_type);



        // academic year
        $pdf->SetXY(40, 112);
        $pdf->write(0, $data->Academic_year);

        // Embassy name
        $pdf->SetXY(112, 148);
        $pdf->write(0, $data->Embassy_name);

        // Date
        $pdf->SetXY(121, 166);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, date('dS, F Y', strtotime($data->CreateDate)));

        // Date
        // Organization Name
        $pdf->SetXY(40, 184);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(0, Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COMPANY_NAME));


        $pdf->Output();
        //.. send appropriate headers
        header("Content-type: application/pdf");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file);
        //echo $content;
    }

    public function actionForwardnote($id) {

        $this->hasPrivilege();
        $model = Doc::model()->loadModel(Newdoc::model()->getScalar('Doc_template', "DocID = $id"));
        $data = Newdoc::model()->loadModel($id);
        $this->pageTitle = $data->Reference; 
        
        $file = $model->getFilePath();
        //.. get the content of the requested file
        //$content = file_get_contents($file);
        $file_name = Common::cleanString($model->doc_file);

        $pdf = Yii::app()->pdfFactory->getFPDI(); //other options like above  
        $pdf->SetCreator(PDF_CREATOR);
        $name = Person::model()->getScalar('full_names', "id = " . Yii::app()->user->id);
        $pdf->SetAuthor($name);

        //import the template
        $pdf->setSourceFile($file);
        $tplidx = $pdf->importPage(1);
        $pdf->addPage();
        $pdf->useTemplate($tplidx, 10, 10, 200);
        $pdf->SetFont('Times', "", "11");

        //Reference
        $pdf->SetXY(46, 10);
        $pdf->SetTextColor(37, 32, 97);
        $pdf->write(105, $data->Reference);
        // Embassy name
        $pdf->SetXY(100, 33);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Embassy_name);
        // Letter Ref
        $pdf->SetXY(40, 48);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->letter_ref_no);
        // Letter date
        $pdf->SetXY(110, 48);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->letter_date);
        // Ministry Name
        $pdf->SetXY(40, 56);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->ministry_name);
        // Embassy name
        $pdf->SetXY(50, 64);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Embassy_name);
        // Embassy name
        $pdf->SetXY(120, 119);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, $data->Embassy_name);
        // Date
        $pdf->SetXY(122, 150);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, date('dS, F Y', strtotime($data->CreateDate)));
        // Organization Name
        $pdf->SetXY(40, 166);
        //$pdf->SetTextColor(255, 0, 0);
        $pdf->write(105, Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COMPANY_NAME));


        $pdf->Output();
        //.. send appropriate headers
        header("Content-type: application/pdf");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file);
        //echo $content;
    }

    protected function getPurpose($content) {

        // Get a random sentence
        $nb = explode(" ", $content);
        $s = '';
        for ($i = 1; $i < count($nb); $i++)
            $s .= $nb[$i] . ' ';
        return substr($s, 0, -1);
    }

    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = Newdoc::model()->loadModel($id);
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {

            $model->attributes = $_POST[$model_class_name];
            if ($model->Doc_template == 4)
                $model->scenario = Newdoc::SCENARIO_VISA_REQUEST;
            if ($model->Doc_template == 5):
                $model->scenario = Newdoc::SCENARIO_IMMIGRATION;

                foreach ($_POST['Immigrant']['name'] As $item):
                    if (empty($item))
                        $model->addError('name', 'Please enter Immigrant name');
                    if (empty($_POST['Immigrant']['td_no'][array_search($item, $_POST['Immigrant']['name'])]))
                        $model->addError('td_no', 'Please enter Immigrant TD No');
                endforeach;
            endif;
            if ($model->Doc_template == 6)
                $model->scenario = Newdoc::SCENARIO_FORWARD_NOTE;

            if ($model->Doc_template == 7)
                $model->scenario = Newdoc::SCENARIO_NO_OBJECTION;
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save();
                if ($model->Doc_template == 5):
                    $data = [];
                    Yii::app()->db->createCommand()
                            ->delete('tbl_immigrants' , "doc_id = $model->DocID ");
                            
                    foreach ($_POST['Immigrant']['name'] As $item):
                        array_push($data, array('name' => $item, 'td_no' => $_POST['Immigrant']['td_no'][array_search($item, $_POST['Immigrant']['name'])], 'doc_id' => $model->DocID));
                    endforeach;

                    Yii::app()->db->schema->commandBuilder->createMultipleInsertCommand('tbl_immigrants', $data)->execute();
                endif;
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('default/index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
            'id' => $model->Doc_template
                ), FALSE, TRUE);
    }

    public function actionIndex($doc_template = null) {
        $this->hasPrivilege();
        $condition = $doc_template === null ? "" : " AND Doc_template = ". $doc_template;
        $this->activeMenu = ManagerModuleConstants::MENU_DOC_MANAGER;
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $searchModel = Newdoc::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'CreateDate Desc' , " Author = ". Yii::app()->user->id . $condition);
        $this->render('manage', array(
            'dtp' => $doc_template,
            'model' => $searchModel,
            
        ));
    }

    public function actionApprove($id) {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeMenu = self::MENU_APPROVE;
        $this->pageTitle = Lang::t("Approve " . Common::pluralize($this->resourceLabel));
        $model = Newdoc::model()->loadModel($id);
        $model_class_name = $model->getClassName();
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('default/index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('approve', array(
            'model' => $model,
                ), FALSE, TRUE);
    }
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        Newdoc::model()->loadModel($id)->delete();
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }
    
    protected function header($pdf = null )
    {
        $logo = Yii::app()->theme->baseUrl ."/images/logo-pdf.png";
        $pdf->Cell( 40, 40, $pdf->Image($logo, $pdf->GetX() + 75, $pdf->GetY(), 20.78), 0, 0, 'R', false );
        //Header
        $pdf->SetXY(55, 26);
        $pdf->SetTextColor(160, 160, 170);
        $pdf->SetFont('Times', "B", "14");
        $pdf->write(0, Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COMPANY_NAME));
        
//Header - Nairobi
        $pdf->SetXY(85, 31);
        $pdf->SetTextColor(150, 160, 160);
        $pdf->SetFont('Times', "", "10");
        $pdf->write(0, 'Nairobi - Kenya');
        $pdf->SetDrawColor(65,137,221);
        $pdf->Line(20, 37, 200-20, 37);
    }

}
