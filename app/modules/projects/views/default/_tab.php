<?php
$data = Doc::model()->getData("*" , "doc_type_id=1");
?>

<div class="list-group my-list-group">
    <a href="<?php echo $this->createUrl('index') ?>" class="list-group-item">
        <h4 class="list-group-item-heading"><?php echo Lang::t('All Documents') ?> <span class="badge"><?php echo Newdoc::model()->getDocCount() ?></span></h4>
        <p class="list-group-item-text"><?php echo Lang::t('Browse all documents'); ?></p>
    </a>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $row): ?>
            <a href="<?php echo $this->createUrl('index', array('doc_template' => $row['id'])) ?>" class="list-group-item">
                <h4 class="list-group-item-heading"><?php echo CHtml::encode($row['name']) ?>  <span class="badge"><?php echo Newdoc::model()->getDocCount($row['id']) ?></span></h4>
                <p class="list-group-item-text"><?php echo CHtml::encode($row['description']) ?></p>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>