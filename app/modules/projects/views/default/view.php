<?php
$this->breadcrumbs = array(' Manage Documents' => 'manage',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
        
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                <?php $this->renderPartial('_view', array('model' => $model)); ?>
                        </div>
                </div>
        </div>
