<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('manager.views.default._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-fw fa-file"></i>
                    <?php echo CHtml::encode($this->pageTitle); ?>
                    
                </h1> 
            </div>
        </div>
        <?php $this->renderPartial('_grid', array('model' => $model ,'dtp' => $dtp)); ?>
    </div>
</div>