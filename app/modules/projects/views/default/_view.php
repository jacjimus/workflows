<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <i class="fa fa-anchor"></i> <?php echo Lang::t('Preview document details') ?>

        </h4>
    </div>
    <div id="exam_info" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="detail-view">
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        
                        array(
                            'name' => 'Title',
                           
                        ),
                        array(
                            'name' => 'Reference',
                           
                        ),
                        array(
                            'name' => 'Subject',
                           
                        ),
                        array(
                            'name' => 'DocType',
                            'type' => 'raw',
                            'value' => Doctypes::model()->get($model->DocType , "Description")
                        ),
                        
                        array(
                            'name' => 'CreateDate',
                            'type' => 'raw',
                            'value' =>  MyYiiUtils::formatDate($model->CreateDate),
                        ),
                        
                        
                        array(
                            'name' => 'Status',
                        ),
                        
                        
                )
                    )
                        );
                ?>
            </div>
            <div class="detail-view">
                <?php echo $model->Content ?>
            </div>
           <?php if ($this->showLink($this->resource)): ?>
            <div class="clearfix form-actions">
        <div class="col-lg-offset-3 col-lg-2">
            <a href="#" class="btn btn-default show-colorbox" ><i class="icon-eye-open bigger-110"></i> <?php echo Lang::t('Preview Document') ?></a>
        </div>
        <div class="col-lg-offset-1 col-lg-2">
            <button class="btn btn-success" name="forward" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Approve Document') ?></button>
        </div>
            </div>
            <?php endif; ?>
        </div>
        </div>
     </div>

