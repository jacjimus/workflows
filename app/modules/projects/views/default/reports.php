<?php
/* @var $this SynchOrderController */

$this->breadcrumbs = array(
    'Mothers Module' => 'manage',
    'Mothers Reports',
);
?>
<div class="widget-box transparent">
    <div class="widget-header">
        <h4><?php echo CHtml::encode($this->pageTitle); ?><i class="icon-bar-chart"></i></h4>
    </div> 
        
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Daily Report</a></li>
        <li><a href="#tabs-4">Weekly Report</a></li>
        <li><a href="#tabs-2">Monthly Report</a></li>

    </ul>
    <div id="tabs-1">


        <?php
        $model = new Antenatal();

        $start = "";
        $end = "";


        $form = $this->beginWidget('CActiveForm', array(
            'method' => 'post',
        ));
        ?>
        <table style="width: 90%"><tr>
                <td style="width: 150px; padding: 5px 0px 5px 30px; ">

                    <?php echo $form->label($model, 'Issuance Date'); ?>
                </td>
                <td style="width: 150px;">

                    <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'Prevdate',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',
                            'showAnim' => 'fold',
                            'changeMonth' => true,
                            'changeYear' => true,
                        ),
                        'htmlOptions' => array(
                            'style' => 'width:150px;',
                            'value' => date("Y-m-d")
                        ),
                    ));
                    ?>
                </td>



                <td style="width: 150px; padding: 0px 20px 0px 20px;">

<?php echo $form->labelEx($model , 'Preventive Care'); ?>
                </td>
                <td style="width: 150px;">
<?php
echo $form->dropDownList($model, 'Previd', Preventive::model()->getListData('Previd' , 'Prevname' , array(""=> "[Select one]" ))
                    
        , array('empty' => '[Default]', 'style' => 'width:150px;'));
?>
                    <?php echo $form->error($model, 'Previd'); ?>

                </td>

</tr>
            <tr>

                <td style="width: 150px; padding: 0px 20px 0px 20px;">

<?php echo $form->label($model , 'Issuance Status'); ?>
                </td>
                <td>
<?php

echo $form->dropDownList($model, 'Status', array(
    Antenatal::STATUS_ANTENATAL_GIVEN => Antenatal::STATUS_ANTENATAL_GIVEN,
    Antenatal::STATUS_ANTENATAL_PENDING => Antenatal::STATUS_ANTENATAL_PENDING,
        ), array('empty' => '[Default]', 'style' => 'width:150px;', 'required' => true));
?>
                    <?php echo $form->error($model, 'Issuance Status');  ?>

                </td>
                
                 <td style="width: 150px; padding: 0px 20px 0px 20px;">

<?php echo $form->label($model , 'Smsnotifystatus'); ?>
                </td>
                <td>
<?php

echo $form->dropDownList($model, 'Smsnotifystatus', array(
    Antenatal::STATUS_SMS_NOTIFY_NO => Antenatal::STATUS_SMS_NOTIFY_NO,
    Antenatal::STATUS_SMS_NOTIFY_YES => Antenatal::STATUS_SMS_NOTIFY_YES,
        ), array('empty' => '[Default]', 'style' => 'width:150px;', 'required' => true));
?>
                    <?php echo $form->error($model, 'Smsnotifystatus');  ?>

                </td>
            </tr>
            <tr>

                <td style="vertical-align: bottom; text-align: center" colspan="3">


                    <div class="row buttons" style="padding: 5px 0px 5px 60px; " id="loading">
<?php echo CHtml::Button('Generate Report', array('id' => 'Daily', 'onclick' => 'getADReports()')); ?>
                    </div>
                </td>
            </tr>

        </table>	
                        <?php $this->endWidget(); ?>
        <div id="daily_reports">
            <!--   Daily Reports will be displayed here after query :) :) :)-->
        </div>


    </div>
    <div id="tabs-4">


<?php
$start = "";
$end = "";


$form = $this->beginWidget('CActiveForm', array(
    'method' => 'post',
        ));
?>
        <table style="width: 80%"><tr>
                <td style="width: 70px; padding: 5px 0px 5px 30px; ">

        <?php echo $form->label($model, 'Start Date'); ?>
                </td>
                <td style="width: 150px;">

<?php
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'Regdate',
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'showAnim' => 'fold',
        'changeMonth' => true,
        'changeYear' => true,
    ),
    'htmlOptions' => array(
        'style' => 'width:100px;',
        'value' => date("Y-m-d")
    ),
));
?>
                </td>


                <td style="width: 100px; padding: 5px 0px 5px 30px; ">

                    <?php echo $form->label($model, 'End Date'); ?>
                </td>
                <td style="width: 150px;">

                    <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'Regdate',
                        'options' => array(
                            'dateFormat' => 'yy-mm-dd',
                            'showAnim' => 'fold',
                            'changeMonth' => true,
                            'changeYear' => true,
                        ),
                        'htmlOptions' => array(
                            'style' => 'width:150px;',
                            'value' => date("Y-m-d")
                        ),
                    ));
                    ?>
                </td>



                <td style="padding: 0px 20px 0px 20px;">

                    <?php echo $form->labelEx($model, 'w_short_code'); ?>
                </td>
                <td>
                    <?php
                    // echo $form->dropDownList($model, 'w_short_code', $options, array('empty' => 'All Shortcodes', 'style' => 'width:150px;'));
                    ?>
                    <?php echo $form->error($model, 'Regdate'); ?> </td>
                <td style="width: 150px; padding: 0px 20px 0px 20px;">

                    <?php echo $form->labelEx($model, 'Status'); ?>
                </td>
                <td>
                    <?php
                    echo $form->dropDownList($model, 'Status', array(1 => 'Subscription', 2 => 'Un-Subscription'), array('empty' => '--Select--', 'style' => 'width:150px;'));
                    ?>
<?php echo $form->error($model, 'w_status'); ?>

                </td>
            </tr>
            <tr>
                <td style="vertical-align: bottom;" colspan="3">


                    <div class="row buttons" style="padding: 5px 0px 5px 60px; ">
                    <?php echo CHtml::Button('Generate Report', array('id' => 'Weekly', 'onclick' => 'getWeeklyReports()')); ?></div>
                </td>
            </tr>

        </table>	
                    <?php $this->endWidget(); ?>
        <div id="weekly_reports">
            <!--   Daily Reports will be displayed here after query :) :) :)-->
        </div>


    </div>
    <div id="tabs-2">
        <p>


<?php
$start = "";
$end = "";


$form = $this->beginWidget('CActiveForm', array(
    'method' => 'post',
        ));
?>
        <table style="width: 90%">
            <tr>
                <td><?php echo $form->label($model, 'Prevdate'); ?></td><td>

<?php
$last = date('Y') - 2;
$now = date('Y');
$arr = array();

for ($i = $last; $i <= $now; $i++) {
    $arr += array($i => $i);
}
//var_dump($arr);
echo $form->dropDownList($model, 'Prevdate', CHtml::encodeArray($arr));
?></td>
                <td>

            <?php echo $form->label($model, 'Prevdate'); ?></td>

                <td>

            <?php
            echo $form->dropDownList($model, 'Prevdate', CHtml::encodeArray(Yii::app()->locale->getMonthNames()));
            ?>
                </td>
                <td style="padding: 0px 20px 0px 20px;">

                    <?php echo $form->labelEx($model, 'Prevdate'); ?>
                </td>
                <td>
                    <?php
                    // echo $form->dropDownList($model, 'm_short_code', $options, array('empty' => 'All Shortcodes', 'style' => 'width:150px;'));
                    ?>
                    <?php echo $form->error($model, 'Status'); ?> </td>
                <td>
                    <?php echo $form->label($model, 'Status'); ?></td>   
                </td>
                <td>
                    <?php echo $form->dropDownList($model, 'Status', array(1 => 'Subscription', 2 => "Un-Subscription")) ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3" style="padding: 10px 20px 0px 20px;">


                    <div class="row buttons">
                    <?php echo CHtml::Button('Generate Report', array('id' => 'Monthly', 'onclick' => 'getMonthlyReports()')); ?></div>
                </td>
            </tr></table>	
                    <?php $this->endWidget(); ?>
        <div id="monthly_reports">
            <!--    Reports will be displayed here after query :) :) :)-->
        </div>
        </p>
    </div>


</div>
</div>
</div>
</div>
</div>


                    <?php $this->renderPartial('ajax/sync') ?>
