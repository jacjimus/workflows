<div class="alert hidden" id="my-modal-notif"></div>



<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Reference', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, "Reference", array('class' => "form-control")); ?>
        <?php echo CHtml::activeHiddenField($model, "Doc_template", array('value' => $id)); ?>


        <?php echo CHtml::error($model, 'Reference') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Embassy_name', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Embassy_name', array('class' => "form-control")); ?>

        <?php echo CHtml::error($model, 'Embassy_name') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Type', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeDropDownList($model, 'Type', ['Entry' => 'Entry', 'Exit' => 'Exit'], array('prompt' => '[Type of Visa]', 'class' => 'form-control')); ?>

        <?php echo CHtml::error($model, 'Type') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Salutation', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeDropDownList($model, 'Salutation', ['Dr.' => 'Dr.', 'Imam' => 'Imam', 'Rev.' => 'Rev.', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms'], array('prompt' => '[Salutation]', 'class' => 'form-control')); ?>


        <?php echo CHtml::error($model, 'Salutation') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Applicant_name', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Applicant_name', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'Applicant_name') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Passport_no', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Passport_no', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'Passport_no') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Occupation', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Occupation', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'Occupation') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Destination', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Destination', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'Destination') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Date_departure', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Date_departure', array('class' => "form-control datepicker")); ?>


        <?php echo CHtml::error($model, 'Date_departure') ?>
    </div>
</div>


<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Purpose', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-12">
        <?php echo CHtml::error($model, 'Purpose') ?>
        <?php echo CHtml::activeTextArea($model, 'Purpose', array('class' => 'redactor', 'rows' => 3, 'size' => 100)); ?>
    </div>  
</div>  
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-success" name="forward" type="submit" ><i class="icon-forward bigger-110"></i> <?php echo Lang::t('Save Document') ?></button>
</div>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 50,
        'convertDivs' => true,
        'cleanup' => TRUE,
        'paragraphy' => false,
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));
?>