<?php $theme = Yii::app()->theme->baseUrl; ?>
<div class="alert hidden" id="my-modal-notif"></div>



<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Reference', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, "Reference", array('class' => "form-control")); ?>
        <?php echo CHtml::activeHiddenField($model, "Doc_template", array('value' => $id)); ?>


        <?php echo CHtml::error($model, 'Reference') ?>
    </div>
</div>

<table id="immigrants"  class="atable table-responsive">
    <tr><th colspan="2">Enter Immigrant details</th>
        <th ><input type="button" id="addRow" value="Add Row" class="alternativeRow"/></th></tr>
    <tr><td>Name</td>
        <td>TD No.</td>
        <td></td></tr>
    <?php if(!$model->isNewRecord): ?>
        <?php
      $immigrants = Yii::app()->db->createCommand()
              ->select("imm_id,name,td_no")
              ->from("tbl_immigrants")
              ->where("doc_id=:id" , array(':id' => $model->DocID))
              ->queryAll();
      foreach($immigrants As $imm):?>
    <tr><td><input name="Immigrant[imm_id][]" value="<?=$imm['imm_id']?>" type="hidden" />
            <input name="Immigrant[name][]" value="<?=$imm['name']?>" type="text" size="40" required/></td>
        <td><input type="text" name="Immigrant[td_no][]" value="<?=$imm['td_no']?>" size="20" required/></td>
        <td><a href="#"><i class="fa fa-trash-o fa-1x text-danger delRow" title="delete row" onclick="$(this).closest('tr').remove();">&nbsp;</i></a></td></tr>
      <?php
      endforeach;
      else: ?>
    <tr><td><input name="Immigrant[name][]" type="text" size="40" required/></td>
        <td><input type="text" name="Immigrant[td_no][]" value="ESR/" size="20" required/></td>
        <td></td></tr>
    <?php 
        endif; ?>
</table>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-success" name="forward" type="submit" ><i class="icon-forward bigger-110"></i> <?php echo Lang::t('Save Document') ?></button>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#addRow').on('click', function () {
            $('#immigrants tr:last').after("<tr><td><input name=\"Immigrant[name][]\" type=\"text\" size=\"40\" required/></td><td><input type=\"text\" name=\"Immigrant[td_no][]\" value=\"ESR/\" size=\"20\" required/></td><td><a href=\"#\"><i class=\"fa fa-trash-o fa-1x text-danger delRow\" title=\"delete row\" onclick=\"$(this).closest('tr').remove();\">&nbsp;</i></a></td></tr>");
        });


    });
</script>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/css/table-add-row.css');
