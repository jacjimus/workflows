<?php
/* @var $this DocTypesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Doc Types',
);

$this->menu=array(
	array('label'=>'Create DocTypes', 'url'=>array('create')),
	array('label'=>'Manage DocTypes', 'url'=>array('admin')),
);
?>

<h1>Doc Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
