
<?php
if (ModulesEnabled::model()->isModuleEnabled(ProjectsModuleConstants::MOD_PROJECTS)): ?>
      <?php if ($this->showLink(ProjectsModuleConstants::RES_PROJECTS)): ?>
        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-tasks"></i> <span class="menu-item-parent"><?php echo Lang::t('Project Management') ?></span></a>
            <ul>
                <?php if($this->showLink(ProjectsModuleConstants::RES_PROJECT_ONGOING)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_CATEGORY ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('projects/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Ongoing Projects') ?></span></a></li>
                <?php endif; ?>
                <?php if($this->showLink(ProjectsModuleConstants::RES_PROJECT_TASK_BY_DATE)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_MANAGER ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Project tasks by date') ?></span></a></li>
            <?php endif; ?>
                <?php if($this->showLink(ProjectsModuleConstants::RES_PROJECT_TASK_BY_PERSON)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_MANAGER ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Project tasks by person') ?></span></a></li>
            <?php endif; ?>
                <?php if($this->showLink(ProjectsModuleConstants::RES_PROJECT_PENDING_TASKS_REPORTS)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_MANAGER ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Pending tasks') ?></span></a></li>
            <?php endif; ?>
                <?php if($this->showLink(ProjectsModuleConstants::RES_PROJECT_COMPLETED_TASK_REPORTS)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_MANAGER ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Completed tasks') ?></span></a></li>
            <?php endif; ?>
            </ul>
        </li>
            <?php endif; ?>
<?php endif; ?>