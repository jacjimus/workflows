<?php

/**
 * Home controller
 * @author John<johnerick8@gmail.com>
 */
class ReportsController extends Controller {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index','training','costs'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex($qualification = null, $company = null) {
        //$this->redirect(array('inv/default/index'));
		$this->activeTab = TrainingModuleConstants::TAB_TRAINING_TRAINERS;
        $this->pageTitle = Lang::t('Reports');
		$model=new ReportForm();
	
        $searchModel = Employees::model()->searchModel(array('company_name'=>$company,'qualification'=>$qualification), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id,fullname', array());
        $this->render('index', array(
            'model' => $searchModel,
            'company'=>$company,
            'qualification'=>$qualification
        ));
		
    }

	 /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionTraining() {
        //$this->redirect(array('inv/default/index'));
		$this->activeTab = TrainingModuleConstants::TAB_TRAINING_TYPES;
        $this->pageTitle = Lang::t('Reports');
		$model=new ReportForm();
		if(isset($_POST['ReportForm'])){
			$model->attributes=$_POST['ReportForm'];
			$condition='1=1';
			if($model->date_from!=null && $model->date_to!=null){
				$condition.=" AND `start_date` between '".$model->date_from."' and '".$model->date_to."' AND  `end_date` between '".$model->date_from."' and '".$model->date_to."'";
			}elseif($model->date_from!=null && $model->date_to==null){
				$condition.=" AND `start_date` >='".$model->date_from."' AND `end_date` >='".$model->date_from."'";
			}elseif($model->date_from==null && $model->date_to!=null){
				$condition.=" AND `start_date` <='".$model->date_to."' AND `end_date` >='".$model->date_from."'";
			}
			if($model->country_id !=null){
				$condition .=" AND `country_of_inst` = '".$model->country_id."'";
			}if($model->skill_id !=null){
				$condition .=" AND `skill_id` = '".$model->skill_id."'";
			}
			
			
		$models = Empskills::model()->searchModel(array(), 10000000000, 'id',$condition);
		$posted=1;
		
		}
		/*if(isset($posted)&&$posted==1)
			$this->render('training', array('model'=>$model,'models'=>$models,'posted'=>$posted));

		else
        $this->render('training', array('model'=>$model));*/
        $this->render('costs');
    }
	
	public function actionCosts(){
		$this->activeTab = TrainingModuleConstants::TAB_TRAINING_READING_MATERIALS;
        $this->pageTitle = Lang::t('Training Costs Reports');
		$this->render('costs');
	}

}
