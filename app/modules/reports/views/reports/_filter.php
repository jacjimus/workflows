<?= CHtml::form(Yii::app()->createUrl($this->route, $this->actionParams), 'get', ['class' => '']) ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Lang::t('Filter By:') ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <?= CHtml::label('Company', "", ['class' => 'control-label']) ?>
                <br/>
                <?= CHtml::textField('company', $company, ['class' => 'form-control']); ?>
            </div>

           
            <div class="col-md-2">
                <?= CHtml::label('Qualification', "", ['class' => 'control-label']) ?>
                <br/>
                <?= CHtml::textField('qualification', $qualification, ['class' => 'form-control']); ?>
            </div>
			<div class="col-md-1">
                <br/>
                <button class="btn btn-primary" type="submit"><?= Lang::t('Go') ?></button>
            </div>
            <div class="col-md-1">
                <br/>
                <a class="btn btn-default"
                   href="<?= Yii::app()->createUrl($this->route) ?>"><?= Lang::t('Clear') ?></a>
            </div>
        </div>
    </div>
</div>
<?= CHtml::endForm() ?>