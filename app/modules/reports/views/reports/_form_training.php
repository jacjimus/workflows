<?php
if (!isset($label_size)):
    $label_size = 2;
endif;
if (!isset($input_size)):
    $input_size = 8;
endif;
$label_class = "col-md-{$label_size} control-label";
$input_class = "col-md-{$input_size}";
$half_input_size = $input_size / 2;
$half_input_class = "col-md-{$half_input_size}";
?>
<?php
$form_id = 'employees-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo CHtml::encode($this->pageTitle) ?></h3>
    </div>

    <div class="panel-body">
        <?php echo CHtml::errorSummary($model, ""); ?>
        <div class="table-responsive">
         

<table class="table table-condensed"><tr style="background-color:inherit;background-image: none">
<th>Training Date From</th>
<th>Training Date to</th>
<th>Country of Training</th>
<th>skill</th>
<th>Search</th>
</tr>
<tr><td>
                <?php echo CHtml::activeTextField($model, 'date_from', array('class' => 'form-control show-datepicker')); ?>
               
            </td><td>
                <?php echo CHtml::activeTextField($model, 'date_to', array('class' => 'form-control show-datepicker')); ?>
             
            </td><td>
        <?php echo CHtml::activeDropDownList($model, 'skill_id', Skills::model()->getListData('id', 'skill'), array('class' => 'form-control')); ?>&nbsp;&nbsp;
      </td><td>
        <?php echo CHtml::activeDropDownList($model, 'skill_id', MeCountry::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>&nbsp;&nbsp;
      </td><td>
        <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-ok"></i>
            <?php echo Lang::t('Create Report') ?>
        </button>
    </td>
	</tr>
	</table>
	</div>

    </div>

</div>
<?php $this->endWidget(); ?>


