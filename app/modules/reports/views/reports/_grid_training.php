<?php
$grid_id = 'empskills-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<br/>
<?php
$this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'rowCssClassExpression' => '',
    'columns' => array(
		array('name'=>'Employee',
			   'value'=>'$data->getEmployee()'
	    ),
		array('name' => 'Country of Training',
            'value' => '$data->getCountry()'
			),
        array(
            'name' => 'skill_id',
            'value' => 'Skills::model()->get($data->skill_id,"skill")',
        ),
		
        'start_date',
        'end_date',
        array(
            'name' => 'proficiency_id',
            'value' => 'Proficiency::model()->get($data->proficiency_id,"prof_name")',
        ),
		'comp_not_used',
		'comp_used',
		'competencies',
		'certifying_body',
		'certification'
      
    ),
));
?>


