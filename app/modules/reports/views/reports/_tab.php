
<ul class="nav nav-tabs order-tabs">
    <li class="<?php echo $this->activeTab === TrainingModuleConstants::TAB_TRAINING_TRAINERS ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('reports/index') ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Employee Report') ?></span></a></li>
    <li class=" <?php echo $this->activeTab === TrainingModuleConstants::TAB_TRAINING_TYPES ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('reports/training') ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Training Report') ?></span></a></li>
    <li class=" <?php echo $this->activeTab === TrainingModuleConstants::TAB_TRAINING_READING_MATERIALS ? 'active' : '' ?>"><a href="<?php echo $this->createUrl('reports/costs') ?>"><span><i class="fa fa-circle text-success"></i> <?php echo Lang::t('Training Costs Report') ?></span></a></li>
    

    <li class="pull-right">
        <a href="<?php echo $this->createUrl('reports/index') ?>"><i class="fa fa-angle-double-left"></i> <?php echo Lang::t('Back to Reports') ?></a>
    </li>
</ul>