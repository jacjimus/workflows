<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
<?php $this->renderPartial('_tab'); ?>

    <div class="col-md-12">
	 <div class="well well-lights">
	
        <div class="row">
                 <?php   $this->renderPartial('_filter', ['company' => $company, 'qualification' => $qualification]); 
            ?>
                </div>
        <div class="row">
        <?php 
		
		$this->renderPartial('_grid', array('model' => $model)); ?>
    </div>
    </div>
	</div>
</div>

