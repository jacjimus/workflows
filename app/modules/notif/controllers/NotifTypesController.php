<?php

class NotifTypesController extends NotifModuleController
{

    public function init()
    {
        $this->resource = SettingsModuleConstants::RES_SETTINGS;
        $this->resourceLabel = 'Notification type';
        $this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
        $this->activeTab = SettingsModuleConstants::TAB_NOTIF;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        if (Yii::app()->user->user_level !== UserLevels::LEVEL_ENGINEER)
            throw new CHttpException(403, Lang::t('403_error'));
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);
        $model = new NotifTypes();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl(Yii::app()->createUrl($this->route, $this->actionParams)));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $model = NotifTypes::model()->loadModel($id);
        $this->pageTitle = $model->name;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl(Yii::app()->createUrl($this->route, $this->actionParams)));
            }
        }

        $model->users = NotifTypes::model()->getUsers($id);
        $model->roles = NotifTypes::model()->getRoles($id);

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        NotifTypes::model()->loadModel($id)->delete();

        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex()
    {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $this->render('index', array(
            'model' => NotifTypes::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE]),
        ));
    }

}
