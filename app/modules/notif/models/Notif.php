<?php

/**
 * This is the model class for table "notif".
 *
 * The followings are the available columns in table 'notif':
 * @property string $id
 * @property string $notif_type_id
 * @property string $user_id
 * @property string $item_id
 * @property integer $is_read
 * @property integer $is_seen
 * @property string $date_created
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property NotifTypes $notifType
 */
class Notif extends ActiveRecord {

    const TASK_NOTIFICATION_MANAGER = 'notificationManager';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'notif';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('notif_type_id, user_id, item_id', 'required'),
            array('is_read,is_seen', 'numerical', 'integerOnly' => true),
            array('notif_type_id', 'length', 'max' => 60),
            array('user_id, item_id', 'length', 'max' => 11),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'notifType' => array(self::BELONGS_TO, 'NotifTypes', 'notif_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'notif_type_id' => Lang::t('Notif Type'),
            'user_id' => Lang::t('User'),
            'item_id' => Lang::t('Item'),
            'is_read' => Lang::t('Read'),
            'date_created' => Lang::t('Date'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Notif the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Pushes a new notification
     * @param string $notif_type_id
     * @param int $item_id
     * @param array $user_ids
     */
    public function pushNotif($notif_type_id, $item_id, $user_ids = array()) {
        if (empty($user_ids)) {
            //get the users that should receive notif
            $user_ids = $this->getNotifUsers($notif_type_id);
        }

        if (!empty($user_ids)) {
            $notif_data = array();
            $date_created = new CDbExpression('NOW()');
            foreach ($user_ids as $user_id) {
                $notif_data[] = array(
                    'notif_type_id' => $notif_type_id,
                    'user_id' => $user_id,
                    'item_id' => $item_id,
                    'date_created' => $date_created,
                );
            }
            $this->insertMultiple($notif_data);
        }
        //process email
        $this->processEmail($notif_type_id, $user_ids, $item_id);
    }

    /**
     *
     * @param type $notif_type_id
     * @param type $users
     * @param type $item_id
     */
    protected function processEmail($notif_type_id, $users, $item_id) {
        if (empty($users))
            return FALSE;

        $notif_type = NotifTypes::model()->getRow('send_email,email_template,model_class_name', '`id`=:t1', array(':t1' => $notif_type_id));
        if (empty($notif_type['send_email']))
            return FALSE;

        $model_class_name = $notif_type['model_class_name'];
        $message = $model_class_name::model()->processNotifEmailTemplate($notif_type['email_template'], $item_id);

        if (!empty($message)) {
            $this->sendEmail($notif_type_id, $users, $message);
        }
    }

    /**
     * Update notification
     *
     */
    public function updateNotification() {
        $rowset = NotifTypes::model()->getColumnData('model_class_name', '`notification_trigger`=:t1 AND `is_active`=:t2', array(':t1' => NotifTypes::TRIGGER_SYSTEM, ':t2' => 1));
        foreach ($rowset as $model_class) {
            $model_class::model()->updateNotification();
        }
    }

    /**
     * Get notification date
     * @param string $notif_type_id
     * @return string $date
     */
    public function getNotificationDate($notif_type_id) {
        $notify_days_before = NotifTypes::model()->get($notif_type_id, 'notify_days_before');
        if (empty($notify_days_before)) {
            return date('Y-m-d');
        }

        return Common::addDate(date('Y-m-d'), (int) $notify_days_before, 'day');
    }

    /**
     * Get users who are supposed to receive a notification
     * @param string $notif_type_id
     * @return array $users
     */
    public function getNotifUsers($notif_type_id) {
        $notify = NotifTypes::model()->get($notif_type_id, 'notify');
        if (empty($notify))
            return FALSE;
        if ($notify === NotifTypes::NOTIFY_ALL_USERS)
            return Users::model()->getColumnData('id', '`status`=:t1', array(':t1' => Users::STATUS_ACTIVE));
        //get notif_type_users
        $users = NotifTypeUsers::model()->getColumnData('user_id', '`notif_type_id`=:t1', array(':t1' => $notif_type_id));
        //get notif_type_roles
        $notif_type_roles = NotifTypeRoles::model()->getColumnData('role_id', '`notif_type_id`=:t1', array(':t1' => $notif_type_id));
        if (!empty($notif_type_roles)) {
            foreach ($notif_type_roles as $role_id) {
                $users = array_merge($users, UserRoles::model()->getUsers($role_id));
            }
        }
        return array_unique($users);
    }

    /**
     * Send notification as an email
     * @param type $notif_type_id
     * @param array $users
     * @param type $message
     */
    public function sendEmail($notif_type_id, array $users, $message) {
        $from_name = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_APP_NAME, Yii::app()->name);
        $from_email = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_EMAIL, SettingsModuleConstants::SETTINGS_EMAIL_USERNAME);
        $subject = NotifTypes::model()->get($notif_type_id, 'name');


        foreach ($users as $user_id) {
//replace  '{{user}}' placeholder with the the user name
            $message = Common::myStringReplace($message, array(
                        '{{user}}' => Employeeslist::model()->get($user_id, 'empname'),
            ));

            MsgEmail::model()->push(array(
                'from_name' => $from_name,
                'from_email' => $from_email,
                'to_email' => Users::model()->get($user_id, 'email'),
                'subject' => $subject,
                'message' => $message,
            ));
        }
    }

    /**
     * Fetch notification
     * @param type $user_id
     * @return type
     */
    public function fetchNotif($user_id = NULL) {
        if (empty($user_id))
            $user_id = Yii::app()->user->id;
        return $this->getData('*', '`user_id`=:t1 AND is_read=0', array(':t1' => $user_id), 'id desc', 200);
    }

    /**
     *
     * @param type $user_id
     * @return type
     */
    public function getUnSeenNotif($user_id = NULL) {
        if (empty($user_id))
            $user_id = Yii::app()->user->id;
        return $this->getTotals('`user_id`=:t1 AND `is_seen`=:t2', array(':t1' => $user_id, ':t2' => 0));
    }

    /**
     *
     * @param type $notif_type_id
     * @param type $item_id
     * @return string $processed_template
     */
    public function processTemplate($notif_type_id, $item_id) {
        $notif_type = NotifTypes::model()->getRow('notif_template,model_class_name', '`id`=:id', array(':id' => $notif_type_id));
        if (empty($notif_type))
            return FALSE;
        $model_class_name = $notif_type['model_class_name'];
        return $model_class_name::model()->processNotifTemplate($notif_type['notif_template'], $item_id);
    }

    /**
     *
     * @return type
     */
    public function markAsSeen() {
        return Yii::app()->db->createCommand()
                        ->update(Notif::model()->tableName(), array('is_seen' => 1), '`user_id`=:t1', array(':t1' => Yii::app()->user->id));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function markAsRead($id = NULL) {
        $conditions = '`user_id`=:t1';
        $params = array(':t1' => Yii::app()->user->id);
        if (!empty($id)) {
            $conditions.=' AND `id`=:t2';
            $params[':t2'] = $id;
        }

        return Yii::app()->db->createCommand()
                        ->update(Notif::model()->tableName(), array('is_read' => 1), $conditions, $params);
    }

}
