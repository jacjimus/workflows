<?php

/**
 * This is the model class for table "notif_type_roles".
 *
 * The followings are the available columns in table 'notif_type_roles':
 * @property string $id
 * @property string $notif_type_id
 * @property string $role_id
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property NotifTypes $notifType
 */
class NotifTypeRoles extends ActiveRecord {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'notif_type_roles';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('notif_type_id, role_id, date_created', 'required'),
                    array('notif_type_id', 'length', 'max' => 60),
                    array('role_id, created_by', 'length', 'max' => 11),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'notifType' => array(self::BELONGS_TO, 'NotifTypes', 'notif_type_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => 'ID',
                    'notif_type_id' => 'Notif Type',
                    'role_id' => 'Role',
                    'date_created' => 'Date Created',
                    'created_by' => 'Created By',
                );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
                // @todo Please modify the following code to remove attributes that should not be searched.

                $criteria = new CDbCriteria;

                $criteria->compare('id', $this->id, true);
                $criteria->compare('notif_type_id', $this->notif_type_id, true);
                $criteria->compare('role_id', $this->role_id, true);
                $criteria->compare('date_created', $this->date_created, true);
                $criteria->compare('created_by', $this->created_by, true);

                return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return NotifTypeRoles the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

}
