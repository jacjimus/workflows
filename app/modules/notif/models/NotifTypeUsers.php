<?php

/**
 * This is the model class for table "notif_type_users".
 *
 * The followings are the available columns in table 'notif_type_users':
 * @property string $id
 * @property string $notif_type_id
 * @property string $user_id
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property NotifTypes $notifType
 */
class NotifTypeUsers extends ActiveRecord {

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'notif_type_users';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
                return array(
                    array('notif_type_id, user_id, date_created', 'required'),
                    array('notif_type_id', 'length', 'max' => 60),
                    array('user_id, created_by', 'length', 'max' => 11),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
                    'notifType' => array(self::BELONGS_TO, 'NotifTypes', 'notif_type_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'notif_type_id' => Lang::t('Notif Type'),
                    'user_id' => Lang::t('User'),
                    'date_created' => Lang::t('Date Created'),
                    'created_by' => Lang::t('Created By'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return NotifTypeUsers the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

}
