<?php
/* @var $this NotifTypeUsersController */
/* @var $model NotifTypeUsers */

$this->breadcrumbs=array(
	'Notif Type Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List NotifTypeUsers', 'url'=>array('index')),
	array('label'=>'Manage NotifTypeUsers', 'url'=>array('admin')),
);
?>

<h1>Create NotifTypeUsers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>