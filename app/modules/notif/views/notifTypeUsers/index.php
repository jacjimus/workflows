<?php
/* @var $this NotifTypeUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Notif Type Users',
);

$this->menu=array(
	array('label'=>'Create NotifTypeUsers', 'url'=>array('create')),
	array('label'=>'Manage NotifTypeUsers', 'url'=>array('admin')),
);
?>

<h1>Notif Type Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
