<?php
/* @var $this NotifTypeUsersController */
/* @var $model NotifTypeUsers */

$this->breadcrumbs=array(
	'Notif Type Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List NotifTypeUsers', 'url'=>array('index')),
	array('label'=>'Create NotifTypeUsers', 'url'=>array('create')),
	array('label'=>'Update NotifTypeUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete NotifTypeUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage NotifTypeUsers', 'url'=>array('admin')),
);
?>

<h1>View NotifTypeUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'notif_type_id',
		'user_id',
		'date_created',
		'created_by',
	),
)); ?>
