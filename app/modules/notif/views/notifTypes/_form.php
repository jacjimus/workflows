<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'notif-types-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form'
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bell-o"></i> <?php echo CHtml::encode($this->pageTitle) ?></h3>
    </div>
    <div class="panel-body">
        <?php echo CHtml::errorSummary($model, ''); ?>
        <fieldset>
            <legend><?php echo Lang::t('Notification details') ?></legend>
            <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'id', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activeTextField($model, 'id', array('class' => 'form-control', 'maxlength' => 60)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'maxlength' => 255, 'rows' => 3)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'model_class_name', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activeTextField($model, 'model_class_name', array('class' => 'form-control', 'maxlength' => 60)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'fa_icon_class', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activeTextField($model, 'fa_icon_class', array('class' => 'form-control', 'maxlength' => 30)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'notification_trigger', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activeDropDownList($model, 'notification_trigger', NotifTypes::notificationTriggerOptions(), array('class' => 'form-control')); ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-4">
                    <div class="checkbox">
                        <label>
                            <?php echo CHtml::activeCheckBox($model, 'is_active'); ?>&nbsp;<?php echo NotifTypes::model()->getAttributeLabel('is_active') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'notif_template', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-6">
                    <span class="help-block">Template for displaying notification within this system</span>
                    <?php echo CHtml::activeTextArea($model, 'notif_template', array('class' => 'form-control', 'maxlength' => 500, 'rows' => 4)); ?>
                    <span class="help-block">Please do not remove placeholders (terms enclosed in {{}})</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-6">
                    <div class="checkbox">
                        <label>
                            <?php echo CHtml::activeCheckBox($model, 'send_email'); ?>&nbsp;<?php echo Lang::t('send email when this notifcation is created.') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group hidden" id="email_template_wrapper">
                <?php echo CHtml::activeLabelEx($model, 'email_template', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-8">
                    <span class="help-block">Template for sending the notification as email</span>
                    <?php echo CHtml::activeTextArea($model, 'email_template', array('class' => 'form-control redactor', 'rows' => 6)); ?>
                    <span class="help-block">Please do not remove placeholders (terms enclosed in {{}})</span>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'notify_days_before', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-2">
                    <?php echo CHtml::activeDropDownList($model, 'notify_days_before', NotifTypes::notifyDaysBeforeOptions(), array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'notify', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-2">
                    <?php echo CHtml::activeDropDownList($model, 'notify', NotifTypes::notifyOptions(), array('class' => 'form-control', 'data-show-users' => NotifTypes::NOTIFY_SPECIFIED_USERS)); ?>
                </div>
            </div>
        </fieldset>
        <fieldset class="hidden" id="notify_users">
            <legend><?php echo Lang::t('Users and/or Roles to notify') ?></legend>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'users', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-6">
                    <?php echo $form->dropDownList($model, 'users', YII_DEBUG ? Employeeslist::model()->getListData('id', 'empname', false) : Employeeslist::model()->getListData('id', 'empname', false, '`user_level`<>:t1', array(':t1' => UserLevels::LEVEL_ENGINEER)), array('multiple' => 'multiple', 'class' => 'form-control chosen-select')); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'roles', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-6">
                    <?php echo $form->dropDownList($model, 'roles', UserRoles::model()->getListData('id', 'name', false), array('multiple' => 'multiple', 'class' => 'form-control chosen-select')); ?>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($this->createUrl('index')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" type="submit"><?php echo Lang::t('Save Changes') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 100,
        'convertDivs' => false,
        'cleanup' => TRUE,
        'paragraphy' => false,
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));

Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('notif.notifTypes._form', "$('.chosen-select').chosen(); NotifModule.NotifTypes.initForm();");
?>