<?php

class EmailController extends SettingsModuleController
{

    /**
     * Executed before every action
     */
    public function init()
    {
        $this->resource = SettingsModuleConstants::RES_SETTINGS;
        $this->activeMenu = SettingsModuleConstants::MENU_SETTINGS;
        $this->activeTab = SettingsModuleConstants::TAB_EMAIL;
        $this->resourceLabel = 'Email template';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'settings', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);

        $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

        $model = new SettingsEmailTemplate;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);

        $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);


        $model = SettingsEmailTemplate::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        Acl::hasPrivilege($this->privileges, $this->resource, Acl::ACTION_DELETE);
        SettingsEmailTemplate::model()->loadModel($id)->delete();
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex()
    {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $this->render('index', array(
            'model' => SettingsEmailTemplate::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id'),
        ));
    }

    public function actionSettings()
    {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t('Email Settings');
        if (isset($_POST['settings'])) {
            $this->hasPrivilege(Acl::ACTION_UPDATE);
            foreach ($_POST['settings'] as $key => $value) {
                Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_EMAIL, $key, $value);
            }

            Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
            $this->refresh();
        }
        $settings = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_EMAIL, array(
            SettingsModuleConstants::SETTINGS_EMAIL_MAILER,
            SettingsModuleConstants::SETTINGS_EMAIL_HOST,
            SettingsModuleConstants::SETTINGS_EMAIL_PORT,
            SettingsModuleConstants::SETTINGS_EMAIL_USERNAME,
            SettingsModuleConstants::SETTINGS_EMAIL_PASSWORD,
            SettingsModuleConstants::SETTINGS_EMAIL_SECURITY,
            SettingsModuleConstants::SETTINGS_EMAIL_MASTER_THEME,
            SettingsModuleConstants::SETTINGS_EMAIL_SENDMAIL_COMMAND,
        ));
        $this->render('settings', array(
            'settings' => $settings,
        ));
    }

}
