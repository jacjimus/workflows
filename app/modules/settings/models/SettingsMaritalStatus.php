<?php

/**
 * This is the model class for table "settings_marital_status".
 *
 * The followings are the available columns in table 'settings_marital_status':
 * @property string $id
 * @property string $name
 * @property string $date_created
 * @property string $created_by
 *
 */
class SettingsMaritalStatus extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_marital_status';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 10),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name' => Lang::t('Marital Status'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsMaritalStatus the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     *
     * @param type $add_tip default TRUE
     * @return type
     */
    public function getListData($add_tip = true)
    {
        return parent::getListData('id', 'name', $add_tip, '`is_active`=1');
    }

}
