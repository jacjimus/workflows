<?php

/**
 * This is the model class for table "settings_currency_conversion".
 *
 * The followings are the available columns in table 'settings_currency_conversion':
 * @property string $id
 * @property string $to_currency_id
 * @property string $from_currency_id
 * @property string $exchange_rate
 * @property string $date_created
 * @property string $created_by
 *
 */
class SettingsCurrencyConversion extends ActiveRecord implements IMyActiveSearch
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_currency_conversion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('to_currency_id, exchange_rate', 'required'),
            array('exchange_rate', 'numerical', 'min' => 0),
            array('from_currency_id', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'from_currency' => array(self::BELONGS_TO, 'SettingsCurrency', 'from_currency_id'),
            'to_currency' => array(self::BELONGS_TO, 'SettingsCurrency', 'to_currency_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'to_currency_id' => Lang::t('Currency'),
            'exchange_rate' => Lang::t('Exchange Rate'),
            'date_created' => Lang::t('Updated'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsCurrencyConversion the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Update exchange rate
     * @param int $to_currency_id
     * @param float $exchange_rate
     * @return boolean
     */
    public function updateExchangeRate($to_currency_id, $exchange_rate)
    {
        $from_currency_id = SettingsCurrency::model()->getCurrency('id');
        $model = $this->find('`from_currency_id`=:t1 AND `to_currency_id`=:t2', array(':t1' => $from_currency_id, ':t2' => $to_currency_id));
        if (NULL === $model) {
            $class_name = $this->getClassName();
            $model = new $class_name();
            $model->from_currency_id = $from_currency_id;
            $model->to_currency_id = $to_currency_id;
        }
        $model->exchange_rate = $exchange_rate;
        return $model->save(FALSE);
    }

    /**
     * Get exchange rate
     * @param type $to_currency_id
     * @param type $from_currency_id
     * @return type
     */
    public function getExchangeRate($to_currency_id, $from_currency_id = null)
    {
        if (empty($from_currency_id))
            $from_currency_id = SettingsCurrency::model()->getCurrency('id');
        if ($from_currency_id === $to_currency_id)
            return 1;
        $rate = $this->getScalar('exchange_rate', '`from_currency_id`=:t1 AND `to_currency_id`=:t2', array(':t1' => $from_currency_id, ':t2' => $to_currency_id));
        if (empty($rate))
            $rate = null;
        return $rate;
    }

    /**
     * Convert amount
     * @param float $amount
     * @param integer $from_currency_id
     * @param integer $to_currency_id
     * @return float $converted_amount
     */
    public function convertAmount($amount, $from_currency_id, $to_currency_id)
    {
        if (empty($amount))
            return $amount;
        //ks200 to $
        $exchange_rate = $this->getExchangeRate($to_currency_id, $from_currency_id);
        if (!empty($exchange_rate))
            return (float) ($amount * $exchange_rate);
        return $amount;
    }

    public function searchParams()
    {
        return array(
            'from_currency_id',
            'to_currency_id',
        );
    }

}
