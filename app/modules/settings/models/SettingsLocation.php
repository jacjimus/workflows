<?php

/**
 * This is the model class for table "settings_location".
 *
 * The followings are the available columns in table 'settings_location':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $latitude
 * @property string $longitude
 * @property string $country_id
 * @property string $town_id
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $status
 * @property string $date_created
 * @property string $created_by
 *
 */
class SettingsLocation extends ActiveRecord implements IMyActiveSearch {

    const STATUS_ACTIVE = 'Active';
    const STATUS_CLOSED = 'Closed';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'settings_location';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('name', 'required'),
            array('country_id, town_id, created_by', 'length', 'max' => 11),
            array('name, email', 'length', 'max' => 128),
            array('description, address', 'length', 'max' => 255),
            array('latitude, longitude', 'length', 'max' => 30),
            array('phone', 'length', 'max' => 15),
            array('status', 'length', 'max' => 7),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'docs' => array(self::HAS_MANY, 'Doc', 'location_id'),
            'events' => array(self::HAS_MANY, 'Event', 'location_id'),
            'invCountSheetItems' => array(self::HAS_MANY, 'InvCountSheetItem', 'location_id'),
            'invInventories' => array(self::HAS_MANY, 'InvInventory', 'location_id'),
            'invInventoryLogDetails' => array(self::HAS_MANY, 'InvInventoryLogDetail', 'from_location_id'),
            'invInventoryLogDetails1' => array(self::HAS_MANY, 'InvInventoryLogDetail', 'to_location_id'),
            'invInventoryQuantityTotals' => array(self::HAS_MANY, 'InvInventoryQuantityTotal', 'location_id'),
            'invProducts' => array(self::HAS_MANY, 'InvProduct', 'default_location_id'),
            'invPurchaseOrders' => array(self::HAS_MANY, 'InvPurchaseOrder', 'location_id'),
            'invPurchaseOrderReceiveItems' => array(self::HAS_MANY, 'InvPurchaseOrderReceiveItem', 'location_id'),
            'invPurchaseOrderUnstockItems' => array(self::HAS_MANY, 'InvPurchaseOrderUnstockItem', 'location_id'),
            'invSalesOrders' => array(self::HAS_MANY, 'InvSalesOrder', 'location_id'),
            'invSalesOrderPickItems' => array(self::HAS_MANY, 'InvSalesOrderPickItem', 'location_id'),
            'invSalesOrderRestockItems' => array(self::HAS_MANY, 'InvSalesOrderRestockItem', 'location_id'),
            'invWorkOrders' => array(self::HAS_MANY, 'InvWorkOrder', 'location_id'),
            'invWorkOrderPickItems' => array(self::HAS_MANY, 'InvWorkOrderPickItem', 'location_id'),
            'invWorkOrderPutItems' => array(self::HAS_MANY, 'InvWorkOrderPutItem', 'location_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'name' => Lang::t('Branch Name'),
            'description' => Lang::t('Description'),
            'latitude' => Lang::t('Latitude'),
            'longitude' => Lang::t('Longitude'),
            'country_id' => Lang::t('Country'),
            'town_id' => Lang::t('Town'),
            'email' => Lang::t('Email'),
            'phone' => Lang::t('Phone'),
            'address' => Lang::t('Address'),
            'status' => Lang::t('Status'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsLocation the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
            array('name', self::SEARCH_FIELD, true),
            'country_id',
            'town_id',
            'status',
            'is_active',
        );
    }

    public static function statusOptions() {
        return array(
            self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
            self::STATUS_CLOSED => Lang::t(self::STATUS_CLOSED),
        );
    }

    /**
     * Get default_location_id
     * @return int $location_id
     */
    public function getDefaultLocationId() {
        return Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_DEFAULT_LOCATION_ID);
    }

    /**
     *
     * @param boolean $add_tip
     * @return type
     */
    /* public function getListData($add_tip = true) {
      return parent::getListData('id', 'name', $add_tip, '`is_active`=1');
      } */
}
