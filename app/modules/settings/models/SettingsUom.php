<?php

/**
 * This is the model class for table "settings_uom".
 *
 * The followings are the available columns in table 'settings_uom':
 * @property string $id
 * @property string $unit
 * @property string $description
 * @property string $date_created
 * @property string $created_by
 */
class SettingsUom extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_uom';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('unit,description', 'required'),
            array('unit', 'length', 'max' => 20),
            array('description', 'length', 'max' => 255),
            array('unit,description', 'unique', 'message' => Lang::t('{attribute} ({value}) already exists.')),
            array('created_by', 'length', 'max' => 11),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'unit' => Lang::t('Unit'),
            'description' => Lang::t('Description'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsUom the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
