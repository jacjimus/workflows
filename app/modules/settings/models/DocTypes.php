<?php

/**
 * This is the model class for table "doc_types".
 *
 * The followings are the available columns in table 'doc_types':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $date_created
 * @property string $created_by
 *
 */
class DocTypes extends ActiveRecord implements IMyActiveSearch
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'doc_types';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 128),
            array('description', 'length', 'max' => 255),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'docs' => array(self::HAS_MANY, 'Doc', 'doc_type_id'),
            'meActivityDocs' => array(self::HAS_MANY, 'MeActivityDocs', 'doc_type_id'),
            'meIndicatorTransactionDocs' => array(self::HAS_MANY, 'MeIndicatorTransactionDocs', 'doc_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'name' => Lang::t('Name'),
            'description' => Lang::t('Description'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DocTypes the static model class
     */ public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function searchParams()
    {
        return array(
            array('name', self::SEARCH_FIELD, true),
            'id',
        );
    }

}
