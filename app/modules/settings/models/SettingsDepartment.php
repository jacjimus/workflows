<?php

/**
 * This is the model class for table "org_department".
 *
 * The followings are the available columns in table 'org_department':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 */
class SettingsDepartment extends ActiveRecord {

      const STATUS_ACTIVE = 'Active';
      const STATUS_CLOSED = 'Closed';

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'settings_department';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('name', 'required'),
                array('created_by', 'length', 'max' => 11),
                array('name', 'length', 'max' => 128),
                array('description', 'length', 'max' => 255),
                array('status', 'length', 'max' => 7),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'name' => Lang::t('Department'),
                'description' => Lang::t('Description'),
                'status' => Lang::t('Status'),
                'date_created' => Lang::t('Date Created'),
                'created_by' => Lang::t('Created By'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return SettingsDepartment the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      public function searchParams() {
            return array(
                array('name', self::SEARCH_FIELD, true),
                'status',
            );
      }

      public static function statusOptions() {
            return array(
                self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
                self::STATUS_CLOSED => Lang::t(self::STATUS_CLOSED),
            );
      }

}
