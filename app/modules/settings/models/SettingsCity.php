<?php

/**
 * This is the model class for table "settings_city".
 *
 * The followings are the available columns in table 'settings_city':
 * @property string $id
 * @property string $name
 * @property string $country_id
 * @property string $latitude
 * @property string $longitude
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property SettingsCountry $country
 */
class SettingsCity extends ActiveRecord implements IMyActiveSearch
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_city';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, country_id', 'required'),
            array('created_by', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 128),
            array('country_id', 'length', 'max' => 11),
            array('latitude, longitude', 'length', 'max' => 30),
            array('name', 'checkDuplicates'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'invCustomerAddresses' => array(self::HAS_MANY, 'InvCustomerAddress', 'city_id'),
            'invPurchaseOrders' => array(self::HAS_MANY, 'InvPurchaseOrder', 'vendor_city_id'),
            'invPurchaseOrders1' => array(self::HAS_MANY, 'InvPurchaseOrder', 'ship_to_city_id'),
            'invReceivingAddresses' => array(self::HAS_MANY, 'InvReceivingAddress', 'city_id'),
            'invSalesOrders' => array(self::HAS_MANY, 'InvSalesOrder', 'billing_city_id'),
            'invSalesOrders1' => array(self::HAS_MANY, 'InvSalesOrder', 'shipping_city_id'),
            'invVendorAddresses' => array(self::HAS_MANY, 'InvVendorAddress', 'city_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'name' => Lang::t('City'),
            'country_id' => Lang::t('Country'),
            'latitude' => Lang::t('Latitude'),
            'longitude' => Lang::t('Longitude'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsCity the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function checkDuplicates()
    {
        $conditions = '`name`=:t1 AND `country_id`=:t2';
        $params = array(':t1' => $this->name, ':t2' => $this->country_id);
        if (!$this->isNewRecord) {
            $conditions.=' AND `id`<>:t3';
            $params[':t3'] = $this->id;
        }
        if ($this->exists($conditions, $params)) {
            $this->addError('name', Lang::t('{value} already exists.', array('{value}' => $this->name)));
        }
    }

    public function searchParams()
    {
        return array(
            array('name', self::SEARCH_FIELD, true),
            'country_id',
        );
    }

    /**
     * Add city
     * @param type $name
     * @param type $country_id
     */
    public function addCity($name, $country_id)
    {
        $model = new SettingsCity();
        $model->country_id = $country_id;
        $model->name = $name;
        $model->save();
        return $model->id;
    }

}
