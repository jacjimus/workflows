<?php

/**
 * This is the model class for table "settings_numbering_format".
 *
 * The followings are the available columns in table 'settings_numbering_format':
 * @property string $id
 * @property string $type
 * @property string $description
 * @property integer $next_number
 * @property integer $min_digits
 * @property string $prefix
 * @property string $suffix
 * @property string $preview
 * @property string $module
 * @property string $date_created
 * @property string $created_by
 */
class SettingsNumberingFormat extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_numbering_format';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('type, description', 'required'),
            array('next_number', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('min_digits', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 6),
            array('type', 'length', 'max' => 60),
            array('description', 'length', 'max' => 255),
            array('prefix, suffix', 'length', 'max' => 5),
            array('created_by', 'length', 'max' => 11),
            array('type', 'unique', 'message' => Lang::t('{attribute} ({value}) already exists.')),
            array('preview,module', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'type' => Lang::t('Type'),
            'description' => Lang::t('Description'),
            'next_number' => Lang::t('Next Number'),
            'min_digits' => Lang::t('Minimum Digits'),
            'prefix' => Lang::t('Prefix'),
            'suffix' => Lang::t('Suffix'),
            'preview' => Lang::t('Preview'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsNumberingFormat the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

    /**
     * Get next formated number
     * @param string $type
     * @param boolean $increment_next_number
     * @return string $formated_number
     */
    public function getNextFormatedNumber($type, $increment_next_number = true)
    {
        $format = $this->getRow('*', '`type`=:type', array(':type' => $type));
        $next_number = ArrayHelper::getValue($format, 'next_number', 1);
        $min_digits = ArrayHelper::getValue($format, 'min_digits', 4);
        $prefix = ArrayHelper::getValue($format, 'prefix', '');
        $suffix = ArrayHelper::getValue($format, 'suffix', '');
        $template = '{{prefix}}{{number}}{{suffix}}';

        $number = str_pad($next_number, $min_digits, "0", STR_PAD_LEFT);
        if (!empty($format) && $increment_next_number) {
            $next_number ++;
            Yii::app()->db->createCommand()
                    ->update($this->tableName(), array('next_number' => $next_number), '`id`=:id', array(':id' => $format['id']));
        }
        return strtr($template, array(
            '{{prefix}}' => $prefix,
            '{{number}}' => $number,
            '{{suffix}}' => $suffix,
        ));
    }

}
