<?php

/**
 *
 * @author Fred <mconyango@gmail.com>
 */
class GeneralSettings extends FormModel
{

    /**
     *
     * @var type
     */
    public $company_name;

    /**
     *
     * @var type
     */
    public $app_name;

    /**
     *
     * @var type
     */
    public $company_email;

    /**
     *
     * @var type
     */
    public $default_timezone;

    /**
     *
     * @var type
     */
    public $country_id;

    /**
     *
     * @var type
     */
    public $items_per_page;

    /**
     *
     * @var type
     */
    public $theme;

    const THEME1 = 'smart-style-0';
    const THEME2 = 'smart-style-1';
    const THEME3 = 'smart-style-2';
    const THEME4 = 'smart-style-3';

    public function init()
    {
        $settings = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, array(
            SettingsModuleConstants::SETTINGS_COMPANY_NAME => Yii::app()->name,
            SettingsModuleConstants::SETTINGS_COMPANY_EMAIL,
            SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE => 30,
            SettingsModuleConstants::SETTINGS_APP_NAME,
            SettingsModuleConstants::SETTINGS_DEFAULT_TIMEZONE => 'Africa/Nairobi',
            SettingsModuleConstants::SETTINGS_COUNTRY_ID,
            SettingsModuleConstants::SETTINGS_THEME,
        ));
        $this->company_name = $settings[SettingsModuleConstants::SETTINGS_COMPANY_NAME];
        $this->app_name = $settings[SettingsModuleConstants::SETTINGS_APP_NAME];
        $this->company_email = $settings[SettingsModuleConstants::SETTINGS_COMPANY_EMAIL];
        $this->default_timezone = $settings[SettingsModuleConstants::SETTINGS_DEFAULT_TIMEZONE];
        $this->country_id = $settings[SettingsModuleConstants::SETTINGS_COUNTRY_ID];
        $this->items_per_page = $settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE];
        $this->theme = $settings[SettingsModuleConstants::SETTINGS_THEME];
        parent::init();
    }

    public function rules()
    {
        return array(
            array('company_name,app_name,default_timezone,country_id,items_per_page', 'required'),
            array('items_per_page', 'numerical', 'min' => 5, 'max' => 500),
            array('company_email', 'email', 'message' => 'Enter a valid Email Address.'),
            array('theme', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'company_name' => Lang::t('Company Name'),
            'app_name' => Lang::t('App Name'),
            'company_email' => Lang::t('Email'),
            'default_timezone' => Lang::t('Default Timezone'),
            'country_id' => Lang::t('Country'),
            'items_per_page' => Lang::t('Items Per Page'),
            'theme' => Lang::t('Theme'),
        );
    }

    public function save()
    {
        if ($this->validate()) {
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COMPANY_NAME, $this->company_name);
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_APP_NAME, $this->app_name);
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COMPANY_EMAIL, $this->company_email);
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_DEFAULT_TIMEZONE, $this->default_timezone);
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_COUNTRY_ID, $this->country_id);
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE, $this->items_per_page);
            Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_THEME, $this->theme);

            return true;
        }
        return false;
    }

    public static function themeOptions()
    {
        return array(
            self::THEME1 => Lang::t('Default'),
            self::THEME2 => Lang::t('Dark Elegance'),
            self::THEME3 => Lang::t('Ultra Light'),
            self::THEME4 => Lang::t('Google Skin'),
        );
    }

}
