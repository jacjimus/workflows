<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'general-settings-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $this->pageTitle; ?></h3>
    </div>
    <div class="panel-body">
        <?php echo CHtml::errorSummary($model, ''); ?>
        <fieldset>
            <legend><?php echo Lang::t('Company details') ?></legend>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'company_name', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeTextField($model, 'company_name', array('class' => 'form-control', 'maxlength' => 60)); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'app_name', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeTextField($model, 'app_name', array('class' => 'form-control', 'maxlength' => 30)); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'company_email', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeTextField($model, 'company_email', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'default_timezone', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeDropDownList($model, 'default_timezone', SettingsTimezone::model()->getListData('name', 'name', false), array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'country_id', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeDropDownList($model, 'country_id', SettingsCountry::model()->getListData('id', 'name', false), array('class' => 'form-control')); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend><?php echo Lang::t('Display options') ?></legend>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'items_per_page', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeDropDownList($model, 'items_per_page', Common::generateIntergersList(5, 100, 5), array('class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'theme', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeDropDownList($model, 'theme', GeneralSettings::themeOptions(), array('class' => 'form-control')); ?>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl(Yii::app()->createUrl('default/index')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" <?php echo!$this->showLink($this->resource, Acl::ACTION_UPDATE) ? 'disabled="disabled"' : null ?> type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Save Changes') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

