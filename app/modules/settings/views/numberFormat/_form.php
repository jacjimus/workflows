<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <?php echo CHtml::activeHiddenField($model, 'module'); ?>
    <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'type', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php echo CHtml::activeTextField($model, 'type', array('class' => 'form-control', 'maxlength' => 60)); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php echo CHtml::activeTextField($model, 'description', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'next_number', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-2">
                <?php echo CHtml::activeTextField($model, 'next_number', array('class' => 'form-control update-preview')); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'min_digits', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-2">
            <?php echo CHtml::activeTextField($model, 'min_digits', array('class' => 'form-control update-preview')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'prefix', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-2">
            <?php echo CHtml::activeTextField($model, 'prefix', array('class' => 'form-control update-preview', 'maxlength' => 5)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'suffix', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-2">
            <?php echo CHtml::activeTextField($model, 'suffix', array('class' => 'form-control update-preview', 'maxlength' => 5)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'preview', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'preview', array('class' => 'form-control update-preview', 'maxlength' => 128, 'readonly' => true)); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript->registerScript('settings.numberFormat._form', "SettingsModule.NumberingFormat.Form.init();")
?>