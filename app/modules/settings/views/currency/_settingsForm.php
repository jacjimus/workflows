<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'currency-settings-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-inline',
    )
        ));
?>
<div class="form-group">
    <?php echo CHtml::activeDropDownList($model, 'currency_id', SettingsCurrency::model()->getListData('id', 'description', false), array('class' => 'form-control')); ?>
</div>
<button type="submit" class="btn btn-primary"><?php echo Lang::t('Save') ?></button>
<?php $this->endWidget(); ?>

