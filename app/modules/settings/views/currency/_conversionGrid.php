<?php

$grid_id = 'settings-currency-conversion-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Currency conversion rates againt <b>1 {default}</b>', array('{default}' => SettingsCurrency::model()->getCurrency())),
    'titleIcon' => '<i class="fa fa-refresh"></i>',
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_UPDATE), 'url' => $this->createUrl('updateRates'), 'label' => '<i class="fa fa-edit"></i> ' . Lang::t('Update conversion rates'), 'modal' => true),
    'toolbarButtons' => array(),
    'panel' => array('showFooter' => false),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'to_currency_id',
                'value' => 'SettingsCurrency::model()->get($data->to_currency_id,"code")',
            ),
            array(
                'name' => 'exchange_rate',
                'value' => '(float)$data->exchange_rate',
            ),
            array(
                'name' => 'last_modified',
                'value' => 'MyYiiUtils::formatDate(!empty($data->last_modified) ? $data->last_modified : $data->date_created)',
            )
        ),
    )
));
