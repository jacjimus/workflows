<?php
$class_name = SettingsCurrencyConversion::model()->getClassName();
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::decode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Currency</th>
                <th>Rate</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (SettingsCurrency::model()->getConversionListData() as $row): ?>
                <tr>
                    <td><?php echo CHtml::encode($row['code']) ?></td>
                    <td>
                        <?php echo CHtml::hiddenField("{$class_name}[{$row['id']}][to_currency_id]", $row['id']) ?>
                        <?php echo CHtml::textField("{$class_name}[{$row['id']}][exchange_rate]", (float) SettingsCurrencyConversion::model()->getExchangeRate($row['id']), array('class' => 'form-control', 'style' => 'width:100px')) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>