<?php
/* @var $this BankBranchesController */
/* @var $model BankBranches */
?>

<?php
    $this->breadcrumbs=array(
        $this->homeTitle=>array('index'),
         CHtml::encode($this->pageTitle),
    );
    $can_update=Acl::hasPrivilege($this->privileges, $this->resource,  Acl::ACTION_UPDATE,false);
    Yii::app()->clientScript->registerScript('admin', "
       AdminController.init();
    ");
?>
<div class="container-fluid">
    <div class="row-fluid">
      <div class="area-top clearfix">
        <div class="pull-left header">
            <h3 class="title"> <i class="icon-user"></i><?php echo CHtml::encode($this->pageTitle);?></h3>
            <h5><a class=""></a></h5>
        </div>
         <ul class="inline pull-right sparkline-box no-top-margins unstyled user-summary">
                <?php if($can_update):?>
             <li class="sparkline-row"><h4 class="blue"><span><a href="<?php echo $this->createUrl('update',array(AdminUsers::USERS_FILTER_GET_PARAM=>$this->resource,'id'=>$model->id))?>"><i class="icon-edit"></i> Update details</a></a></span></h4></li>
                <?php endif;?>
             <li class="sparkline-row"><h4 class="blue"><span><a href="<?php echo $this->createUrl('index',array(AdminUsers::USERS_FILTER_GET_PARAM=>  $this->resource))?>">Close <i class="icon-remove"></i></a></a></span></h4></li>
         </ul>
      </div>
    </div>
</div>
<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->renderPartial('//widgets/_alert');?>
            <div class="box">
                <div class="box-header"><span class="title">&nbsp;</span></div>
                <div class="box-content">
                   <?php $this->widget('zii.widgets.CDetailView', array(
                    'data'=>$model,
                    'attributes'=>array(
                            'id',
                             array(
                                     'name'=>'bank_id',
                                     'value'=>$model->bank->bank_name,
                                 ),
                            'branch_code',
                            'branch_name',               
                           
                    ),
            )); ?>
                </div>
            </div>
        </div>
    </div>
</div>

