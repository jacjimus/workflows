<?php

$grid_id = 'bank-branches-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => false,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'id',
            'branch_code',
            'branch_name',
            array(
                'name' => 'bank_id',
                'value' => '$data->bank->bank_name',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x text-success"></i> ',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_SETTINGS . '", "' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => 'Edit',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i> ',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_SETTINGS . '", "' . Acl::ACTION_DELETE . '")&&$data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'my-update-grid delete',
                            'title' => 'Delete',
                        ),
                    ),
                )
            ),
        ),
    )
));
?>