<?php

$grid_id = 'modules-enabled-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-check-square-o"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . ModulesEnabled::STATUS_DISABLED . '"?"bg-danger":""',
        'columns' => array(
            'id',
            'name',
            'description',
            array(
                'name' => 'status',
                'value' => 'CHtml::tag("span", array("class" => $data->status==="' . ModulesEnabled::STATUS_ENABLED . '"?"badge bg-success":"badge badge-danger"), $data->status)',
                'type' => 'raw',
            ),
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'template' => '{enable}{disable}{update}{delete}',
                'buttons' => array(
                    'enable' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-check fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("updateStatus",array("id"=>$data->id,"status"=>"' . ModulesEnabled::STATUS_ENABLED . '"))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_MODULES_ENABLED . '","' . Acl::ACTION_UPDATE . '") && ($data->status==="' . ModulesEnabled::STATUS_DISABLED . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Enable this module'),
                        ),
                    ),
                    'disable' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-times fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("updateStatus",array("id"=>$data->id,"status"=>"' . ModulesEnabled::STATUS_DISABLED . '"))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_MODULES_ENABLED . '","' . Acl::ACTION_UPDATE . '") && ($data->status==="' . ModulesEnabled::STATUS_ENABLED . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Disable this module'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_MODULES_ENABLED . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_MODULES_ENABLED . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>