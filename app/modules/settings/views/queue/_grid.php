<?php

$grid_id = 'queue-tasks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-tasks"></i>',
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . QueueTasks::STATUS_INACTIVE . '"?"bg-danger":""',
        'columns' => array(
            'id',
            'execution_type',
            array(
                'name' => 'last_run',
                'value' => 'MyYiiUtils::formatDate($data->last_run,"d-m-Y H:i:s ")',
            ),
            'threads',
            'max_threads',
            'sleep',
            array(
                'name' => 'status',
                'value' => 'CHtml::tag("span", array("class" => $data->status==="' . QueueTasks::STATUS_ACTIVE . '"?"badge bg-success":"badge badge-danger"), $data->status)',
                'type' => 'raw',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{start}{stop}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 150px;'),
                'buttons' => array(
                    'start' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-check fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("start",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_QUEUEMANAGER . '","' . Acl::ACTION_UPDATE . '") && ($data->status==="' . QueueTasks::STATUS_INACTIVE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Start the process'),
                        ),
                    ),
                    'stop' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-times fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("stop",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_QUEUEMANAGER . '","' . Acl::ACTION_UPDATE . '") && ($data->status==="' . QueueTasks::STATUS_ACTIVE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Stop all processes'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_QUEUEMANAGER . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_QUEUEMANAGER . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
