<?php
$form_id = 'email-template-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
      <div class="panel-heading">
            <h3 class="panel-title"><?php echo CHtml::encode($this->pageTitle) ?></h3>
      </div>
      <div class="panel-body">
            <?php echo $form->errorSummary($model, ''); ?>
            <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
                  <div class="form-group">
                        <?php echo $form->labelEx($model, 'key', array('class' => 'col-md-2 control-label')); ?>
                        <div class="col-md-6">
                              <?php echo $form->textField($model, 'key', array('class' => 'form-control', 'maxlength' => 128)); ?>
                        </div>
                  </div>
                  <div class="form-group">
                        <?php echo $form->labelEx($model, 'description', array('class' => 'col-md-2 control-label')); ?>
                        <div class="col-md-6">
                              <?php echo $form->textField($model, 'description', array('class' => 'form-control', 'maxlength' => 128)); ?>
                        </div>
                  </div>
            <?php endif ?>
            <div class="form-group">
                  <?php echo $form->labelEx($model, 'subject', array('class' => 'col-md-2 control-label')); ?>
                  <div class="col-md-6">
                        <?php echo $form->textField($model, 'subject', array('class' => 'form-control', 'maxlength' => 255)); ?>
                  </div>
            </div>
            <div class="form-group">
                  <?php echo $form->labelEx($model, 'from', array('class' => 'col-md-2 control-label')); ?>
                  <div class="col-md-6">
                        <?php echo $form->textField($model, 'from', array('class' => 'form-control', 'maxlength' => 255)); ?>
                  </div>
            </div>
            <div class="form-group">
                  <?php echo $form->labelEx($model, 'body', array('class' => 'col-md-2 control-label')); ?>
                  <div class="col-md-8">
                        <?php echo $form->textArea($model, 'body', array('class' => 'form-control redactor', 'rows' => 6)); ?>
                        <p class="help-block">NOTE: Please DO NOT remove placeholders (words enclosed with {}). You are free to reorganize the body template and add other words or html tags but do not remove the placeholders</p>
                  </div>
            </div>
      </div>
      <div class="panel-footer clearfix">
            <div class="pull-right">
                  <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($this->createUrl('index')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
                  <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
            </div>
      </div>
      <?php $this->endWidget(); ?>
</div>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 100,
        'convertDivs' => false,
        'cleanup' => TRUE,
        'paragraphy' => false,
        'imageUpload' => Yii::app()->createUrl('helper/uploadRedactor'),
        'imageUploadErrorCallback' => new CJavaScriptExpression(
                'function(obj,json) {console.log(json.error);}'
        ),
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));
?>