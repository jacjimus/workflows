<ul class="nav nav-tabs my-nav">
        <li><a href="<?php echo Yii::app()->createUrl('settings/email/index') ?>"><?php echo Lang::t('Email Templates') ?></a></li>
        <li><a href="<?php echo Yii::app()->createUrl('settings/email/settings') ?>"><?php echo Lang::t('Email Settings') ?></a></li>
</ul>
