<ul class="nav nav-tabs">
        <li class="<?php echo $this->activeTab === SettingsModuleConstants::TAB_BANKS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('settings/banks/index') ?>"><?php echo Lang::t('Banks') ?></a></li>
        <li class="<?php echo $this->activeTab === SettingsModuleConstants::TAB_BANK_BRANCHES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('settings/bankBranches/index') ?>"><?php echo Lang::t('Bank Branches') ?></a></li>
</ul>
