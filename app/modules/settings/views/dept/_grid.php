<?php

$grid_id = 'settings-dept-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Departments'),
    'titleIcon' => '',
    'showExportButton' => true,
    'exportRoute' => 'settings/dept/export',
    'exportActionParams' => $this->actionParams,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'url' => $this->createUrl('dept/create'), 'label' => '<i class="fa fa-plus-circle"></i> ' . Lang::t(Constants::LABEL_CREATE . ' Department'), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . SettingsDepartment::STATUS_CLOSED . '"?"bg-danger":""',
        'columns' => array(
            'name',
            'status',
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("dept/update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_ORG_STRUCTURE . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_ORG_STRUCTURE . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>