<?php if ($this->showLink(SettingsModuleConstants::RES_SETTINGS)): ?>
    <li class="<?php echo $this->activeMenu === SettingsModuleConstants::MENU_SETTINGS ? 'active' : '' ?>">
        <a href="<?php echo Yii::app()->createUrl('settings/default/index') ?>"><i class="fa fa-lg fa-fw fa-cog"></i> <span class="menu-item-parent"><?php echo Lang::t('Settings') ?></span></a>
    </li>
<?php endif; ?>