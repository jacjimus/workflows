<div class="well no-padding">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => false,
            'focus' => array($model, 'username'),
            'clientOptions' => array(
                'validateOnSubmit' => false,
            ),
            'htmlOptions' => array(
                'class' => 'smart-form client-form',
            )
        ));
        ?>
        <header><center><img src="<?php echo Yii::app()->theme->baseUrl . '/img/logo.png'?>"  height="90px"  width="100%"><br>
                PROJECTS MANAGEMENT </center></header>
        <fieldset>
                <?php echo $form->errorSummary($model, ''); ?>
                <section>
                        <?php echo $form->labelEx($model, 'username', array('class' => 'label')); ?>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                <?php echo $form->textField($model, 'username', array('class' => '', 'required' => true, 'placeholder' => Lang::t('Username or Email'))); ?>
                        </label>
                </section>
                <section>
                        <?php echo $form->labelEx($model, 'password', array('class' => 'label')); ?>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <?php echo $form->passwordField($model, 'password', array('class' => '', 'required' => true, 'placeholder' => Lang::t('Enter your password'))); ?>
                        </label>
                        <div class="note">
                                <a href="<?php echo $this->createUrl('forgotPassword') ?>"><?php echo Lang::t('Forgot password?') ?></a>
                        </div>
                </section>
        </fieldset>
        <footer>
                <button type="submit" class="btn btn-primary"><?php echo Lang::t('Sign in'); ?></button>
        </footer>
        <?php $this->endWidget(); ?>
</div>