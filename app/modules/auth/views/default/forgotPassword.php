<div class="well no-padding">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'forgot-password-form',
            'enableClientValidation' => false,
            'focus' => array($model, 'username'),
            'clientOptions' => array(
                'validateOnSubmit' => false,
            ),
            'htmlOptions' => array(
                'class' => 'smart-form client-form',
            )
        ));
        ?>
        <header><?php echo CHtml::encode($this->pageTitle) ?></header>
        <?php echo $form->errorSummary($model, ''); ?>
        <fieldset>
                <section>
                        <label class="label"><?php echo Lang::t('Enter your Username or Email'); ?></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                <?php echo $form->textField($model, 'username', array('class' => '', 'required' => true)); ?>
                        </label>
                </section>
        </fieldset>
        <footer>
                <button type="submit" class="btn btn-primary"><?php echo Lang::t('Submit'); ?></button>
                <a class="btn btn-link" href="<?php echo $this->createUrl('login') ?>"><?php echo Lang::t('Back to login') ?></a>

        </footer>
        <?php $this->endWidget(); ?>
</div>