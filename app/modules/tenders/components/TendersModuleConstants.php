<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class TendersModuleConstants {

      const MOD_TENDERS = 'tenders';
      //resources constants
      const RES_TENDERS= 'TENDERS';
      const RES_TENDERS_REGISTER = 'TENDERS_REGISTER';
      const RES_TENDERS_CHECKLIST = 'TENDERS_CHECKLIST';
      //menu constants
      const MENU_TENDERS = 'MENU_TENDERS';
      const MENU_TENDERS_REG = 'MENU_TENDERS_REG';
      const MENU_TENDERS_CHECKLIST = 'MENU_TENDERS_CHECKLIST';

}
