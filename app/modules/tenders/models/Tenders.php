<?php

/**
 * This is the model class for table "td_tenders".
 *
 * The followings are the available columns in table 'td_tenders':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $tender_date
 * @property string $closing_date
 * @property string $tendering_company
 * @property string $company_location
 * @property integer $has_addendum
 * @property integer $addendum
 * @property string $tender_doc_path
 * @property string $tender_code
 */
class Tenders extends ActiveRecord
{
    
    public $temp_doc_file;
    
    public $tender_doc_path;
    
    const BASE_DIR = 'tenders';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'td_tenders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, category_id, closing_date, tendering_company, company_location', 'required' , 'message' => "{attribute} is required"),
			array('has_addendum, addendum', 'numerical', 'integerOnly'=>true),
			array('name, tendering_company, company_location', 'length', 'max'=>100),
			array('tender_doc_path,tender_url', 'length', 'max'=>255),
			array('tender_code', 'length', 'max'=>50),
			array('tender_url', 'url'),
                        array('temp_doc_file, category_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, tender_date, closing_date, tendering_company, company_location, has_addendum, addendum, tender_doc_path, tender_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
        
        /**
       * Get the dir of a user
       * @param string $doc_type_id
       */
      public function getDir($id = NULL) {
            if (!empty($this->id))
                  $id = $this->id;
            return Common::createDir($this->getBaseDir() . DS . $id);
      }

      public function getBaseDir() {
            return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
      }

      protected function setDocFile() {
            //using fineuploader
            if (!empty($this->temp_doc_file)) {
                  $temp_file = Common::parseFilePath($this->temp_doc_file);
                  $image_name = $temp_file['name'];
                  $temp_dir = $temp_file['dir'];
                  $new_path = $this->getDir() . DS . $image_name;
                  if (copy($this->temp_doc_file, $new_path)) {
                        if (!empty($temp_dir))
                              Common::deleteDir($temp_dir);

                        $this->tender_doc_path = $image_name;
                        $this->temp_doc_file = NULL;
                  }
            }
      }

      /**
       * Get file path
       * @return type
       */
      public function getFilePath() {
            return $this->getDir() . DS . $this->doc_file;
      }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'tender_date' => 'Tender Date',
			'closing_date' => 'Closing Date',
			'tendering_company' => 'Client',
			'company_location' => 'Company Location',
			'has_addendum' => 'Has Addendum',
			'addendum' => 'Addendum',
			'tender_doc_path' => 'Tender Doc',
			'tender_code' => 'Tender Code',
			'tender_url' => 'Tender download link',
			'bind_bond' => 'Bind Bond',
			'category_id' => 'Tender Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('tender_date',$this->tender_date,true);
		$criteria->compare('closing_date',$this->closing_date,true);
		$criteria->compare('tendering_company',$this->tendering_company,true);
		$criteria->compare('company_location',$this->company_location,true);
		$criteria->compare('has_addendum',$this->has_addendum);
		$criteria->compare('addendum',$this->addendum);
		$criteria->compare('tender_doc_path',$this->tender_doc_path,true);
		$criteria->compare('tender_code',$this->tender_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
          /**
       * Get doc count
       * @param type $doc_type_id
       * @return int
       */
      public function getTenderCount($category_id = NUll) {
            $conditions = "";
            $params = array();
            if (!empty($category_id)) {
                  $conditions.='`category_id`=:category_id';
                  $params[':category_id'] = $category_id;
            }
            
            return $this->getTotals($conditions, $params);
      }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tenders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
