<?php

class ChecklistController extends TendersModuleController
{

    public function init()
    {
        $this->resourceLabel = 'Tender Checklist';
        $this->resource = TendersModuleConstants::RES_TENDERS_CHECKLIST;
        $this->activeMenu = TendersModuleConstants::MENU_TENDERS_CHECKLIST;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'download', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->hasPrivilege();
        $model = Doc::model()->loadModel($id);
        if($model->doc_type_id == 2):
        $file = $model->getFilePath();
        //.. get the content of the requested file
        $content = file_get_contents($file);
        $file_name = Common::cleanString($model->name) . '.pdf';
        //.. send appropriate headers
        header("Content-type: application/pdf, application/octet-stream");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        echo $content;
        else:
            
        $this->redirect("index" , array('category_id' => $model->category_id));
        endif;
    }

    public function actionDownload($id)
    {
        $this->hasPrivilege();
        $model = Doc::model()->loadModel($id);
        $file = $model->getFilePath();
        MyYiiUtils::downloadFile($file);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($tender_id = NULL)
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_REGISTER . ' ' . Tenders::model()->get($tender_id , 'name') . " New Tender " );

        $model = new TenderChecklist(ActiveRecord::SCENARIO_CREATE);
        $model->tender_id = $tender_id;
        $model->created_by = Yii::app()->user->id;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
                   
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('checklist/index', array('tender_id' => $model->tender_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
            'id' => $tender_id
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = TenderChecklist::model()->loadModel($id);
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('checklist/index', array('tender_id' => $model->tender_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
            'id' => $id,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        TenderChecklist::model()->loadModel($id)->delete();
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }


    public function actionIndex($tender_id = NULL)
    {
        
         $this->hasPrivilege();
         $tender_type_model = NULL;
        if (!empty($tender_id)) {
            $tender_type_model = Tenders::model()->loadModel($tender_id);
            $this->pageTitle = $tender_type_model->name;
        } else
            $this->pageTitle = Lang::t($this->resourceLabel);

        $this->render('checklist', array(
            'model' => TenderChecklist::model()->searchModel(array('tender_id' => $tender_id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'name ASC'),
            'tender_type_model' => $tender_type_model,
        ));
    }

}
