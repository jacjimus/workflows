<?php

class DefaultController extends TendersModuleController
{

    public function init()
    {
         $this->resourceLabel = 'Tender Registration';
        $this->activeMenu = TendersModuleConstants::MENU_TENDERS_REG;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'download', 'create', 'update', 'delete', 'checklist'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->hasPrivilege();
        $model = Doc::model()->loadModel($id);
        if($model->doc_type_id == 2):
        $file = $model->getFilePath();
        //.. get the content of the requested file
        $content = file_get_contents($file);
        $file_name = Common::cleanString($model->name) . '.pdf';
        //.. send appropriate headers
        header("Content-type: application/pdf, application/octet-stream");
        header('Content-Disposition: inline; filename="' . $file_name . '"');
        header("Content-Length: " . filesize($file));
        echo $content;
        else:
            
        $this->redirect("index" , array('category_id' => $model->category_id));
        endif;
    }

    public function actionDownload($id)
    {
        $this->hasPrivilege();
        $model = Doc::model()->loadModel($id);
        $file = $model->getFilePath();
        MyYiiUtils::downloadFile($file);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($category_id = NULL)
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_REGISTER . ' ' . TenderCategories::model()->get($category_id , 'name') . " New Tender " );

        $model = new Tenders(ActiveRecord::SCENARIO_CREATE);
        $model->category_id = $category_id;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
                   
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('default/index', array('category_id' => $model->category_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
            'id' => $category_id
                ), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = Tenders::model()->loadModel($id);
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('default/index', array('category_id' => $model->category_id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array(
            'model' => $model,
            'id' => $id,
                ), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        Doc::model()->loadModel($id)->delete();
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function actionIndex($category_id = NULL)
    {
        $this->resource = TendersModuleConstants::RES_TENDERS_REGISTER;
        $this->hasPrivilege();
        $this->resourceLabel = 'Tender Registration';
        $this->activeTab = TendersModuleConstants::MENU_TENDERS_REG;
        $tender_type_model = NULL;
        if (!empty($category_id)) {
            $tender_type_model = TenderCategories::model()->loadModel($category_id);
            $this->pageTitle = $tender_type_model->name;
        } else
            $this->pageTitle = Lang::t($this->resourceLabel);

        $this->render('index', array(
            'model' => Tenders::model()->searchModel(array('category_id' => $category_id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'tender_date DESC'),
            'tender_type_model' => $tender_type_model,
        ));
    }
    public function actionChecklist($tender_id = NULL)
    {
        $this->resource = TendersModuleConstants::RES_TENDERS_CHECKLIST;
         $this->resourceLabel = 'Tender Checklist';
         $this->hasPrivilege();
        $this->activeTab = TendersModuleConstants::MENU_TENDERS_CHECKLIST;
        $tender_type_model = NULL;
        if (!empty($tender_id)) {
            $tender_type_model = Tenders::model()->loadModel($tender_id);
            $this->pageTitle = $tender_type_model->name;
        } else
            $this->pageTitle = Lang::t($this->resourceLabel);

        $this->render('checklist', array(
            'model' => TenderChecklist::model()->searchModel(array('tender_id' => $tender_id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'tender_date DESC'),
            'tender_type_model' => $tender_type_model,
        ));
    }

}
