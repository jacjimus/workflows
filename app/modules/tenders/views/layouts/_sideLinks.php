<?php if (ModulesEnabled::model()->isModuleEnabled(TendersModuleConstants::MOD_TENDERS)): ?>
      <?php if ($this->showLink(TendersModuleConstants::RES_TENDERS)): ?>
 <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-briefcase"></i> <span class="menu-item-parent"><?php echo Lang::t('Tenders Manager') ?></span></a>
            <ul>
                <?php if ($this->showLink(TendersModuleConstants::RES_TENDERS_REGISTER)): ?>
                <li class="<?php echo $this->activeMenu === TendersModuleConstants::MENU_TENDERS_REG ? 'active' : '' ?>"> <a href="<?php echo Yii::app()->createUrl('tenders/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Tender Registration') ?></span></a></li>
                <?php endif; ?>
                <?php if ($this->showLink(TendersModuleConstants::RES_TENDERS_CHECKLIST)): ?>
                <li class="<?php echo $this->activeMenu === TendersModuleConstants::MENU_TENDERS_CHECKLIST? 'active' : '' ?>"> <a href="<?php echo Yii::app()->createUrl('tenders/checklist/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Tender Checklist') ?></span></a></li>
                 <?php endif; ?>
            </ul>
        </li>

      <?php endif; ?>
<?php endif; ?>