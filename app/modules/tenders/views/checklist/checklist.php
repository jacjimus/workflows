<div class="row">
    <div class="col-md-4">
        <?php $this->renderPartial('tenders.views.categories._tab2') ?>
    </div>
    <div class="col-md-8">
        
        <?php $this->renderPartial('_grid_checklist', array('model' => $model, 'tender_type_model' => $tender_type_model)); ?>
    </div>
</div>