<?php

$grid_id = 'checklist-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t( !empty($tender_type_model) ? $tender_type_model->name. ' requirements' : 'Tender' ),
    'titleIcon' => '<i class="fa fa-file-o"></i>',
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true , 'label' => "Add item"),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'name',
                'value' => 'CHtml::encode($data->name)',
                'type' => 'raw',
            ),
            
            array(
                'name' => 'created_on',
                'value' => 'MyYiiUtils::formatDate($data->created_at)',
                'type' => 'raw',
            ),
            array(
                'name' => 'created_by',
                'value' => 'UsersView::model()->get($data->created_by , "name")',
                'type' => 'raw',
            ),
             
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . TendersModuleConstants::RES_TENDERS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . DocModuleConstants::RES_COMPANY_DOCS . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                     
                )
            ),
        ),
    )
));
?>