<?php

$grid_id = 'tender-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Open Tenders'),
    'titleIcon' => '<i class="fa fa-file-o"></i>',
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true , 'label' => "Register new tender"),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'name',
                'value' => 'CHtml::link(CHtml::encode($data->name), "$data->tender_url", array("target" => "_blank" ,"title" => "Tender download link"))',
                'type' => 'raw',
            ),
            array(
                'name' => 'description',
                'value' => 'MyYiiUtils::myShortenedString($data->description, 20)',
                'type' => 'raw',
            ),
            array(
                'name' => 'tendering_company',
                'value' => '$data->tendering_company',
                'type' => 'raw',
            ),
             
            array(
                'name' => 'closing_date',
                'value' => 'MyYiiUtils::formatDate($data->closing_date)',
                'type' => 'raw',
            ),
            array(
                'name' => 'bind_bond',
                'value' => 'MyYiiUtils::formatMoney($data->bind_bond)',
                'type' => 'raw',
            ),
             
            array(
                'class' => 'ButtonColumn',
                'template' => '{download}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'download' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-download fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("download",array("id"=>$data->id))',
                        'visible' => '$data->category_id == 2?true:false',
                        'options' => array(
                            'class' => '',
                            'title' => Lang::t('Download'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . TendersModuleConstants::RES_TENDERS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . DocModuleConstants::RES_COMPANY_DOCS . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                     
                )
            ),
        ),
    )
));
?>