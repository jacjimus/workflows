<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'category_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                       <?php echo CHtml::activeDropDownList($model, 'category_id', TenderCategories::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
            
                </div>
        </div>
       
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'maxlength' => 255, 'rows' => 3)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'tendering_company', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'tendering_company', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'closing_date', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'closing_date', array('class' => 'form-control show-datepicker', 'maxlength' => 128)); ?>
                </div>
        </div>
        
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'has_addendum', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'has_addendum', array()); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'bind_bond', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                       <?php echo CHtml::activeNumberField($model, 'bind_bond', array('class' => 'form-control', 'maxlength' => 15)); ?>
              
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'tender_url', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                       <?php echo CHtml::activeUrlField($model, 'tender_url', array('class' => 'form-control', 'maxlength' => 255)); ?>
              
                </div>
        </div>
        <?php $this->renderPartial('_file_field', array('model' => $model)); ?>
</div>

<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); 

//Yii::app()->clientScript->registerScript('doctype', '
//    if($(\'#doc_type_id\').val() == "1")
//     $(\'#file-field\').hide(100);
//    $(\'#doc_type_id\').change(function(e){
//    if($(\'#doc_type_id\').val() == "2")
//        $(\'#file-field\').show(1000);
//        else
//        $(\'#file-field\').hide(1000);
//    })
//');