<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('tenders.views.categories._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-fw fa-briefcase"></i>
                    <?php echo CHtml::encode($this->pageTitle); ?>
                    <?php if (!empty($tender_type_model)): ?>
                        &nbsp;
                        <small>
                            <?php if ($this->showLink(TendersModuleConstants::RES_TENDERS, Acl::ACTION_DELETE) && $tender_type_model->canDelete()): ?>
                                <a href="javascript:void(0)" class="text-danger ajax-delete" data-ajax-url="<?php echo $this->createUrl('categories/delete', array('id' => $tender_type_model->id)) ?>" data-confirm-message="<?php echo Lang::t('DELETE_CONFIRM') ?>"><i class="fa fa-trash-o"></i> <?php echo Lang::t(Constants::LABEL_DELETE) ?></a>
                            <?php endif; ?>
                            |
                            <?php if ($this->showLink(TendersModuleConstants::RES_TENDERS, Acl::ACTION_UPDATE)): ?>
                                <a href="<?php echo $this->createUrl('categories/update', array('id' => $tender_type_model->id)) ?>" class="text-success show_modal_form"><i class="fa fa-edit"></i> <?php echo Lang::t(Constants::LABEL_UPDATE) ?></a>
                            <?php endif; ?>
                        </small>
                    <?php endif; ?>
                </h1> 
            </div>
        </div>
        <?php $this->renderPartial('_grid', array('model' => $model)); ?>
    </div>
</div>