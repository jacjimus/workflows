<?php
$data = Tenders::model()->getData();
?>
<?php if ($this->showLink(TendersModuleConstants::RES_TENDERS, Acl::ACTION_CREATE)): ?>
    <h3><i class="fa fa-briefcase"></i> <?php echo Lang::t('Active Tenders') ?></p>
<?php endif; ?>
<div class="list-group my-list-group">
   
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $row): ?>
            <a href="<?php echo $this->createUrl('checklist/index', array('tender_id' => $row['id'])) ?>" class="list-group-item">
                <h4 class="list-group-item-heading"><?php echo CHtml::encode($row['name']) ?> </h4>
                <p class="list-group-item-text text-muted" style="font-size: 10px;"><?php echo CHtml::encode($row['description']) ?></p>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>