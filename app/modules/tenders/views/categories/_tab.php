<?php
$data = TenderCategories::model()->getData();
?>
<?php if ($this->showLink(TendersModuleConstants::RES_TENDERS, Acl::ACTION_CREATE)): ?>
    <p><a class="btn btn-sm btn-default btn-block show_modal_form" href="<?php echo $this->createUrl('categories/create') ?>"><i class="fa fa-play-circle-o"></i> <?php echo Lang::t('Add category') ?></a></p>
<?php endif; ?>
<div class="list-group my-list-group">
    <a href="<?php echo $this->createUrl('default/index') ?>" class="list-group-item">
        <h4 class="list-group-item-heading"><?php echo Lang::t('Tender Categories') ?> <span class="badge"><?php echo Tenders::model()->getTenderCount() ?></span></h4>
        <p class="list-group-item-text"><?php echo Lang::t('Browse all'); ?></p>
    </a>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $row): ?>
            <a href="<?php echo $this->createUrl('default/index', array('category_id' => $row['id'])) ?>" class="list-group-item">
                <h4 class="list-group-item-heading"><?php echo CHtml::encode($row['name']) ?>  <span class="badge"><?php echo Tenders::model()->getTenderCount($row['id']) ?></span></h4>
                <p class="list-group-item-text"><?php echo CHtml::encode($row['description']) ?></p>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>