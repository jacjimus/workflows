<?php

class Newdoc extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
      
       const STATUS_SAVED = "Saved";
       const STATUS_PENDING = "Submitted";
       const STATUS_APPROVED = "Approved";
       const STATUS_PRINTED = "Printed";
       const SEARCH_FIELD = '_search';
      
       
      public $Title;
      public $Doc_template;
      public $name;
      public $td_no;
      public $letter_ref_no;
      public $Academic_year;
      
      
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }
        
         public function getFilePath() {
            return $this->getDir() . DS . $this->doc_file;
      }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_documents';
        }
        
        public function beforeSave() {
            $this->Author = Yii::app()->user->id;
            return parent::beforeSave();
        }
        public function afterFind() {
            $this->CreateDate = date('d-M-Y', strtotime($this->CreateDate));
            $this->Date_departure = date('d-M-Y', strtotime($this->Date_departure));
            return parent::afterFind();
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                    // array('Doc_template, Purpose, Reference, ', 'required' , "message" => "{attribute} should be filled"), 
                    array('Embassy_name,Reference, Type, Salutation, Applicant_name, Passport_no, Destination, Purpose, Date_departure, Occupation' , 'required', 'on' => self::SCENARIO_VISA_REQUEST), 
                    array('Embassy_name,Reference, Salutation, Applicant_name, Passport_no, Destination,  Academic_year, Degree_type, School_name' , 'required', 'on' => self::SCENARIO_NO_OBJECTION), 
                    array('Embassy_name,Reference, letter_ref_no, letter_date, ministry_name' , 'required', 'on' => self::SCENARIO_FORWARD_NOTE), 
                    array('Reference' , 'required', 'on' => self::SCENARIO_IMMIGRATION), 
                    array('Approver, PrintCount, Doc_template', 'numerical', 'integerOnly' => true),
                     array('Reference', 'unique', 'message' => '{attribute} has been used for another document'),
                     array('Reference, Subject', 'length', 'max' => 100),
                     array('Title', 'length', 'max' => 50),
                      array('DocID,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
       

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'DocID' => Lang::t('ID'),
                    'Subject' => Lang::t('Subject'),
                    'Title' => Lang::t('Document title'),
                    'Embassy_name' => Lang::t('Embassy name'),
                    'Type' => Lang::t('Type of Visa'),
                    'Salutation' => Lang::t('Salutation'),
                    'Applicant_name' => Lang::t('Applicant name'),
                    'Passport_no' => Lang::t('Passport number'),
                    'Destination' => Lang::t('Location of Visit'),
                    'Purpose' => Lang::t('Purpose of Travel'),
                    'Date_departure' => Lang::t('Date of Entry/Departure'),
                    'Reference' => Lang::t('Reference'),
                    'Occupation' => Lang::t('Occupation / Company & Position'),
                    'Doc_template' => Lang::t('Template ID'),
                    'CreateDate' => Lang::t('Create Date'),
                    'Academic_year' => Lang::t('Academic year'),
                    'Status' => Lang::t('Status'),
                    'Author' => Lang::t('Author'),
                    'Approver' => Lang::t('Approval'),
                    'PrintCount' => Lang::t('No of Prints'),
                    'letter_ref_no' => Lang::t('Letter Ref No'),
                    'letter_date' => Lang::t('Letter date'),
                    'ministry_name' => Lang::t('Ministry name'),
                    'School_name' => Lang::t('School name'),
                    'Degree_type' => Lang::t('Degree type'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                array('Reference', self::SEARCH_FIELD, true, 'OR'),
                array('Embassy_name', self::SEARCH_FIELD, true, 'OR'),
                array('Type', self::SEARCH_FIELD, true, 'OR'),
                array('Applicant_name', self::SEARCH_FIELD, true, 'OR'),
                array('Passport_no', self::SEARCH_FIELD, true, 'OR'),
                array('Destination', self::SEARCH_FIELD, true, 'OR'),
                array('Purpose', self::SEARCH_FIELD, true, 'OR'),
                array('Destination', self::SEARCH_FIELD, true, 'OR'),
                array('Date_departure', self::SEARCH_FIELD, true, 'OR'),
                array('Doc_template', self::SEARCH_FIELD, true, 'OR'),
                array('Status', self::SEARCH_FIELD, true, 'OR'),
                array('Title', self::SEARCH_FIELD, true, 'OR'),
                
            );
      }
        

     /**
       * Get doc count
       * @param type $doc_type_id
       * @return int
       */
      public function getDocCount($doc_type = NUll) {
            $conditions = "";
            $params = array();
            if (!empty($doc_type)) {
                  $conditions.='`Doc_template`=:doc_template';
                  $params[':doc_template'] = $doc_type;
            }
            
            return $this->getTotals($conditions, $params);
      }

}

