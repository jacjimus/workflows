<div class="alert hidden" id="my-modal-notif"></div>



<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Reference', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, "Reference", array('class' => "form-control")); ?>
        <?php echo CHtml::activeHiddenField($model, "Doc_template", array('value' => $id)); ?>


        <?php echo CHtml::error($model, 'Reference') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Embassy_name', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'Embassy_name', array('class' => "form-control")); ?>

        <?php echo CHtml::error($model, 'Embassy_name') ?>
    </div>
</div>


<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'letter_ref_no', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'letter_ref_no', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'letter_ref_no') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'letter_date', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'letter_date', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'letter_date') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'ministry_name', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-8">
        <?php echo CHtml::activeTextField($model, 'ministry_name', array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'ministry_name') ?>
    </div>
</div>


<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-success" name="forward" type="submit" ><i class="icon-forward bigger-110"></i> <?php echo Lang::t('Save Document') ?></button>
</div>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 50,
        'convertDivs' => true,
        'cleanup' => TRUE,
        'paragraphy' => false,
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));
?>