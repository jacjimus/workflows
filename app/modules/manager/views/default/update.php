<?php
$this->breadcrumbs = array('Antenatal Module ' => 'manage',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
   
    <div class="widget-header">
        <h4 class="col-md-6"><?php echo CHtml::encode($this->pageTitle); ?></h4>
        <h3 class="col-md-4"><?php echo CHtml::link("Edit" , array("update", 'id'=>$model->DocID))?></h3>
     </div>
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                <?php $this->renderPartial('_form', array('model' => $model)); ?>
                        </div>
                </div>
        </div>
</div>
