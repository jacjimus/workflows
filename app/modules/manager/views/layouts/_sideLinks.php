
<?php
if (ModulesEnabled::model()->isModuleEnabled(ManagerModuleConstants::MOD_DOC_MANAGER)): ?>
      <?php if ($this->showLink(ManagerModuleConstants::RES_DOC)): ?>
        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-folder-open-o"></i> <span class="menu-item-parent"><?php echo Lang::t('Documents Manager') ?></span></a>
            <ul>
                <?php if($this->showLink(ManagerModuleConstants::RES_DOC_CATEGORIES)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_CATEGORY ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('doc/docCategories/index') ?>"><i class="fa fa-lg fa-fw fa-file"></i> <span class="menu-item-parent"><?php echo Lang::t('Document categories') ?></span></a></li>
                <?php endif; ?>
                <?php if($this->showLink(ManagerModuleConstants::RES_DOC_MANAGE)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_MANAGER ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/index') ?>"><i class="fa fa-lg fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Kons Documents') ?></span></a></li>
            <?php endif; ?>
            </ul>
        </li>
            <?php endif; ?>
<?php endif; ?>