<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class HelpModuleConstants {

        //resources constants
        const RES_DOCUMENTATION = 'DOCUMENTATION';

}
