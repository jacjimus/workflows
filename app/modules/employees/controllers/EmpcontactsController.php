<?php

class EmpcontactsController extends EmployeesModuleController
{

    const LOG_ACTIVITY = 'activity_log';
    const LOG_LOGIN = 'login_log';

    /**
     * Stores the user level of a user;
     * @var type
     */
    public $userLevel;

    public function init()
    {
        $this->resource = EmployeesModuleConstants::RES_EMPLOYEE_DETAILS;
        $this->activeMenu = self::MENU_EMPLOYEES_EMPLOYEES;
        $this->activeTab = EmployeesModuleConstants::TAB_CONTACTS;
        $this->resourceLabel = 'Contacts';
        parent::init();
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
                ), FALSE, TRUE);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($empid)
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

        $model = new Empcontacts();
        $modelClassName = $model->getClassName();
        $model->emp_id = $empid;

        if (isset($_POST[$modelClassName])) {
            $model->attributes = $_POST[$modelClassName];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);

            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                Yii::app()->end();
            }

            $model->save(FALSE);
            echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'),
                'redirectUrl' => $this->createUrl('/employees/empcontacts/view', array('id' => $model->emp_id))));
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);

        $model = Empcontacts::model()->loadModel($id);
        $modelClassName = $model->getClassName();

        if (isset($_POST[$modelClassName])) {
            $model->attributes = $_POST[$modelClassName];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);

            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
                Yii::app()->end();
            }

            $model->save(FALSE);
            echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => $this->createUrl('index')));
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), FALSE, TRUE);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($empid)
    {
        $this->hasPrivilege();
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = Employeeslist::model()->loadModel($empid);
        $this->render('index', array(
            'search_model' => Empcontacts::model()->searchModel(array('emp_id' => $empid), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'id desc'),
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Empcontacts('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Empcontacts']))
            $model->attributes = $_GET['Empcontacts'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Empcontacts::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'empcontacts-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
