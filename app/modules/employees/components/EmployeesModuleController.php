<?php

/**
 * @author Fred <mconyango@gmail.com>
 * Parent controller for the Employees module
 */
class EmployeesModuleController extends Controller {

    const MENU_EMPLOYEES_EMPLOYEES = 'EMPLOYEES_EMPLOYEES';

    public function init() {
        parent::init();
    }

    public function setModulePackage() {
        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
            ),
            'css' => array(
                'css/module.css',
            ),
        );
    }

}
