<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class EmployeesModuleConstants {

    //resources constants
    const MOD_EMPLOYEESDETAILS = 'employees';
    const RES_EMPLOYEE_DETAILS = 'EMPLOYEE_DETAILS';
    const RES_EMPLOYEE_LIST = 'EMPLOYEE_LIST';
    const RES_EMPLOYEES_SETTINGS = 'EMPLOYEES_SETTINGS';
    //tab constants
    const TAB_BIODATA = 'TAB_BIODATA';
    const TAB_JOB_DETAILS = 'TAB_JOB_DETAILS';
    const TAB_BANK_DETAILS = 'TAB_BANK_DETAILS';
    const TAB_DEPENDANTS = 'TAB_DEPENDANTS';
    const TAB_CONTACTS = 'TAB_CONTACTS';
    const TAB_QUALIFICATIONS = 'TAB_QUALIFICATIONS';
    const TAB_WORK_EXPERIENCE = 'TAB_WORK_EXPERIENCE';
    const TAB_SKILLS = 'TAB_SKILLS';
    const TAB_HOUSING = 'TAB_HOUSING';
    const TAB_STATUTORY = 'TAB_STATUTORY';
    const TAB_TRAININGS = 'TAB_TRAININGS';

}
