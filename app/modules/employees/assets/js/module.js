/*
 *Module based js functions
 */
var EmployeeModule = {
    Employee: {
        Paye: {
            init: function() {
                this.toggle_paye();

            },
            toggle_paye: function() {
                'use strict';
                var selector = '#Empstatutory_paye',
                        paye_amount_wrapper_id = 'paye',
                        toggle_paye = function(e) {
                            var val = $(e).val();
                            if (val == 3) {
                                $('#' + paye_amount_wrapper_id).show();
                            }
                            else {
                                $('#' + paye_amount_wrapper_id).hide();
                            }
                        };
                //onload event
                toggle_paye(selector);
                //onchange event
                $(selector).on('change', function() {
                    toggle_paye(this);
                });
            }
        },
        NSSF: {
            init: function() {
                this.toggle_nssf();
            },
            toggle_nssf: function() {
                'use strict';
                var selector = '#Empstatutory_nssf',
                        nssf_amount_wrapper_id = 'nssf',
                        toggle_nssf = function(e) {
                            var val = $(e).val();
                            if (val == 3) {
                                $('#' + nssf_amount_wrapper_id).show();
                            }
                            else {
                                $('#' + nssf_amount_wrapper_id).hide();
                            }
                        };
                //onload event
                toggle_nssf(selector);
                //onchange event
                $(selector).on('change', function() {
                    toggle_nssf(this);
                });
            }
        },
        NHIF: {
            init: function() {
                this.toggle_nhif();
            },
            toggle_nhif: function() {
                'use strict';
                var selector = '#Empstatutory_nhif',
                        nhif_amount_wrapper_id = 'nhif',
                        toggle_nhif = function(e) {
                            var val = $(e).val();
                            if (val == 3) {
                                $('#' + nhif_amount_wrapper_id).show();
                            }
                            else {
                                $('#' + nhif_amount_wrapper_id).hide();
                            }
                        };
                //onload event
                toggle_nhif(selector);
                //onchange event
                $(selector).on('change', function() {
                    toggle_nhif(this);
                });
            }
        },
        Housing: {
            init: function() {
                this.toggle_emphousing();

            },
            toggle_emphousing: function() {
                'use strict';
                var selector = '#Emphousing_housing_type',
                        ownhouse_wrapper_id = 'ownhouse',
                        housedbyemp_wrapper_id = 'housedbyemployer',
                        toggle_emphousing = function(e) {
                            var val = $(e).val();
                            if (val == 2) {
                                $('#' + ownhouse_wrapper_id).show();
                                $('#' + housedbyemp_wrapper_id).hide();
                            }
                            else if (val == 1) {
                                $('#' + ownhouse_wrapper_id).hide();
                                $('#' + housedbyemp_wrapper_id).show();
                            } else if (val == 3) {
                                $('#' + ownhouse_wrapper_id).hide();
                                $('#' + housedbyemp_wrapper_id).hide();
                            }
                        };
                //onload event
                toggle_emphousing(selector);
                //onchange event
                $(selector).on('change', function() {
                    toggle_emphousing(this);
                });
            }
        },
    }
};


