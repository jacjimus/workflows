<?php
$can_delete=  Acl::hasPrivilege($this->privileges,$this->resource,'delete',FALSE);
$can_update=  Acl::hasPrivilege($this->privileges,$this->resource,'update',FALSE);
$can_create=  Acl::hasPrivilege($this->privileges,$this->resource,'create',FALSE);
$grid_id='dependants-grid';
Yii::app()->clientScript->registerScript('search', "
$('#active-search-form').submit(function(){
	$.fn.yiiGridView.update('".$grid_id."', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
         <div class="btn-group">
             <a class="btn" href="<?php echo $this->createUrl('dependants/create')?>"><i class="icon-plus-sign"></i> Add Dependant</a>
        </div>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
                'id'=>$grid_id,
                'dataProvider'=>$model->search($this->settings['pagination']),
                'cssFile'=>Yii::app()->theme->baseUrl.'/widgets/gridview/styles.css',
                'pagerCssClass'=>'paginator',
                'itemsCssClass'=>'table table-bordered table-striped',
                'pager'=>array('cssFile'=>Yii::app()->theme->baseUrl.'/widgets/css/pager.css','header'=>  Yii::t($this->lang,'Go to page'),),
                'summaryText'=>Yii::t($this->lang,'summary_text'),
                'enableSorting'=>true,
	'columns'=>array(
                                'id',
                                'name',
                                'email',
                                
                                array(
                                    'class'=>'CButtonColumn',
                                    'template'=>'{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                                    'deleteConfirmation'=>Yii::t($this->lang,'GENERIC_CONFIRM'),
                                    'afterDelete'=>'function(link,success,data){if(success) {$("#statusMsg").html(data);}}',
                                    'htmlOptions'=>array(
                                      'style'=>'width:130px;text-align:center',
                                    ),
                                    'buttons'=>array(
                                         'view'=>array(
                                            'imageUrl'=> false,
                                            'url'=>'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                            'label'=>'<i class="icon-zoom-in"></i>',
                                            'options'=>array(
                                               'class'=>'btn btn-small btn-info',
                                                'title'=>'View',
                                             ),
                                         ),
                                         'update'=>array(
                                            'imageUrl'=> false,
                                             'url'=>'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                                            'label'=>'<i class="icon-edit"></i>',
                                             'visible'=>'"'.$can_update.'"?true:false',
                                             'options'=>array(
                                               'class'=>'btn btn-small btn-info',
                                                'title'=>'Edit',
                                                 
                                             ),
                                         ),
                                         'delete'=>array(
                                            'imageUrl'=> false,
                                             'url'=>'Yii::app()->controller->createUrl("dependants/delete",array("id"=>$data->id))',
                                            'label'=>'<i class="icon-remove"></i>',
                                             'visible'=>'"'.$can_delete.'"?true:false',
                                             'options'=>array(
                                               'class'=>'btn btn-small btn-danger',
                                                'title'=>'Delete'
                                             ),
                                         ),
                                      )
	          ),
	),
)); ?>