
<div class="col-xs-12 col-sm-12">
    <div class="detail-view" >
        <?php
        $this->widget('application.components.widgets.DetailView', array(
            'data' => $model,
            'attributes' => array(
                'id',
                'name',
                array(
                    'header' => Lang::t('Relation'),
                    'name' => Lang::t('relationship_id'),
                    'value' => Relations::model()->get($model->relationship_id, "rel_name"),
                ),
                'email',
                'dob',
                'mobile',
            ),
        ));
        ?>
    </div>

</div>