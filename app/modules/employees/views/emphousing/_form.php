<?php
/* @var $this emphousingController */
/* @var $model emphousing */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="panel-title"><i class="fa fa-plus-circle blue"></i> <?php echo CHtml::encode(Lang::t('Add Housing')) ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>

        <?php echo $form->hiddenField($model, 'emp_id'); ?>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'housing_type', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeDropDownList($model, 'housing_type', EmployeeHousingtype::model()->getListData('id', 'housing_type'), array('class' => 'form-control')); ?>
                </div>
            </div>
        </div>
        <div id="housedbyemployer">
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'housing_value', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php echo CHtml::activeTextField($model, 'housing_value', array('class' => 'form-control')); ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'employer_rent', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php echo CHtml::activeTextField($model, 'employer_rent', array('class' => 'form-control')); ?>

                    </div>
                </div>
            </div>
        </div>

        <div id="ownhouse" class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'allowable_occupier', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'allowable_occupier', array('class' => 'form-control')); ?>

                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('employees.emphousing._form', "EmployeeModule.Employee.Housing.init();")
?>