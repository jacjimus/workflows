
<?php

$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        array(
            'name' => 'housing_type',
            'value' => EmployeeHousingtype::model()->get($model->housing_type, 'housing_type'),
        ),
        'housing_value',
        'employer_rent',
        'allowable_occupier',
    ),
));
?>
 