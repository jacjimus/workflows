<?php
/* @var $this JobsTitlesController */
/* @var $model JobsTitles */

$this->breadcrumbs=array(
	'Jobs Titles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List JobsTitles', 'url'=>array('index')),
	array('label'=>'Create JobsTitles', 'url'=>array('create')),
	array('label'=>'Update JobsTitles', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete JobsTitles', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JobsTitles', 'url'=>array('admin')),
);
?>

<h1>View JobsTitles #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'job_title',
		'min_salary',
		'max_salary',
		'date_created',
		'created_by',
	),
)); ?>
