<?php
/* @var $this JobsTitlesController */
/* @var $model JobsTitles */

$this->breadcrumbs=array(
	'Jobs Titles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JobsTitles', 'url'=>array('index')),
	array('label'=>'Create JobsTitles', 'url'=>array('create')),
	array('label'=>'View JobsTitles', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage JobsTitles', 'url'=>array('admin')),
);
?>

<h1>Update JobsTitles <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>