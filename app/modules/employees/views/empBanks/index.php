<?php
$this->breadcrumbs = array(
    Lang::t('Banks') => array('employees/index'),
    // CHtml::encode($model->vehicle_reg) => array('employees/view', 'id' => $model->id),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('employees.views.employees._sidebar', array('model' => $model)) ?>
    </div>
    <div class="col-md-10">
        <?php $this->renderPartial('employees.views.employees._tab', array('model' => $model)) ?>
        <div class="padding-top-10">
            <?php $this->renderPartial('_grid', array('model' => $search_model)); ?>
        </div>
    </div>
</div>


