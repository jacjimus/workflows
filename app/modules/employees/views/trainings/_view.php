<div class="col-xs-12 col-sm-12">
    <div class="detail-view" >
        <?php $this->widget('zii.widgets.CDetailView', array(
                        'data'=>$model,
                        'attributes'=>array(
                                array(
                                    'name'=>'bank_id',
                                    'value'=>$model->bank->bank_name,
                                ),
                                array('name'=>'bank_branch_id',
                                    'value'=>$model->bankBranch->branch_name
                                ),                           
                                'account_no',
                                array(
                                    'name'=>'paying_acc',
                                    'value'=>$model->paying_acc==1?"Yes":"No",
                                ),
                        ),
                )); ?>
    </div>

</div>


