<?php

$grid_id = 'empbanks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Employee Bank Details'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            'id',
			'institution',
            
            'training',
            'start_date',
            'end_date',
            'notes',
            
            array(
                'class' => 'ButtonColumn',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                   'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/trainings/update",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Edit',
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/trainings/delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'options' => array(
                            'class' => 'delete',
                            'title' => 'Delete',
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
