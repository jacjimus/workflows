
<?php if (ModulesEnabled::model()->isModuleEnabled(EmployeesModuleConstants::MOD_EMPLOYEESDETAILS)): ?>
    <?php if ($this->showLink(EmployeesModuleConstants::RES_EMPLOYEE_DETAILS)): ?>
        <li class="<?php echo $this->activeMenu === EmployeesModuleController::MENU_EMPLOYEES_EMPLOYEES ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('employees/employees/index') ?>"><i class="fa fa-lg fa-fw fa-user-md green"></i> <span class="menu-item-parent"><?php echo Lang::t('Employees') ?></span></a>
        </li>
    <?php endif; ?>
<?php endif; ?>