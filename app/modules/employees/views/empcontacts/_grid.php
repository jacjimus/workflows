<?php

$grid_id = 'empbanks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Employee Contacts'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            'id',
            array(
                'name' => 'category_id',
                'value' => 'PhoneCategories::model()->get($data->category_id,"phone_cat_desc")',
            ),
            'phone',
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empcontacts/view",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'class' => 'blue',
                            'title' => 'View details',
                            'onclick' => 'return MyUtils.showColorbox(this.href,false)',
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empcontacts/update",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => 'Edit',
                            'onclick' => 'return MyUtils.showColorbox(this.href,false)',
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empcontacts/delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'options' => array(
                            'class' => 'delete',
                            'title' => 'Delete',
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
