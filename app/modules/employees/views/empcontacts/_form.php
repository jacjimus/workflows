<?php
/* @var $this DependantsController */
/* @var $model Dependants */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="panel-title"><i class="fa fa-plus-circle blue"></i> <?php echo CHtml::encode(Lang::t('Add Contacts')) ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>

        <?php echo $form->hiddenField($model, 'emp_id'); ?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'phone', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextField($model, 'phone', array('class' => 'form-control')); ?>
            </div>
        </div>


        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'category_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeDropDownList($model, 'category_id', PhoneCategories::model()->getListData('id', 'phone_cat_desc'), array('class' => 'form-control')); ?>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>

