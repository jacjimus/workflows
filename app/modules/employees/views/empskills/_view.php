
<?php

$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'skill_id',
            'value' => Skills::model()->get($model->skill_id, 'skill'),
        ),
        'start_date',
        'end_date',
        array(
            'name' => 'proficiency_id',
            'value' => Proficiency::model()->get($model->proficiency_id, 'prof_name'),
        ),
		
        'notes',
        'certification',
        'certifying_body',
		array(
			'name'=>'country_of_inst',
			'value'=>$model->getCountry()
		),
        'competencies',
        'comp_used',
        'comp_not_used',
    ),
));
?>











