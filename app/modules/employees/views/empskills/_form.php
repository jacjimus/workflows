<?php
/* @var $this workexperienceController */
/* @var $model workexperience */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="panel-title"><i class="fa fa-plus-circle blue"></i> <?php echo CHtml::encode(Lang::t('Add Skills')) ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>

        <?php echo $form->hiddenField($model, 'emp_id'); ?>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'skill_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeDropDownList($model, 'skill_id', Skills::model()->getListData('id', 'skill'), array('class' => 'form-control')); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'start_date', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'start_date', array('class' => 'form-control show-datepicker')); ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'end_date', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'end_date', array('class' => 'form-control show-datepicker')); ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div> 
		<div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'certification', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'certification', array('class' => 'form-control')); ?>
                 </div>
            </div>
        </div>
		<div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'certifying_body', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'certifying_body', array('class' => 'form-control')); ?>
                 </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'proficiency_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeDropDownList($model, 'proficiency_id', Proficiency::model()->getListData('id', 'prof_name'), array('class' => 'form-control')); ?>
                </div>
            </div>
        </div> 
		<div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'country_of_inst', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php echo CHtml::activeDropDownList($model, 'country_of_inst', MeCountry::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'notes', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'notes', array('class' => 'form-control', 'rows' => 6, 'cols' => 50)); ?>
            </div>
        </div> 
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'competencies', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'competencies', array('class' => 'form-control', 'rows' => 6, 'cols' => 50)); ?>
            </div>
        </div> <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'comp_used', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'comp_used', array('class' => 'form-control', 'rows' => 6, 'cols' => 50)); ?>
            </div>
        </div>  <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'comp_not_used', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'comp_not_used', array('class' => 'form-control', 'rows' => 6, 'cols' => 50)); ?>
            </div>
        </div>      


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>

