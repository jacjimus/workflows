<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-12">

        <div class="padding-top-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="panel-title"> <?php echo Lang::t('Training & Development for ') . $empmodel->empname; ?></h3>
                </div>
                <?php $this->renderPartial('_view', array('model' => $model)); ?>

            </div>
        </div>
    </div>

</div>
</div>
