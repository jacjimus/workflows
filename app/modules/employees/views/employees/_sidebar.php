
<div class="well well-sm">
    <div class="profile-picture">
        <?php echo CHtml::image(PersonImages::model()->getProfileImageUrl($model->id, 256, 256), CHtml::encode($model->empname), array('id' => 'avator', 'class' => 'editable img-responsive thumbnail')); ?>
    </div>
    <ul class="list-unstyled">
        <?php if ($this->showLink($this->resource, Acl::ACTION_UPDATE)): ?>
            <li><a class="btn btn-link" href="<?php echo $this->createUrl('/employees/employees/update', array('id' => $model->id)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Edit Details') ?></a></li>
            <?php endif; ?>
    </ul>
</div>

