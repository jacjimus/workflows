<?php
if (!isset($label_size)):
    $label_size = 2;
endif;
if (!isset($input_size)):
    $input_size = 8;
endif;
$label_class = "col-md-{$label_size} control-label";
$input_class = "col-md-{$input_size}";
$half_input_size = $input_size / 2;
$half_input_class = "col-md-{$half_input_size}";
?>

<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Company Name') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'company_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('company_name'))); ?>
        <?php echo CHtml::error($model, 'company_name') ?>
    </div>
</div>
<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Emp Code') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'emp_code', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('emp_code'))); ?>
        <?php echo CHtml::error($model, 'emp_code') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'job_title_id', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'job_title_id', JobsTitles::model()->getListData('id', 'job_title'), array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'job_title_id') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'branch_id', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'branch_id', SettingsLocation::model()->getListData('id', 'name', false),array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'branch_id') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'department_id', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'department_id', SettingsDepartment::model()->getListData('id', 'name'),array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'department_id') ?>
    </div>
</div>


<hr>

<div class="form-group">
     <?php echo CHtml::activeLabelEx($model, 'division', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'division', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('division'))); ?>
        <?php echo CHtml::error($model, 'division') ?>
    </div>
</div>
<div class="form-group">
     <?php echo CHtml::activeLabelEx($model, 'accountabilities', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextArea($model, 'accountabilities', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('accountabilities'))); ?>
        <?php echo CHtml::error($model, 'accountabilities') ?>
    </div>
</div>


<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Type') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'employment_class_id', Employmentclass::model()->getListData('id', 'empclass'),array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'employment_class_id') ?>
    </div>
</div>
<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Category') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeDropDownList($model, 'employment_cat_id', EmployementCategories::model()->getListData('id', 'categoryname'),array('class' => 'select2')); ?>&nbsp;&nbsp;
        <?php echo CHtml::error($model, 'employment_cat_id') ?>
    </div>
</div>



<hr>
<header>
        <h2><?php echo Lang::t('Personal Contacts') ?></h2>
    </header>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'postal_address', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'postal_address', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('postal_address'))); ?>
        <?php echo CHtml::error($model, 'postal_address') ?>
    </div>
</div><div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'email', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'email', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('email'))); ?>
        <?php echo CHtml::error($model, 'email') ?>
    </div>
</div><div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'email_2', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'email_2', array('class' => 'form-control', 'maxlength' => 30)); ?>
        <?php echo CHtml::error($model, 'email_2') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'mobile', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'mobile', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('mobile'))); ?>
        <?php echo CHtml::error($model, 'mobile') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'company_phone', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'company_phone', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('company_phone'))); ?>
        <?php echo CHtml::error($model, 'company_phone') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'company_phone_ext', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'company_phone_ext', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('company_phone_ext'))); ?>
        <?php echo CHtml::error($model, 'company_phone_ext') ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'phy_address', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'phy_address', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('phy_address'))); ?>
        <?php echo CHtml::error($model, 'phy_address') ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'phy_address2', array('class' => $label_class)); ?>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'phy_address2', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('phy_address2'))); ?>
        <?php echo CHtml::error($model, 'phy_address2') ?>
    </div>
</div>



