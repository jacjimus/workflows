
<div class="panel panel-default">

    <div id="address_info" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="detail-view">

                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
                        'emp_code',
                        'hire_date',
                        'job_title_id',
                        'dept_name',
                        'manager_id',
                        'work_hours_perday',
                        'employment_class_id',
                        'employment_cat_id',
                        'pay_type_id',
                        'currency_id',
                        'salary',
                        'employment_status',
                        'email',
                        'mobile',
                        'company_phone',
                        'company_phone_ext',
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>