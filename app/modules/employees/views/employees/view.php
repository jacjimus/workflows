<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('employees.views.employees._sidebar', array('model' => $model)) ?>
    </div>

    <div class="col-md-10">
        <?php $this->renderPartial('employees.views.employees._tab', array('model' => $model)) ?>
        <div class="padding-top-10">
            <?php if (!empty($next_insurance_renewal_date)): ?>
                <div class="alert alert-info">
                    <i class="fa fa-info-circle"></i> <?php echo Lang::t('Next insurence renewal date') ?>:
                    <b><?php echo MyYiiUtils::formatDate($next_insurance_renewal_date, 'M j,Y') ?></b>
                </div>
            <?php endif; ?>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Lang::t('Bio Data') ?></h3>
                </div>
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        'id',
						array(
							'name'=>'Full Name',
							'value'=>$model->first_name." ".$model->last_name,
						),
						array(
							'name'=>'Date of Birth',
							'value'=>$model->birthdate,
						),
						
						'place_of_birth',
                        'email',
                        'gender',
                        array(
                            'name' => 'marital_status_id',
                            'Label' => Lang::t('Marital Status'),
                            'value' => PersonMaritalStatus::model()->get($model->marital_status_id, 'name'),
                        ),
						'spouse',
						'next_of_kin_name',
						'next_of_kin_phone',
						'next_of_kin_email',
                    ),
                ));
                ?>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><?php echo Lang::t('Job Details') ?></h3></div>
                    <?php
                    $this->widget('application.components.widgets.DetailView', array(
                        'data' => $model,
                        'attributes' => array(
                           'company_name',
                            'job_title',
                            'dept_name',
                            'work_hours_perday',
                            'empclass',
                            'categoryname',
                            'pay_type_name',
                            'supervises',
                            'accountabilities',
							'division',
							'postal_address',
                            'email',
                            'email_2',
                            'mobile',
                            'company_phone',
                            'company_phone_ext',
							'postal_address',
							'phy_address',
							'phy_address2'
                        ),
                    ));
                    ?>
            </div>
        </div>
    </div>
</div>
