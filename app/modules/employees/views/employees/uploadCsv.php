<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">

    <div class="col-md-12">
        <?php $this->renderPartial('_uploadCsvForm', array('model' => $model)); ?>
    </div>
</div>

