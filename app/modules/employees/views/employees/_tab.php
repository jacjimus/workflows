<ul class="nav nav-tabs my-nav">
    <li class="<?php echo $this->activeTab === EmployeesModuleConstants::TAB_BIODATA ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('employees/employees/view', array('id' => $model->id)) ?>"><?php echo Lang::t('Employee Information') ?></a></li>
    <!--<li class="<?php //echo $this->activeTab === EmployeesModuleConstants::TAB_BANK_DETAILS ? 'active' : '' ?>"><a href="<?php //echo Yii::app()->createUrl('employees/empBanks/index', array('empid' => $model->id)) ?>"><?php //echo Lang::t('Bank Details') ?></a></li>-->
    <!--<li class="<?php //echo $this->activeTab === EmployeesModuleConstants::TAB_DEPENDANTS ? 'active' : '' ?>"><a href="<?php //echo Yii::app()->createUrl('employees/dependants/index', array('empid' => $model->id)) ?>"><?php //echo Lang::t('Dependants') ?></a></li>-->
    <!--<li class="<?php //echo $this->activeTab === EmployeesModuleConstants::TAB_CONTACTS ? 'active' : '' ?>"><a href="<?php //echo Yii::app()->createUrl('employees/empcontacts/index', array('empid' => $model->id)) ?>"><?php //echo Lang::t('Contacts') ?></a></li>-->
    <li class="<?php echo $this->activeTab === EmployeesModuleConstants::TAB_QUALIFICATIONS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('employees/empQualifications/index', array('empid' => $model->id)) ?>"><?php echo Lang::t('Educational Background') ?></a></li>
    <li class="<?php echo $this->activeTab === EmployeesModuleConstants::TAB_WORK_EXPERIENCE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('employees/workexperience/index', array('empid' => $model->id)) ?>"><?php echo Lang::t('Past Work Experience') ?></a></li>
    <li class="<?php echo $this->activeTab === EmployeesModuleConstants::TAB_SKILLS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('employees/empskills/index', array('empid' => $model->id)) ?>"><?php echo Lang::t('Professional Certifications') ?></a></li>
    <li class="<?php echo $this->activeTab === EmployeesModuleConstants::TAB_TRAININGS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('employees/trainings/index', array('empid' => $model->id)) ?>"><?php echo Lang::t('Training & Development') ?></a></li>
    <!--li class="<?php //echo $this->activeTab === EmployeesModuleConstants::TAB_HOUSING ? 'active' : '' ?>"><a href="<?php //echo Yii::app()->createUrl('employees/emphousing/index', array('empid' => $model->id)) ?>"><?php //echo Lang::t('Housing') ?></a></li-->
    
</ul>


