<?php
/* @var $this EmpstatutoryController */
/* @var $model Empstatutory */

$this->breadcrumbs=array(
	'Empstatutories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Empstatutory', 'url'=>array('index')),
	array('label'=>'Manage Empstatutory', 'url'=>array('admin')),
);
?>

<h1>Create Empstatutory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>