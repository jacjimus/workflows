<?php

$grid_id = 'empbanks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Skills'),
    'titleIcon' => '<i class="fa fa-money"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            array(
                'name' => 'paye',
                'value' => '$data->paye0->statutory_action',
            ),
            array(
                'name' => 'nssf',
                'value' => '$data->nssf0->statutory_action',
            ),
            array(
                'name' => 'nhif',
                'value' => '$data->nhif0->statutory_action',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empstatutory/view",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'title' => Lang::t(Constants::LABEL_VIEW),
                            'class' => 'show_modal_form',
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empstatutory/update",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("/employees/empstatutory/delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . EmployeesModuleConstants::RES_EMPLOYEE_DETAILS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'options' => array(
                            'class' => 'delete',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
