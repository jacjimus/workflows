<?php
/* @var $this workexperienceController */
/* @var $model workexperience */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
$actions = CHtml::listData(StatutoryActions::model()->findAll(), 'id', 'statutory_action');
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="panel-title"><i class="fa fa-plus-circle blue"></i> <?php echo CHtml::encode(Lang::t('Add Statutory')) ?></h3>
    </div>
    <div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>

        <?php echo $form->hiddenField($model, 'emp_id'); ?>


        <div class="form-group">
            <?php echo $form->labelEx($model, 'paye', array('class' => 'col-lg-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php
                    echo CHtml::activeDropDownList($model, 'paye', $actions, array('prompt' => 'Select an Option'));
                    ?>

                </div>
            </div>
        </div>

        <div id="paye">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'paye_amount', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php echo CHtml::activeTelField($model, 'paye_amount', array('size' => 18, 'maxlength' => 18)); ?>

                    </div>
                </div>
            </div>

        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'nssf', array('class' => 'col-lg-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php
                    echo CHtml::activeDropDownList($model, 'nssf', $actions, array('prompt' => 'Select an Option'));
                    ?>

                </div>
            </div>
        </div>

        <div id="nssf">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'nssf_amount', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php echo CHtml::activeTelField($model, 'nssf_amount', array('size' => 18, 'maxlength' => 18)); ?>

                    </div>
                </div>
            </div>

        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'nhif', array('class' => 'col-lg-3 control-label')); ?>
            <div class="col-md-4">
                <div class="input-group">
                    <?php
                    echo CHtml::activeDropDownList($model, 'nhif', $actions, array('prompt' => 'Select an Option'));
                    ?>

                </div>
            </div>
        </div>

        <div id="nhif">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'nhif_amount', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-md-4">
                    <div class="input-group">
                        <?php echo CHtml::activeTelField($model, 'nhif_amount', array('size' => 18, 'maxlength' => 18)); ?>

                    </div>
                </div>
            </div>

        </div>  

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php
Yii::app()->clientScript->registerScript('employees.empsstatutory._colorboxForm', "EmployeeModule.Employee.Paye.init();EmployeeModule.Employee.NSSF.init();EmployeeModule.Employee.NHIF.init();", CClientScript::POS_READY);
?>