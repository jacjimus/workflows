
<?php

$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'paye',
            'value' => $model->paye0->statutory_action,
        ),
        array(
            'name' => 'nssf',
            'value' => $model->nssf0->statutory_action,
        ),
        array(
            'name' => 'nhif',
            'value' => $model->nhif0->statutory_action,
        ),
    ),
));
?>
