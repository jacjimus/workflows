<?php
/* @var $this EmpstatutoryController */
/* @var $model Empstatutory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paye'); ?>
		<?php echo $form->textField($model,'paye'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paye_amount'); ?>
		<?php echo $form->textField($model,'paye_amount',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nssf'); ?>
		<?php echo $form->textField($model,'nssf'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nssf_amount'); ?>
		<?php echo $form->textField($model,'nssf_amount',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nhif'); ?>
		<?php echo $form->textField($model,'nhif'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nhif_amount'); ?>
		<?php echo $form->textField($model,'nhif_amount',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->