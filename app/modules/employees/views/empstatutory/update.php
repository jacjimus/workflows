<?php
/* @var $this EmpstatutoryController */
/* @var $model Empstatutory */

$this->breadcrumbs=array(
	'Empstatutories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Empstatutory', 'url'=>array('index')),
	array('label'=>'Create Empstatutory', 'url'=>array('create')),
	array('label'=>'View Empstatutory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Empstatutory', 'url'=>array('admin')),
);
?>

<h1>Update Empstatutory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>