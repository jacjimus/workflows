<?php
/* @var $this QualificationlevelsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Qualificationlevels',
);

$this->menu=array(
	array('label'=>'Create Qualificationlevels', 'url'=>array('create')),
	array('label'=>'Manage Qualificationlevels', 'url'=>array('admin')),
);
?>

<h1>Qualificationlevels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
