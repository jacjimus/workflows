<?php

/**
 * This is the model class for table "hr_emphousing".
 *
 * The followings are the available columns in table 'hr_emphousing':
 * @property integer $id
 * @property integer $emp_id
 * @property integer $housing_type
 * @property string $housing_value
 * @property string $employer_rent
 * @property string $allowable_occupier
 * @property integer $created_by
 * @property string $date_created
 */
class Emphousing extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Emphousing the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_emphousing';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id,housing_type', 'required'),
            array('emp_id, housing_type, created_by', 'numerical', 'integerOnly' => true),
            array('housing_value, employer_rent, allowable_occupier', 'length', 'max' => 18),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, emp_id, housing_type, housing_value, employer_rent, allowable_occupier, created_by, date_created', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'housingType' => array(self::BELONGS_TO, 'EmployeeHousingtype', 'housing_type'),
            'emp' => array(self::BELONGS_TO, 'Users', 'emp_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'emp_id' => Lang::t('Emp'),
            'housing_type' => Lang::t('Housing Type'),
            'housing_value' => Lang::t('Housing Value'),
            'employer_rent' => Lang::t('Employer Rent'),
            'allowable_occupier' => Lang::t('Owner Occupier Interest'),
            'created_by' => Lang::t('Created By'),
            'date_created' => Lang::t('Date Created'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('employer_rent', self::SEARCH_FIELD, true, 'OR'),
            array('allowable_occupier', self::SEARCH_FIELD, true, 'OR'),
            'housing_value',
            'id',
            'emp_id',
            'housing_type',
        );
    }

    public function getShowcolumn($value) {
        switch ($value) {
            case 1:
                return false;
                break;
            case 2:
                return true;
                break;
        }
    }

}

