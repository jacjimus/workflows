<?php

/**
 * This is the model class for table "hr_phone_categories".
 *
 * The followings are the available columns in table 'hr_phone_categories':
 * @property integer $id
 * @property string $phone_cat_desc
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Empcontacts[] $empcontacts
 */
class PhoneCategories extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PhoneCategories the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_phone_categories';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('phone_cat_desc, date_created, created_by', 'required'),
            array('created_by', 'numerical', 'integerOnly' => true),
            array('phone_cat_desc', 'length', 'max' => 45),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, phone_cat_desc, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empcontacts' => array(self::HAS_MANY, 'Empcontacts', 'category_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'phone_cat_desc' => 'Phone Cat Desc',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search23() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('phone_cat_desc', $this->phone_cat_desc, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            array('phone_cat_desc', self::SEARCH_FIELD, true),
            'id',
        );
    }

}

