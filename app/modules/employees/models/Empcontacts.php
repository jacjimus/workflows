<?php

/**
 * This is the model class for table "hr_empcontacts".
 *
 * The followings are the available columns in table 'hr_empcontacts':
 * @property integer $id
 * @property integer $emp_id
 * @property string $phone
 * @property integer $category_id
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Employees $emp
 * @property PhoneCategories $category
 */
class Empcontacts extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Empcontacts the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_empcontacts';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id, phone, category_id', 'required'),
            array('emp_id, category_id, created_by', 'numerical', 'integerOnly' => true),
            array('phone', 'length', 'max' => 20),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, emp_id, phone, category_id, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'emp' => array(self::BELONGS_TO, 'Users', 'emp_id'),
            'category' => array(self::BELONGS_TO, 'PhoneCategories', 'category_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'emp_id' => 'Emp',
            'phone' => 'Phone',
            'category_id' => 'Category',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search22() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('emp_id', $this->emp_id);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            array('phone', self::SEARCH_FIELD, true),
            'id',
            'emp_id',
            'created_by',
            'category_id',
        );
    }

}

