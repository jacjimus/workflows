<?php

/**
 * This is the model class for table "hr_skills".
 *
 * The followings are the available columns in table 'hr_skills':
 * @property integer $id
 * @property string $skill
 * @property string $description
 * @property integer $enabled
 * @property integer $created_by
 * @property string $date_created
 *
 * The followings are the available model relations:
 * @property Empskills[] $empskills
 */
class Skills extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Skills the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_skills';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('skill, created_by, date_created', 'required'),
            array('enabled, created_by', 'numerical', 'integerOnly' => true),
            array('skill, description', 'length', 'max' => 120),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, skill, description, enabled, created_by, date_created', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empskills' => array(self::HAS_MANY, 'Empskills', 'skill_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'skill' => Lang::t('Skill'),
            'description' => Lang::t('Description'),
            'enabled' => Lang::t('Enabled'),
            'created_by' => Lang::t('Created By'),
            'date_created' => Lang::t('Date Created'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('skill', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'enabled',
            'created_by',
            'category_id',
        );
    }

}

