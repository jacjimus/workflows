<?php

/**
 * This is the model class for table "hr_employees".
 *
 * The followings are the available columns in table 'hr_employees':
 * @property integer $id
 * @property string $emp_code
 * @property string $hire_date
 * @property integer $job_title_id
 * @property integer $department_id
 * @property integer $manager_id
 * @property double $work_hours_perday
 * @property integer $employment_class_id
 * @property integer $employment_cat_id
 * @property integer $pay_type_id
 * @property integer $currency_id
 * @property string $salary
 * @property string $employment_status
 * @property string $email
 * @property string $mobile
 * @property string $company_phone
 * @property string $company_phone_ext
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property EmployementCategories $employmentCat
 * @property SettingsDepartment $department
 * @property JobsTitles $jobTitle
 * @property Paytypes $payType
 */
class Employees extends ActiveRecord implements IMyActiveSearch{

    /**
     * @return string the associated database table name
     */
    //birthdate variables
    public $hiredate_year;
    public $hiredate_month;
    public $hiredate_day;
    public $csv;

    const STATUS_ACTIVE = 'Active';
    const STATUS_TERMINATED = 'Terminated';
    const STATUS_SUSPENDED = 'Suspended';

    public function tableName() {
        return 'hr_employees';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email,employment_class_id', 'required'),
            array('branch_id, job_title_id, department_id', 'required', 'on' => ActiveRecord::SCENARIO_CREATE),
            array('job_title_id,paygroup_id, department_id, manager_id, employment_class_id, employment_cat_id, pay_type_id, currency_id, created_by', 'numerical', 'integerOnly' => true),
            array('work_hours_perday', 'numerical'),
            array('emp_code', 'length', 'max' => 30),
            array('salary', 'length', 'max' => 18),
            array('employment_status, company_phone_ext', 'length', 'max' => 10),
            array('email, mobile', 'length', 'max' => 25),
            array('company_phone,company_name', 'length', 'max' => 26),
            array('email,email_2', 'email'),
			array('postal_address,phy_address,phy_address2,division,supervises,accountabilities,csv,qualification,qualification_desc,title,institution,fullname','safe'),
            array('hire_date', 'date', 'format' => 'yyyy-M-d', 'message' => Lang::t('Please choose a valid birthdate')),
            ['csv', 'file', 'types' => 'csv',  'allowEmpty' => false, 'on' => 'csv'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            /*array('id, emp_code,paygroup_id, hire_date,hiredate_year,hiredate_month,hiredate_day, job_title_id, department_id, manager_id, work_hours_perday, employment_class_id, employment_cat_id, pay_type_id, currency_id, salary, employment_status, email, mobile, company_phone, company_phone_ext, date_created, created_by,qualification,qualification_desc,title,institution,fullname', 'safe', 'on' => 'search'),*/
             array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),

        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'employmentCat' => array(self::BELONGS_TO, 'EmployementCategories', 'employment_cat_id'),
            'department' => array(self::BELONGS_TO, 'OrgDepartment', 'department_id'),
            'jobTitle' => array(self::BELONGS_TO, 'JobsTitles', 'job_title_id'),
            //'payType' => array(self::BELONGS_TO, 'Paytypes', 'pay_type_id'),
            'empClass' => array(self::BELONGS_TO, 'Employmentclass', 'employment_class_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'id'),
            //'empBanks' => array(self::HAS_MANY, 'EmpBanks', 'emp_id'),
            //'empBanks' => array(self::HAS_MANY, 'ConAscJobAssignments', 'consultant_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => lang::t('ID'),
            'branch_id' => Lang::t('Branch'),
            'accountabilities' => Lang::t('Key Accountabilities'),
            'emp_code' => Lang::t('Emp Code'),
            'company_name' => Lang::t('Company Name'),
            'hire_date' => 'Hire Date',
            'job_title_id' => 'Job Title',
            'department_id' => 'Department',
            'postal_address' => 'Personal Postal Address',
            'phy_address' => 'Permanent Address',
            'supervises' => 'Supervises',
            'division' => 'Division',
            'phy_address2' => 'Residential Address',
            'manager_id' => 'Manager',
            'work_hours_perday' => 'Work Hours Perday',
            'employment_class_id' => 'Employment Class',
            'employment_cat_id' => 'Employment Cat',
            'pay_type_id' => 'Pay Type',
            'currency_id' => 'Currency',
            'salary' => 'Salary',
            'employment_status' => 'Employment Status',
            'email' => 'Email Address- No. 1 Office ',
            'email_2' => 'Email Address- No. 2 ',
            'mobile' => 'Mobile',
            'company_phone' => 'Personal Telephone No 1',
            'company_phone_ext' => 'Personal Telephone No 2',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'paygroup_id' => 'Salary Scale',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
  

    public function searchParams() {
        return array(
            array('qualification', self::SEARCH_FIELD, true, 'OR'),
            array('qualification_desc', self::SEARCH_FIELD, true, 'OR'),
            array('title', self::SEARCH_FIELD, true, 'OR'),
            array('institution', self::SEARCH_FIELD, true, 'OR'),
            array('fullname', self::SEARCH_FIELD, true, 'OR'),
            array('company_name', self::SEARCH_FIELD, true, 'OR'),
           
        );
    }

    /**
     *
     * @return type
     * returns the total number of job assignments an consultant has
     */
    public function getAssignments() {
        $val = ConAscJobAssignments::model()->count('`consultant_id`=:t1', array(':t1' => $this->id));
        return $val;
    }

    /**
     *
     * @return type
     * returns the total number of job specializations an consultant has
     */
    public function getSpecializations() {
        $val = ConAscJobAssignments::model()->count('`consultant_id`=:t1', array(':t1' => $this->id));
        return $val;
    }

    public function afterSave() {
        parent::afterSave();
        //run Leave Types procedure
        //Common::leaveproc();
        return true;
    }

    public function beforeValidate() {

        if (!empty($this->hiredate_day) || !empty($this->hiredate_month) || !empty($this->hiredate_year))
            $this->hire_date = $this->hiredate_year . "-" . $this->hiredate_month . "-" . $this->hiredate_day;

        return parent::beforeValidate();
    }

    public function beforeSave() {

        if ($this->isNewRecord)
            $this->employment_status = 'Active';
        return parent::beforeSave();
    }

    public function afterFind() {

        if (!empty($this->hire_date)) {
            $this->hiredate_day = Common::formatDate($this->hire_date, 'd');
            $this->hiredate_month = Common::formatDate($this->hire_date, 'm');
            $this->hiredate_year = Common::formatDate($this->hire_date, 'Y');
        }
        return parent::afterFind();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Employees the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Birth date year options
     * @return type
     */
    public static function hireDateYearOptions() {
        $min_year = 0;
        $max_year = 100;
        $current_year = date('Y');
        $years = array('' => Lang::t('Year'));

        for ($i = $min_year; $i <= $max_year; $i++) {
            $year = $current_year - $i;
            $years[$year] = $year;
        }

        return $years;
    }

    /**
     * Birth date month options
     * @return type
     */
    public static function hireDateMonthOptions() {
        return array(
            '' => Lang::t('Month'),
            1 => Lang::t('Jan'),
            2 => Lang::t('Feb'),
            3 => Lang::t('Mar'),
            4 => Lang::t('Apr'),
            5 => Lang::t('May'),
            6 => Lang::t('Jun'),
            7 => Lang::t('Jul'),
            8 => Lang::t('Aug'),
            9 => Lang::t('Sep'),
            10 => Lang::t('Oct'),
            11 => Lang::t('Nov'),
            12 => Lang::t('Dec'),
        );
    }

    /**
     * Birthdate day options
     * @return type
     */
    public static function hireDateDayOptions() {
        $days = array('' => Lang::t('Day'));
        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = $i;
        }
        return $days;
    }

    public function afterDelete() {
        Person::model()->deletePerson($this->id);
        return parent::afterDelete();
    }

    public function uploadCsv()
    {
        $success = 0;
        $failure = 0;
        $file = CUploadedFile::getInstance($this, 'csv');
        if ($file !== null) {
            $file_path = PUBLIC_DIR . DS . 'temp' . DS . $file->name;
            $file->saveAs($file_path);
            //parse the file
            $handle = fopen($file_path, "r");
            while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
                $newModel = clone $this;

                $company='ARMED FORCES SHOP';
                if(!isset($data[0])||$data[0]=='')
                    $newModel->company_name=$company;
                else{
                $newModel->company_name = $data[0];
                $company=$data[0];
                }
                if (isset($data[0]) && isset($data[1]) && isset($data[2]) && isset($data[6]) && isset($data[7])&& $data[0]!='surname') {
                    if (isset($data[1]))
                        $newModel->title = $data[1];
                    if (isset($data[2]))
                        $newModel->work_hours_perday = $data[2];
                    if (isset($data[3]))
                        $newModel->accountabilities = $data[3];
                    if (isset($data[4]))
                        $newModel->fullname = $data[4];
                    if (isset($data[5]))
                        $newModel->institution = $data[5];
                    if (isset($data[6]))
                        $newModel->qualification = $data[6];
                    if (isset($data[7])){
                        
                        $newModel->qualification_desc =  $data[7];
                    }
                    
                    if ($newModel->save(false)) {
                        $success++;
                    } else {
                        $failure++;
                    }
                } else {
                    $failure++;
                }
            }
            @unlink($file_path);
        }
        return [
            'success' => $success,
            'failure' => $failure,
        ];
    }


}
