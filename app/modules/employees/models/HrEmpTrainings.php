<?php

/**
 * This is the model class for table "hr_emp_trainings".
 *
 * The followings are the available columns in table 'hr_emp_trainings':
 * @property integer $id
 * @property integer $emp_id
 * @property string $institution
 * @property string $training
 * @property string $start_date
 * @property string $end_date
 * @property string $notes
 * @property string $date_created
 * @property integer $created_by
 */
class HrEmpTrainings extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hr_emp_trainings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, institution, training, start_date, end_date', 'required'),
			array('emp_id, created_by', 'numerical', 'integerOnly'=>true),
			array('institution, training', 'length', 'max'=>128),
			array('notes','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			 array('id,emp_id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'institution' => 'Institution',
			'training' => 'Training',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'notes' => 'Notes',
			'date_created' => 'Date Created',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	 public function searchParams() {
        return array(
            array('institution', self::SEARCH_FIELD, true, 'OR'),
            array('training', self::SEARCH_FIELD, true, 'OR'),
            array('start_date', self::SEARCH_FIELD, true, 'OR'),
            array('end_date', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'emp_id',
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HrEmpTrainings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
