<?php

/**
 * This is the model class for table "hr_jobs_titles".
 *
 * The followings are the available columns in table 'hr_jobs_titles':
 * @property integer $id
 * @property string $job_title
 * @property string $min_salary
 * @property string $max_salary
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property EmployeeJobDetails[] $employeeJobDetails
 * @property Employees[] $employees
 * @property Jobhistory[] $jobhistories
 */
class JobsTitles extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JobsTitles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hr_jobs_titles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('job_title, min_salary, max_salary, date_created, created_by', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('job_title', 'length', 'max'=>145),
			array('min_salary, max_salary', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, job_title, min_salary, max_salary, date_created, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employeeJobDetails' => array(self::HAS_MANY, 'EmployeeJobDetails', 'job_title'),
			'employees' => array(self::HAS_MANY, 'Employees', 'job_id'),
			'jobhistories' => array(self::HAS_MANY, 'Jobhistory', 'job_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'job_title' => 'Job Title',
			'min_salary' => 'Min Salary',
			'max_salary' => 'Max Salary',
			'date_created' => 'Date Created',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search23()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('job_title',$this->job_title,true);
		$criteria->compare('min_salary',$this->min_salary,true);
		$criteria->compare('max_salary',$this->max_salary,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
         public function getSearchParams() {
            return array(
               array('id','id',true),
                array('job_title','job_title',true),  
                array('min_salary','min_salary',true),
                array('max_salary','max_salary',true),
                array('date_created','date_created',true),
                array('created_by','created_by',true),
            );
        }
}