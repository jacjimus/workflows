<?php

/**
 * This is the model class for table "hr_dependants".
 *
 * The followings are the available columns in table 'hr_dependants':
 * @property integer $id
 * @property integer $emp_id
 * @property string $name
 * @property integer $relationship_id
 * @property string $email
 * @property string $mobile
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Employees $emp
 * @property Relations $relationship
 */
class Dependants extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Dependants the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_dependants';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id, name, relationship_id', 'required'),
            array('emp_id, relationship_id, created_by', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 145),
            array('email', 'length', 'max' => 85),
            array('mobile', 'length', 'max' => 65),
            array('dob', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id,date_created, created_by, emp_id, name, relationship_id, email, mobile, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'emp' => array(self::BELONGS_TO, 'Employees', 'emp_id'),
            'relationship' => array(self::BELONGS_TO, 'Relations', 'relationship_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'emp_id' => 'Emp',
            'name' => 'Name',
            'relationship_id' => 'Relationship',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search22() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('emp_id', $this->emp_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('relationship_id', $this->relationship_id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            array('name', self::SEARCH_FIELD, true, 'OR'),
            array('email', self::SEARCH_FIELD, true, 'OR'),
            array('mobile', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'emp_id',
            'created_by',
        );
    }

}

