<?php

/**
 * This is the model class for table "hr_qualificationlevels".
 *
 * The followings are the available columns in table 'hr_qualificationlevels':
 * @property integer $id
 * @property string $level_name
 * @property integer $enabled
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property EmpQualifications[] $empQualifications
 */
class Qualificationlevels extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Qualificationlevels the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_qualificationlevels';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('level_name, date_created, created_by', 'required'),
            array('enabled, created_by', 'numerical', 'integerOnly' => true),
            array('level_name', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, level_name, enabled, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SEARCH_SCENARIO),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empQualifications' => array(self::HAS_MANY, 'EmpQualifications', 'education_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'level_name' => Lang::t('Level Name'),
            'enabled' => Lang::t('Enabled'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('level_name', self::SEARCH_FIELD, true),
            'id',
            'enabled',
            'created_by',
            'date_created',
        );
    }

}

