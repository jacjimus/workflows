<?php

/**
 * This is the model class for table "employeeslist".
 *
 * The followings are the available columns in table 'employeeslist':
 * @property integer $id
 * @property string $empname
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $gender
 * @property string $birthdate
 * @property string $status
 * @property string $emp_code
 * @property string $hire_date
 * @property string $email
 * @property string $start_date
 * @property integer $job_title
 * @property string $categoryname
 * @property string $dept_name
 * @property string $pay_type_name
 * @property string $salary
 * @property string $company_phone_ext
 * @property string $company_phone
 * @property string $branch_id
 * @property string $mobile
 */
class Employeeslist extends ActiveRecord {

    public function primaryKey() {
        return 'id';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'employeeslist';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, first_name, last_name, job_title, categoryname, dept_name', 'required'),
            array('id, job_title,branch_id,paygroup_id,marital_status_id,currency_id', 'numerical', 'integerOnly' => true),
            array('empname', 'length', 'max' => 92),
            array('first_name, middle_name, last_name', 'length', 'max' => 30),
            array('gender', 'length', 'max' => 6),
            array('status', 'length', 'max' => 10),
            array('emp_code', 'length', 'max' => 15),
            array('email, categoryname, pay_type_name', 'length', 'max' => 255),
            array('dept_name', 'length', 'max' => 120),
            array('salary', 'length', 'max' => 18),
            array('company_phone_ext', 'length', 'max' => 8),
            array('company_phone, mobile', 'length', 'max' => 25),
            array('branch_id', 'length', 'max' => 11),
            array('birthdate, hire_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, empname,paygroup_id,employment_status,marital_status_id, first_name,currency_id, middle_name, last_name,branch_id, gender, birthdate, status, emp_code, hire_date, email, job_title, categoryname, dept_name, pay_type_name, salary, company_phone_ext, company_phone, branch_id, mobile', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'branch_id' => 'Branch',
            'empname' => 'Empname',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'gender' => 'Gender',
            'birthdate' => 'Birthdate',
            'status' => 'Status',
            'emp_code' => 'Emp Code',
            'hire_date' => 'Hire Date',
            'email' => 'Email',
            'job_title' => 'Job Title',
            'categoryname' => 'Categoryname',
            'dept_name' => 'Dept Name',
            'pay_type_name' => 'Pay Type Name',
            'salary' => 'Salary',
            'company_phone_ext' => 'Company Phone Ext',
            'company_phone' => 'Company Phone',
            'branch_id' => 'Branch',
            'mobile' => 'Mobile',
            'employment_status' => 'Employment Status',
            'marital_status_id' => 'Marital Status',
            'paygroup_id' => 'Salary Scale'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search45() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('empname', $this->empname, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('middle_name', $this->middle_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('gender', $this->gender, true);
        $criteria->compare('birthdate', $this->birthdate, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('emp_code', $this->emp_code, true);
        $criteria->compare('hire_date', $this->hire_date, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('job_title', $this->job_title);
        $criteria->compare('categoryname', $this->categoryname, true);
        $criteria->compare('dept_name', $this->dept_name, true);
        $criteria->compare('pay_type_name', $this->pay_type_name, true);
        $criteria->compare('salary', $this->salary, true);
        $criteria->compare('company_phone_ext', $this->company_phone_ext, true);
        $criteria->compare('company_phone', $this->company_phone, true);
        $criteria->compare('branch_id', $this->branch_id, true);
        $criteria->compare('mobile', $this->mobile, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            array('empname', self::SEARCH_FIELD, true, 'OR'),
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Employeeslist the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
