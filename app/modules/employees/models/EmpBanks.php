<?php

/**
 * This is the model class for table "hr_emp_banks".
 *
 * The followings are the available columns in table 'hr_emp_banks':
 * @property integer $id
 * @property integer $emp_id
 * @property string $account_no
 * @property integer $bank_id
 * @property integer $bank_branch_id
 * @property integer $paying_acc
 *
 * The followings are the available model relations:
 * @property BankBranches $bankBranch
 * @property Banks $bank
 */
class EmpBanks extends ActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EmpBanks the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hr_emp_banks';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id,account_name', 'required'),
            array('emp_id,bank_id, bank_branch_id, paying_acc', 'numerical', 'integerOnly' => true),
            array('account_no', 'length', 'max' => 20),
            array('account_name', 'length', 'max' => 128),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'bankBranch' => array(self::BELONGS_TO, 'BankBranches', 'bank_branch_id'),
            'bank' => array(self::BELONGS_TO, 'Banks', 'bank_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'emp_id' => Lang::t('Emp'),
            'account_no' => Lang::t('Account No'),
            'bank_id' => Lang::t('Bank'),
            'bank_branch_id' => Lang::t('Bank Branch'),
            'paying_acc' => Lang::t('Paying Acc'),
            'account_name' => Lang::t('Account Name'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchParams() {
        return array(
            array('account_no', self::SEARCH_FIELD, true, 'OR'),
            array('account_name', self::SEARCH_FIELD, true, 'OR'),
            'id',
            'emp_id',
            'bank_id',
            'bank_branch_id',
            'paying_acc',
        );
    }

}
