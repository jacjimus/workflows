<?php

/**
 * This is the model class for table "pyr_empstatutory".
 *
 * The followings are the available columns in table 'pyr_empstatutory':
 * @property integer $id
 * @property integer $emp_id
 * @property integer $paye
 * @property string $paye_amount
 * @property integer $nssf
 * @property string $nssf_amount
 * @property integer $nhif
 * @property string $nhif_amount
 *
 * The followings are the available model relations:
 * @property StatutoryActions $nhif0
 * @property StatutoryActions $nssf0
 * @property StatutoryActions $paye0
 */
class Empstatutory extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pyr_empstatutory';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('emp_id, paye, nssf, nhif', 'required'),
            array('emp_id, paye, nssf, nhif', 'numerical', 'integerOnly' => true),
            array('paye_amount, nssf_amount, nhif_amount', 'length', 'max' => 18),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, emp_id, paye, paye_amount, nssf, nssf_amount, nhif, nhif_amount', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'nhif0' => array(self::BELONGS_TO, 'StatutoryActions', 'nhif'),
            'nssf0' => array(self::BELONGS_TO, 'StatutoryActions', 'nssf'),
            'paye0' => array(self::BELONGS_TO, 'StatutoryActions', 'paye'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'emp_id' => Lang::t('Employee'),
            'paye' => Lang::t('P.A.Y.E'),
            'paye_amount' => Lang::t('Amount'),
            'nssf' => Lang::t('NSSF'),
            'nssf_amount' => Lang::t('NSSF Amount'),
            'nhif' => Lang::t('NHIF'),
            'nhif_amount' => Lang::t('NHIF Amount'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search67() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('emp_id', $this->emp_id);
        $criteria->compare('paye', $this->paye);
        $criteria->compare('paye_amount', $this->paye_amount, true);
        $criteria->compare('nssf', $this->nssf);
        $criteria->compare('nssf_amount', $this->nssf_amount, true);
        $criteria->compare('nhif', $this->nhif);
        $criteria->compare('nhif_amount', $this->nhif_amount, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'id',
            'emp_id',
            'paye',
            'paye_amount',
            'nssf',
            'nhif',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Empstatutory the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
