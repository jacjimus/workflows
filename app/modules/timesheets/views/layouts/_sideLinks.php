
<?php if (ModulesEnabled::model()->isModuleEnabled(TimesheetsModuleConstants::MOD_TIMESHEETS)): ?>
      <?php if ($this->showLink(TendersModuleConstants::RES_TENDERS)): ?>
 <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-briefcase"></i> <span class="menu-item-parent"><?php echo Lang::t('Weekly Planner') ?></span></a>
            <ul>
                <?php if ($this->showLink(TimesheetsModuleConstants::RES_TIMESHEETS_PLANNER)): ?>
                <li class="<?php echo $this->activeMenu === TimesheetsModuleConstants::MENU_TIMESHEET_PLANNER ? 'active' : '' ?>"> <a href="<?php echo Yii::app()->createUrl('timesheets/default/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('My tasks Planner') ?></span></a></li>
                <?php endif; ?>
                <?php if ($this->showLink(TimesheetsModuleConstants::RES_TIMESHEETS_REPORTS)): ?>
                <li class="<?php echo $this->activeMenu === TimesheetsModuleConstants::MENU_TIMESHEET_REPO? 'active' : '' ?>"> <a href="<?php echo Yii::app()->createUrl('timesheets/timesheets/index') ?>"><i class="fa fa-lg fa-fw fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Users Planners') ?></span></a></li>
                 <?php endif; ?>
            </ul>
        </li>

      <?php endif; ?>
<?php endif; ?>