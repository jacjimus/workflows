<div class="row">
    <div class="col-md-3">
        <p><a class="btn btn-default btn-sm btn-block show-event-form" href="javascript:void(0)" data-ajax-url="<?php echo $this->createUrl('create') ?>"><i class="fa fa-tasks"></i> <?php echo Lang::t('Create Task') ?></a></p>
        <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
            <p><a class="btn btn-default btn-sm btn-block" href="<?php echo $this->createUrl('eventType/index') ?>"><i class="fa fa-cog"></i> <?php echo Lang::t('Task Types') ?></a></p>
        <?php endif; ?>
        <p><a class="btn btn-default btn-sm btn-block" href="<?php echo Yii::app()->createUrl('notif/notifTypes/update', array('id' => EventType::NOTIF_EVENTS_REMINDER, UrlManager::GET_PARAM_RETURN_URL => Yii::app()->createUrl($this->route, $this->actionParams))) ?>"><i class="fa fa-bell"></i> <?php echo Lang::t('Notification Settings') ?></a></p>
        <div class="well well-sm" id="event-container">
            <form>
                <fieldset>
                    <ul id="external-events" class="list-unstyled">
                        <?php foreach (Tasks::model()->getEvents() as $r): ?>
                            <li>
                                <span class="<?php echo $r['color_class'] ?> draggable-event" data-description="<?php echo CHtml::encode($r['description']) ?>" data-event-id="<?php echo $r['id'] ?>" data-ajax-url="<?php echo $this->createUrl('addOccurrence'); ?>">
                                    <?php echo CHtml::encode($r['name']) ?>
                                </span>
                                <p class="manage-event hidden">
                                    <a class="text-success show-event-form" href="javascript:void(0)" data-ajax-url="<?php echo $this->createUrl('update', array('id' => $r['id'])) ?>"><?php echo Lang::t('Edit') ?> <i class="fa fa-edit"></i></a>
                                    <a class="text-danger delete-event" href="javascript:void(0)" data-ajax-url="<?php echo $this->createUrl('delete', array('id' => $r['id'])) ?>" data-confirm-message="<?php echo Lang::t('DELETE_CONFIRM') ?>"><?php echo Lang::t('Delete') ?> <i class="fa fa-trash-o"></i></a>
                                </p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </fieldset>
            </form>
        </div>
        <div id="event-form-container">
            <p class="text-center hidden" id="event-form-loading"><i class="fa fa-spinner fa-spin fa-5x text-warning"></i></p>
        </div>
    </div>
    <div class="col-md-9">
        <!-- new widget -->
        <div class="jarviswidget jarviswidget-color-blueDark">

            <!-- widget options:
            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

            data-widget-colorbutton="false"
            data-widget-editbutton="false"
            data-widget-togglebutton="false"
            data-widget-deletebutton="false"
            data-widget-fullscreenbutton="false"
            data-widget-custombutton="false"
            data-widget-collapsed="true"
            data-widget-sortable="false"

            -->
            <header>
                <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>
                <h2> My Timesheets</h2>
                <div class="widget-toolbar">
                    <!-- add: non-hidden - to disable auto hide -->
                    <div class="btn-group">
                        <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                            Showing <i class="fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu js-status-update pull-right">
                            <li>
                                <a href="javascript:void(0);" id="mt">Month</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" id="wk">Weekly</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" id="td">Today</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>

            <!-- widget div-->
            <div>

                <div class="widget-body no-padding">
                    <!-- content goes here -->
                    <div class="widget-body-toolbar">

                        <div id="calendar-buttons">

                            <div class="btn-group">
                                <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="calendar"></div>
                    <!-- end content -->
                </div>
            </div>
            <!-- end widget div -->
        </div>
        <!-- end widget -->
    </div>
</div>
<?php
$events = CJSON::encode(EventOccurrence::model()->getCalendarEvents());
Yii::app()->clientScript
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/fullcalendar/jquery.fullcalendar.min.js', CClientScript::POS_END)
        ->registerScript('event.default.index', "EventModule.Event.init(" . $events . ");")
?>