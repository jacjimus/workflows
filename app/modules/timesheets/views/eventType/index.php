<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-md-3">
        <?php $this->renderPartial('event.views.layouts._tab') ?>
    </div>
    <div class="col-md-9">
        <?php $this->renderPartial('_grid', array('model' => $model)); ?>
    </div>
</div>