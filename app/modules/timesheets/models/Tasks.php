<?php

/**
 * This is the model class for table "tasks".
 *
 * The followings are the available columns in table 'tasks':
 * @property string $id
 * @property string $project_id
 * @property string $name
 * @property string $description
 * @property string $initial_start_date
 * @property string $initial_end_date
 * @property string $repeated
 * @property string $user_id
 * @property integer $is_active
 * @property integer $location_id
 * @property string $color_class
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property EventType $eventType
 * @property EventOccurrence[] $eventOccurrences
 */
class Tasks extends ActiveRecord implements IMyActiveSearch, IMyNotifManager {

      const REPEATED_NONE = 'None';
      const REPEATED_DAILY = 'Daily';
      const REPEATED_WEEKLY = 'Weekly';
      const REPEATED_MONTHLY = 'Monthly';
      const REPEATED_YEARLY = 'Yearly';

      /**
       *
       * @var type
       */
      public $current_initial_start_date;

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'tasks';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('project_id, name, initial_start_date', 'required'),
                array('user_id, created_by', 'length', 'max' => 11),
                array('name,description', 'length', 'max' => 128),
                array('repeated', 'length', 'max' => 7),
                array('initial_end_date,is_active,location_id,color_class', 'safe'),
                array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
                'project' => array(self::BELONGS_TO, 'Projects', 'project_id'),
                'eventOccurrences' => array(self::HAS_MANY, 'EventOccurrence', 'event_id'),
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'project_id' => Lang::t('Project'),
                'name' => Lang::t('Title'),
                'initial_start_date' => Lang::t('Initial Start Date'),
                'initial_end_date' => Lang::t('Initial End Date'),
                'repeated' => Lang::t('Repeated'),
                'user_id' => Lang::t('User'),
                'date_created' => Lang::t('Date Created'),
                'created_by' => Lang::t('Created By'),
                'is_active' => Lang::t('Active'),
                'location_id' => Lang::t('Location'),
                'color_class' => Lang::t('Select Event Color'),
                'description' => Lang::t('Short description'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return Event the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      public function afterSave() {
            if ($this->current_initial_start_date !== $this->initial_start_date) {
                  EventOccurrence::model()->addEventOccurrence($this->id, $this->initial_start_date, $this->initial_end_date);
            }
            return parent::afterSave();
      }

      public function afterFind() {
            $this->current_initial_start_date = $this->initial_start_date;
            return parent::afterFind();
      }

      public function searchParams() {
            return array(
                array('name', self::SEARCH_FIELD, true),
                'event_type_id',
                'repeated',
                'user_id',
                'is_active',
                'location_id',
            );
      }

      public static function eventColorOptions() {

            return array(
                'bg-color-darken' => 'bg-color-darken txt-color-white',
                'bg-color-blue' => 'bg-color-blue txt-color-white',
                'bg-color-orange' => 'bg-color-orange txt-color-white',
                'bg-color-greenLight' => 'bg-color-greenLight txt-color-white',
                'bg-color-blueLight' => 'bg-color-blueLight txt-color-white',
                'bg-color-red' => 'bg-color-red txt-color-white',
            );
      }

      public static function repeatedOptions() {
            return array(
                self::REPEATED_NONE => Lang::t(self::REPEATED_NONE),
                self::REPEATED_DAILY => Lang::t(self::REPEATED_DAILY),
                self::REPEATED_WEEKLY => Lang::t(self::REPEATED_WEEKLY),
                self::REPEATED_MONTHLY => Lang::t(self::REPEATED_MONTHLY),
                self::REPEATED_YEARLY => Lang::t(self::REPEATED_YEARLY),
            );
      }

      /**
       * Get events
       * @return type
       */
      public function getEvents() {
            $conditions = '(`user_id` IS NULL OR `user_id`=:user_id) ';
            $params = array(':user_id' => Yii::app()->user->id);
            return $this->getData('*', $conditions, $params);
      }

      public function processNotifEmailTemplate($email_template, $item_id) {
            //placeholders supported: {{event_name}},{{date}}
            $row = EventOccurrence::model()->getRow('event_id,date_from', '`id`=:id', array(':id' => $item_id));
            if (!empty($row)) {
                  $event_name = $this->get($row['event_id'], 'name');
                  return Common::myStringReplace($email_template, array(
                              '{{event_name}}' => CHtml::link(CHtml::encode($event_name), Yii::app()->createAbsoluteUrl('event/default/index')),
                              '{{date}}' => MyYiiUtils::formatDate($row['date_from'], 'M j, Y'),
                  ));
            }
            return $email_template;
      }

      public function processNotifTemplate($template, $item_id) {
            //placeholders supported: {{event_name}},{{date}}
            $row = EventOccurrence::model()->getRow('event_id,date_from', '`id`=:id', array(':id' => $item_id));
            if (!empty($row)) {
                  $event_name = $this->get($row['event_id'], 'name');
                  return Common::myStringReplace($template, array(
                              '{{event_name}}' => CHtml::link(CHtml::encode($event_name), Yii::app()->createUrl('event/default/index')),
                              '{{date}}' => MyYiiUtils::formatDate($row['date_from'], 'M j, Y'),
                  ));
            }
            return $template;
      }

      public function updateNotification() {
            if (ModulesEnabled::model()->isModuleEnabled(EventModuleConstants::MOD_EVENTS)) {
                  //vehicle servicing notification
                  $notification_date = Notif::model()->getNotificationDate(EventType::NOTIF_EVENTS_REMINDER);
                  $data = EventOccurrence::model()->getData('id,event_id,date_from', 'DATE(`date_from`)=DATE(:t1) AND `notified`=:t2', array(':t1' => $notification_date, ':t2' => 0));
                  if (!empty($data)) {
                        foreach ($data as $row) {
                              Notif::model()->pushNotif(EventType::NOTIF_EVENTS_REMINDER, $row['id']);
                              //mark as notified
                              EventOccurrence::model()->markAsNotified($row['id']);
                              //create the next occurrence
                              EventOccurrence::model()->createNextOccurrence($row['event_id'], $row['date_from']);
                        }
                  }
            }
      }

}
