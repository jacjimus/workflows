<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class TimesheetsModuleConstants {

      const MOD_TIMESHEETS = 'timesheets';
      //resource constants
      const RES_TIMESHEETS = 'TIMESHEETS';
      const RES_TIMESHEETS_PLANNER = 'TIMESHEETS_PLANNER';
      const RES_TIMESHEETS_REPORTS = 'TIMESHEETS_REPORTS';
      //menu constants
      const MENU_TIMESHEET_REPO = 'MENU_TIMESHEET_REPO';
      const MENU_TIMESHEET_PLANNER = 'MENU_TIMESHEET_PLANNER';
      const TAB_EVENT = 'TAB_EVENT';
      const TAB_SETTINGS = 'TAB_SETTINGS';

}
