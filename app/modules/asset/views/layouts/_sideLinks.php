<?php if (ModulesEnabled::model()->isModuleEnabled(AssetModuleConstants::MOD_ASSETS_REGISTER)): ?>
      <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-tag"></i> <span class="menu-item-parent"><?php echo Lang::t('Assets Register') ?></span></a>
            <ul>
                  <?php if ($this->showLink(HrModuleConstants::RES_HR_LEAVES)): ?>
                        <li class="<?php echo $this->activeMenu === AssetModuleController::MENU_ASSETS ? 'active' : '' ?>"><a href="#"><?php echo Lang::t('Assets') ?></a></li>
                        <li class="<?php echo $this->activeMenu === AssetModuleController::MENU_ASSET_TYPES ? 'active' : '' ?>"><a href="#"><?php echo Lang::t('Asset types') ?></a></li>
                  <?php endif; ?>
            </ul>
      </li>
<?php endif; ?>