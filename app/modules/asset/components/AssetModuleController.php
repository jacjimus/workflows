<?php

/**
 * Parent controller  of Asset Module
 *
 * @author Fred <mconyango@gmail.com>
 */
class AssetModuleController extends Controller {

        const MENU_ASSETS = 'ASSETS';
        const MENU_ASSET_TYPES = 'ASSET_TYPES';

        public function init() {
                parent::init();
        }

}
