<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class UsersModuleConstants {

        //resources constants
        const RES_USER_ROLES = 'USER_ROLES';
        const RES_USERS = 'USERS';
        const RES_USER_RESOURCES = 'USER_RESOURCES';
        const RES_USER_LEVELS = 'USER_LEVELS';
        const RES_USER_ACTIVITY = 'USER_ACTIVITY';
        //menu and tabs constants
        const MENU_USERS = 'MENU_USERS';
        const TAB_USERS = 'TAB_USERS';
        const TAB_ROLES = 'TAB_ROLES';
        const TAB_USER_LEVELS = 'TAB_USER_LEVELS';
        const TAB_RESOURCES = 'TAB_RESOURCES';
        //miscelleneous
        const LOG_ACTIVITY = 'activity_log';

}
