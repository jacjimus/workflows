<?php

/**
 * This is the model class for table "person_signature".
 *
 * The followings are the available columns in table 'person_signature':
 * @property integer $id
 * @property integer $person_id
 * @property string $file_name
 * @property string $file_type
 * @property string $file_size
 * @property string $file_content
 * @property string $date_created
 * @property integer $created_by
 */
class PersonSignature extends ActiveRecord {

    public $uploadedFile;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'person_signature';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        return array(
            array('person_id', 'required'),
            array('uploadedFile', 'file', 'types' => 'jpg, gif, png'),
            array('uploadedFile', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * Saves the name, size, type and data of the uploaded file
     */
    public function beforeSave() {
        $file = CUploadedFile::getInstance($this, 'uploadedFile');
        if (!empty($file)) {
            $this->file_name = $file->name;
            $this->file_type = $file->type;
            $this->file_size = $file->size;
            $this->file_content = base64_encode(file_get_contents($file->tempName));
        }

        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'person_id' => 'Person',
            'file_name' => 'File Name',
            'file_type' => 'File Type',
            'file_size' => 'File Size',
            'file_content' => 'File Content',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PersonSignature the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function showsignature() {

        if ($this->file_content != '') {
            return"data:" . $this->file_type . ";base64," . $this->file_content;
        } else {
            $url = Yii::app()->baseUrl . "/img/noimage.jpg";
            return CHtml::image($url, 'No Image');
        }
    }

}
