<?php

/**
 * This is the model class for table "person_department".
 *
 * The followings are the available columns in table 'person_department':
 * @property string $id
 * @property string $person_id
 * @property integer $dept_id
 * @property integer $has_left
 * @property string $reason_for_leaving
 * @property string $date_left
 * @property string $date_created
 * @property string $created_by
 * @property string $last_modified
 * @property string $last_modified_by
 *
 * The followings are the available model relations:
 * @property HrDepartments $dept
 * @property Person $person
 */
class PersonDepartment extends ActiveRecord {

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'person_department';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                    array('person_id, dept_id', 'required'),
                    array('dept_id, has_left', 'numerical', 'integerOnly' => true),
                    array('person_id, created_by, last_modified_by', 'length', 'max' => 11),
                    array('reason_for_leaving', 'length', 'max' => 255),
                    array('date_left, last_modified', 'safe'),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                    'dept' => array(self::BELONGS_TO, 'HrDepartments', 'dept_id'),
                    'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => 'ID',
                    'person_id' => 'Person',
                    'dept_id' => 'Dept',
                    'has_left' => 'Has Left',
                    'reason_for_leaving' => 'Reason For Leaving',
                    'date_left' => 'Date Left',
                    'date_created' => 'Date Created',
                    'created_by' => 'Created By',
                    'last_modified' => 'Last Modified',
                    'last_modified_by' => 'Last Modified By',
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return PersonDepartment the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * Add person to a department
         * @param array $columns
         * @return type
         */
        public function addPersonToDept(array $columns)
        {
                return Yii::app()->db->createCommand()
                                ->insert($this->tableName(), $columns);
        }

        /**
         * Remove a person from a department
         * @param type $person_id
         * @return type
         */
        public function removePersonFromDept($person_id)
        {
                $now = new CDbExpression('NOW()');
                $creator = Yii::app() instanceof CWebApplication ? Yii::app()->user->id : NULL;
                return Yii::app()->db->createCommand()
                                ->update($this->tableName(), array('has_left' => 1, 'date_left' => $now, 'last_modified' => $now, 'last_modified_by' => $creator), '`person_id`=:t1 AND `has_left`=:t2', array(':t1' => $person_id, ':t2' => 0));
        }

        /**
         * Get department id of a person
         * @param type $person_id
         * @return type
         */
        public function getDeptId($person_id)
        {
                if (empty($person_id))
                        return NULL;
                $dept_id = $this->getScalar('dept_id', '`person_id`=:t1 AND `has_left`=:t2', array(':t1' => $person_id, ':t2' => 0));
                return !empty($dept_id) ? $dept_id : NULL;
        }

        /**
         * Update person department
         * @param type $person_id
         * @param type $dept_id
         */
        public function updatePersonDept($person_id, $dept_id = NULL)
        {
                if (!empty($dept_id)) {
                        if (!$this->exists('`person_id`=:t1 AND `dept_id`=:t2 AND `has_left`=:t3', array(':t1' => $person_id, ':t2' => $dept_id, ':t3' => 0))) {
                                $this->removePersonFromDept($person_id);
                                $this->addPersonToDept(array(
                                    'person_id' => $person_id,
                                    'dept_id' => $dept_id,
                                    'date_created' => new CDbExpression('NOW()'),
                                    'created_by' => Yii::app() instanceof CWebApplication ? Yii::app()->user->id : NULL,
                                ));
                        }
                } else {
                        $this->removePersonFromDept($person_id);
                }
        }

}
