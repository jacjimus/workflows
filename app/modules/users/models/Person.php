<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property string $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $gender
 * @property string $birthdate
 * @property integer $birthdate_estimated
 * @property string $date_created
 * @property string $created_by
 * @property string $last_modified
 * @property integer $marital_status_id
 *
 * The followings are the available model relations:
 * @property PersonAddress[] $personAddresses
 */
class Person extends ActiveRecord {

    //gender constants
    const GENDER_MALE = 'Male';
    const GENDER_FEMALE = 'Female';
    //base dir name where images and files are stores
    const BASE_DIR = 'profile';

    /**
     * Holds the temp file for profile image if file upload via ajax.
     * @var type
     */
    public $temp_profile_image;

    /**
     * Full name of the person
     * @var type
     */
    public $name;
    //birthdate variables
    public $birthdate_year;
    public $birthdate_month;
    public $birthdate_day;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'person';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('birthdate_estimated,marital_status_id', 'numerical', 'integerOnly' => true),
            array('first_name,nationality, middle_name, last_name', 'length', 'max' => 30),
            array('gender', 'length', 'max' => 6),
            array('first_name, last_name,id_no,pin_no,spouse,next_of_kin_email,place_of_birth,next_of_kin_name,next_of_kin_phone,full_names', 'safe'),
            array('created_by', 'length', 'max' => 10),
            array('birthdate', 'date', 'format' => 'yyyy-M-d', 'message' => Lang::t('Please choose a valid birthdate')),
            array('birthdate,marital_status_id, id_no,pin_no,last_modified,temp_profile_image,birthdate_year,birthdate_month,birthdate_day', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'users' => array(self::HAS_MANY, 'Users', 'id'),
            'employees' => array(self::HAS_MANY, 'Employees', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'full_names'=>Lang::t('Full Name'),
            'first_name' => Lang::t('First Name'),
            'spouse' => Lang::t('Name of Spouse'),
            'nationality' => Lang::t('Nationality'),
            'middle_name' => Lang::t('Middle Name'),
            'last_name' => Lang::t('Last Name'),
            'gender' => Lang::t('Gender'),
            'birthdate' => Lang::t('Date of Birth'),
            'birthdate_estimated' => Lang::t('Birthdate Estimated'),
            'temp_profile_image' => Lang::t('Profile Image'),
            'date_created' => Lang::t('Date Created'),
            'marital_status_id' => Lang::t('Marital Status'),
            'id_no' => 'ID No',
            'pin_no' => 'PIN Number'
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Person the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {
        if (!empty($this->temp_profile_image))
            PersonImages::model()->updateProfileImage($this->id, $this->temp_profile_image);
        return parent::afterSave();
    }

    /**
     * Get the dir of a user
     * @param string $person_id
     */
    public function getDir($person_id) {
        return Common::createDir($this->getBaseDir() . DS . $person_id);
    }

    public function getBaseDir() {
        return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
    }

    public function afterFind() {
        $this->name = $this->first_name . " " . $this->last_name;
        if (!empty($this->birthdate)) {
            $this->birthdate_day = (int) Common::formatDate($this->birthdate, 'd');
            $this->birthdate_month = (int) Common::formatDate($this->birthdate, 'm');
            $this->birthdate_year = (int) Common::formatDate($this->birthdate, 'Y');
        }
        return parent::afterFind();
    }

    public function beforeValidate() {
        if (!empty($this->birthdate_day) || !empty($this->birthdate_month) || !empty($this->birthdate_year))
            $this->birthdate = $this->birthdate_year . "-" . $this->birthdate_month . "-" . $this->birthdate_day;
        return parent::beforeValidate();
    }

    /**
     * Get gender options
     * @return array Gender Options
     */
    public static function genderOptions() {
        return array(
            self::GENDER_MALE => Lang::t(self::GENDER_MALE),
            self::GENDER_FEMALE => Lang::t(self::GENDER_FEMALE),
        );
    }

    /**
     * Birth date year options
     * @return type
     */
    public static function birthDateYearOptions() {
        $min_year = 10;
        $max_year = 100;
        $current_year = date('Y');
        $years = array('' => Lang::t('Year'));

        for ($i = $min_year; $i <= $max_year; $i++) {
            $year = $current_year - $i;
            $years[$year] = $year;
        }

        return $years;
    }

    /**
     * Birth date month options
     * @return type
     */
    public static function birthDateMonthOptions() {
        return array(
            '' => Lang::t('Month'),
            1 => Lang::t('Jan'),
            2 => Lang::t('Feb'),
            3 => Lang::t('Mar'),
            4 => Lang::t('Apr'),
            5 => Lang::t('May'),
            6 => Lang::t('Jun'),
            7 => Lang::t('Jul'),
            8 => Lang::t('Aug'),
            9 => Lang::t('Sep'),
            10 => Lang::t('Oct'),
            11 => Lang::t('Nov'),
            12 => Lang::t('Dec'),
        );
    }

    /**
     * Birthdate day options
     * @return type
     */
    public static function birthDateDayOptions() {
        $days = array('' => Lang::t('Day'));
        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = $i;
        }
        return $days;
    }

    /**
     *
     * @param type $id
     * @return Person $person
     */
    public function loadModel($id) {
        return parent::loadModel($id);
    }

    /**
     * Delete person
     * @param type $id
     * @return type
     */
    public function deletePerson($id) {
        if (Person::model()->canDelete($id)) {
            Yii::app()->db->createCommand()
                    ->delete($this->tableName(), '`id`=:id', array(':id' => $id));
            $dir = $this->getBaseDir() . DS . $id;
            if (is_dir($dir))
                Common::deleteDir($dir);
        }
    }

}
