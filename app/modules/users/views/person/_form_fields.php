<?php
if (!isset($label_size)):
    $label_size = 2;
endif;
if (!isset($input_size)):
    $input_size = 8;
endif;
$label_class = "col-md-{$label_size} control-label";
$input_class = "col-md-{$input_size}";
$half_input_size = $input_size / 2;
$half_input_class = "col-md-{$half_input_size}";
?>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'full_names', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>" style="padding-top: 4px;">
        <?php echo CHtml::activeTextField($model, 'full_names', array('class' => 'form-control', 'maxlength' => 30,)); ?>
    </div>
</div>


<!--<div class="form-group">
    <label class="<?php echo $label_class ?>"><?php echo Lang::t('Name') ?><span class="required">*</span></label>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'first_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('first_name'))); ?>
        <?php echo CHtml::error($model, 'first_name') ?>
    </div>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'middle_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('middle_name'))); ?>
        <?php echo CHtml::error($model, 'middle_name') ?>
    </div>
    <div class="<?php echo $half_input_class ?>">
        <?php echo CHtml::activeTextField($model, 'last_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('last_name'))); ?>
        <?php echo CHtml::error($model, 'last_name') ?>
    </div>
	
</div>-->
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'gender', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>" style="padding-top: 4px;">
        <?php echo CHtml::activeRadioButtonList($model, 'gender', Person::genderOptions(), array('separator' => '&nbsp;&nbsp;&nbsp;&nbsp;')); ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'id_no', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>" style="padding-top: 4px;">
        <?php echo CHtml::activeTextField($model, 'id_no', array('class' => 'form-control', 'maxlength' => 30,)); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'pin_no', array('class' => $label_class)); ?>
    <div class="<?php echo $input_class ?>" style="padding-top: 4px;">
        <?php echo CHtml::activeTextField($model, 'pin_no', array('class' => 'form-control', 'maxlength' => 30,)); ?>
    </div>
</div>

<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'nationality', array('class' => $label_class)); ?>
    <div class="col-md-2"> <?php echo CHtml::activeTextField($model, 'nationality', array('class' => 'form-control', 'maxlength' => 30)); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'place_of_birth', array('class' => $label_class)); ?>
    <div class="col-md-2"> <?php echo CHtml::activeTextField($model, 'place_of_birth', array('class' => 'form-control', 'maxlength' => 30)); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'next_of_kin_name', array('class' => $label_class)); ?>
    <div class="col-md-2"> <?php echo CHtml::activeTextField($model, 'next_of_kin_name', array('class' => 'form-control', 'maxlength' => 30)); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'next_of_kin_phone', array('class' => $label_class)); ?>
    <div class="col-md-2"> <?php echo CHtml::activeTextField($model, 'next_of_kin_phone', array('class' => 'form-control', 'maxlength' => 30)); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'next_of_kin_email', array('class' => $label_class)); ?>
    <div class="col-md-2"> <?php echo CHtml::activeTextField($model, 'next_of_kin_email', array('class' => 'form-control', 'maxlength' => 30)); ?>
    </div>
</div>




