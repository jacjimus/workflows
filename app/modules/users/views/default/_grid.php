<?php

$grid_id = 'users-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-group"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => false, 'url' => $this->createUrl('/employees/employees/create', array()), 'label' => 'Add User'),
    //'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . Users::STATUS_BLOCKED . '"?"bg-danger":""',
        'columns' => array(
            'id',
            array(
                'name' => 'username',
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::encode($data->username),Yii::app()->controller->createUrl("view",array("id"=>$data->id)))',
            ),
            array(
                'name' => 'name',
            ),
            array(
                'name' => 'email',
            ),
            array(
                'name' => 'date_created',
                'value' => 'MyYiiUtils::formatDate($data->date_created)',
            ),
            array(
                'name' => 'status',
                'type' => 'raw',
                'value' => 'CHtml::tag("span", array("class"=>$data->status==="' . Users::STATUS_ACTIVE . '"?"label label-success":"label label-danger"), $data->status)',
            ),
            array(
                'name' => 'user_level',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'title' => Lang::t('View details'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                        'visible' => '$data->checkPrivilege($this->grid->owner,"' . Acl::ACTION_UPDATE . '")',
                        'options' => array(
                            'title' => Lang::t('Edit'),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                        'visible' => '$this->grid->owner->showLink("' . UsersModuleConstants::RES_USERS . '", "' . Acl::ACTION_DELETE . '") && $data->canDelete()?true:false',
                        'options' => array(
                            'class' => 'delete',
                            'title' => Lang::t('Delete'),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>