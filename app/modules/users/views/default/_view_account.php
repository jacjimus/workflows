<div  class="jarviswidget jarviswidget-color-white" id="wid-account-details" data-widget-editbutton="false"  data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
    <header>
        <h2> <?php echo Lang::t('Account details') ?> </h2>
    </header>
    <!-- widget div-->
    <div>
        <!-- widget content -->
        <div class="widget-body no-padding">
            <?php
            $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array(
                        'name' => 'id',
                    ),
                    array(
                        'name' => 'status',
                        'value' => CHtml::tag('span', array('class' => $model->status === Users::STATUS_ACTIVE ? 'label label-success' : 'label label-danger'), $model->status),
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'username',
                    ),
                    array(
                        'name' => 'email',
                    ),
                    array(
                        'name' => 'user_level',
                        'visible' => Yii::app()->user->user_level == UserLevels::LEVEL_ADMIN || Yii::app()->user->user_level == UserLevels::LEVEL_ENGINEER,
                    ),
                    array(
                        'name' => 'role_id',
                        'visible' => !empty($model->role_id),
                        'value' => UserRoles::model()->get($model->role_id, 'name'),
                    ),
                    array(
                        'name' => 'timezone',
                    ),
                    array(
                        'name' => 'date_created',
                        'value' => MyYiiUtils::formatDate($model->date_created),
                    ),
                    array(
                        'name' => 'created_by',
                        'value' => Users::model()->get($model->created_by, "username"),
                        'visible' => !empty($model->created_by),
                    ),
                    array(
                        'name' => 'last_login',
                        'value' => MyYiiUtils::formatDate($model->last_login),
                    )
                ),
            ));
            ?>
        </div>
        <!-- end widget content -->
    </div>
    <!-- end widget div -->
</div>