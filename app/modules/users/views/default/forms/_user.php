<?php
if (!$model->getIsNewRecord()) {
        $can_update = Users::model()->checkPrivilege($this, Acl::ACTION_UPDATE, false, $model->user_level) && !Users::isMyAccount($model->id);
}
?>
<?php echo CHtml::errorSummary($model, '') ?>
<?php if ($model->getIsNewRecord() || $can_update) : ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'user_level', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                        <?php echo CHtml::activeDropDownList($model, 'user_level', Users::model()->userLevelOptions($this), array('class' => 'form-control', 'data-show-role' => UserLevels::LEVEL_ADMIN)); ?>
                </div>
        </div>
        <div class="form-group" id="Users_role_wrapper">
                <?php echo CHtml::activeLabelEx($model, 'role_id', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                        <?php echo CHtml::activeDropDownList($model, 'role_id', UserRoles::model()->getListData('id', 'name'), array('class' => 'form-control')); ?>
                </div>
        </div>
<?php endif; ?>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'email', array('class' => 'col-md-2 control-label')); ?>
        <div class="col-md-4">
                <?php echo CHtml::activeEmailField($model, 'email', array('class' => 'form-control', 'maxlength' => 128, 'type' => 'email')); ?>
        </div>
</div>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'username', array('class' => 'col-md-2 control-label')); ?>
        <div class="col-md-4">
                <?php echo CHtml::activeTextField($model, 'username', array('class' => 'form-control', 'maxlength' => 30)); ?>
        </div>
</div>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'password', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                        <?php echo CHtml::activePasswordField($model, 'password', array('class' => 'form-control')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'confirm', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                        <?php echo CHtml::activePasswordField($model, 'confirm', array('class' => 'form-control')); ?>
                </div>
        </div>
<?php endif ?>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'timezone', array('class' => 'col-md-2 control-label')); ?>
        <div class="col-md-4">
                <?php echo CHtml::activeDropDownList($model, 'timezone', SettingsTimezone::model()->getListData('name', 'name'), array('class' => 'form-control')); ?>
        </div>
</div>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <div class="col-md-offset-2 col-md-4">
                        <label class="checkbox"><?php echo CHtml::activeCheckBox($model, 'send_email'); ?> <?php echo Lang::t('Email the login details to the user.') ?></label>
                </div>
        </div>
<?php endif; ?>
