<?php echo CHtml::errorSummary($model, '') ?>
<div class="form-group">
        <label class="col-md-2 control-label"><?php echo Lang::t('Phone') ?></label>
        <div class="col-md-2">
                <?php echo CHtml::activeTextField($model, 'phone1', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('phone1'))); ?>
        </div>
        <div class="col-md-2">
                <?php echo CHtml::activeTextField($model, 'phone2', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('phone2'))); ?>
        </div>
</div>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'address', array('class' => 'col-md-2 control-label')); ?>
        <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'address', array('class' => 'form-control', 'rows' => 3)); ?>
        </div>
</div>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'residence', array('class' => 'col-md-2 control-label')); ?>
        <div class="col-md-4">
                <?php echo CHtml::activeTextArea($model, 'residence', array('class' => 'form-control', 'rows' => 3)); ?>
        </div>
</div>

