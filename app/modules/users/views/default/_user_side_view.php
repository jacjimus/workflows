<?php
$can_update = $model->checkPrivilege($this, Acl::ACTION_UPDATE);
$can_view_activity = $this->showLink(UsersModuleConstants::RES_USER_ACTIVITY);
$address = PersonAddress::model()->find('`person_id`=:t1', array(':t1' => $model->id));
?>
<?php echo CHtml::image(PersonImages::model()->getProfileImageUrl($model->id, 256, 256), CHtml::encode($model->name), array('id' => 'avator', 'class' => 'editable img-responsive thumbnail', 'style' => 'width:100%;')); ?>
<div class="text-center">
    <p><i class="fa fa-envelope-o"></i> <?php echo CHtml::encode($model->email); ?></p>
    <?php if (NULL !== $address && (!empty($address->phone1) || !empty($address->phone2))): ?>
        <p><i class="fa fa-phone"></i> <?php echo CHtml::encode($address->phone1) ?><?php echo (!empty($address->phone1) && !empty($address->phone2)) ? '/' : '' ?><?php echo CHtml::encode($address->phone2) ?></p>
    <?php endif; ?>
</div>
<div class="list-group">
    <?php if (Users::isMyAccount($model->id)): ?>
        <a class="list-group-item" href="<?php echo $this->createUrl('changePassword', array('id' => $model->id)) ?>"><i class="fa fa-lock text-success"></i> <?php echo Lang::t('Change your password') ?></a>
    <?php endif; ?>
    <?php if ($can_update || Users::isMyAccount($model->id)): ?>
        <a class="list-group-item" href="<?php echo $this->createUrl('update', array('id' => $model->id)) ?>"><i class="fa fa-edit text-success"></i> <?php echo Lang::t('Edit Profile') ?></a>
    <?php endif; ?>
    <?php if ($can_update || Users::isMyAccount($model->id)): ?>
        <a class="list-group-item" href="<?php echo $this->createUrl('/employees/employees/view', array('id' => $model->id)) ?>"><i class="fa fa-edit text-success"></i> <?php echo Lang::t('View Employment Info') ?></a>
    <?php endif; ?>
    <?php if ($can_update): ?>
        <a class="list-group-item" href="<?php echo $this->createUrl('resetPassword', array('id' => $model->id)) ?>"><i class="fa fa-lock text-success"></i> <?php echo Lang::t('Reset Password') ?></a>
        <?php if ($model->status === Users::STATUS_ACTIVE): ?>
            <a class="list-group-item change_status" href="#" data-ajax-url="<?php echo $this->createUrl('changeStatus', array('id' => $model->id, 'status' => Users::STATUS_BLOCKED)) ?>" ><i class="fa fa-ban text-danger"></i> <?php echo Lang::t('Block Account') ?></a>
        <?php else: ?>
            <a class="list-group-item change_status" href="#" data-ajax-url="<?php echo $this->createUrl('changeStatus', array('id' => $model->id, 'status' => Users::STATUS_ACTIVE)) ?>"><i class="fa fa-check-circle text-success"></i> <?php echo Lang::t('Activate Account') ?></a>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($can_view_activity): ?>
        <a class="hidden list-group-item" href="<?php echo $this->createUrl('activityLog', array('id' => $model->id)) ?>"><i class="fa fa-signal"></i> <?php echo Lang::t('Account activities') ?></a>
    <?php endif; ?>
</div>


