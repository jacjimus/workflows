<?php
if ($this->showLink(UsersModuleConstants::RES_USERS)):
        $this->breadcrumbs = array(
            Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
            $model->name,
        );
else:
        $this->breadcrumbs = array(
            $model->name,
        );
endif;
?>
<div class="row">
        <div class="col-sm-10">
                <h1 class="txt-color-blueDark">
                        <i class="fa fa-fw fa-user"></i>
                        <?php echo CHtml::encode($this->pageTitle); ?>
                </h1>
        </div>
        <div class="col-sm-2">
                <a class="btn btn-danger pull-right" href="<?php echo UrlManager::getReturnUrl($this->showLink(UsersModuleConstants::RES_USERS) ? $this->createUrl('index') : Yii::app()->createUrl('default/index')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
        </div>
</div>
<hr/>
<div class="row">
        <div class="col-xs-12 col-sm-2">
                <?php $this->renderPartial('_user_side_view', array('model' => $model)) ?>
        </div>
        <div class="col-xs-12 col-sm-10">
                <section id="widget-grid">
                        <div class="row">
                                <div class="col-sm-12  sortable-grid ui-sortable">
                                        <?php $this->renderPartial('_view_account', array('model' => $model)) ?>
                                        <?php $this->renderPartial('_view_personal', array('model' => $model)) ?>
                                        <?php $this->renderPartial('_view_address', array('model' => $address_model)) ?>
                                </div>
                        </div>
                </section>
        </div>
</div>