<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
?>
<div class="row">
        <div class="col-md-2">
                <?php $this->renderPartial('users.views.layouts._tab') ?>
        </div>
        <div class="col-md-10">
                <?php $this->renderPartial('users.views.default.forms._form', array('user_model' => $user_model, 'person_model' => $person_model, 'address_model' => $address_model)); ?>
        </div>
</div>





