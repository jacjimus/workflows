<?php

/**
 * The user/staff controller
 * @author Fred <mconyango@gmail.com>
 */
class DefaultController extends UsersModuleController {

    public function init() {
        $this->resource = UsersModuleConstants::RES_USERS;
        $this->resourceLabel = 'User';
        $this->activeMenu = UsersModuleConstants::MENU_USERS;
        $this->activeTab = UsersModuleConstants::TAB_USERS;

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete,changeStatus,deleteLog',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'displaySavedImage', 'create', 'update', 'delete', 'changeStatus', 'resetPassword', 'changePassword', 'deleteLog', 'activityLog'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = NULL) {
        if (NULL === $id)
            $id = Yii::app()->user->id;
        $model = UsersView::model()->loadModel($id);
        if (!Users::isMyAccount($id)) {
            $model->checkPrivilege($this, Acl::ACTION_VIEW);
        }
        $this->pageTitle = $model->name;

        $this->render('view', array(
            'model' => $model,
            'address_model' => PersonAddress::model()->find('`person_id`=:t1', array(':t1' => $id)),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($user_level = NULL) {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        //personal model
        $person_model = new Person();
        $person_model_class_name = $person_model->getClassName();
        //account model
        $user_model = new Users(ActiveRecord::SCENARIO_CREATE);
        $user_model->status = Users::STATUS_ACTIVE;
        $user_model_class_name = $user_model->getClassName();

        //address model
        $address_model = new PersonAddress();
        $address_model_class_name = $address_model->getClassName();


        if (Yii::app()->request->isPostRequest) {
            $person_model->attributes = $_POST[$person_model_class_name];
            $user_model->attributes = $_POST[$user_model_class_name];
            $address_model->attributes = $_POST[$address_model_class_name];

            $person_model->validate();
            $user_model->validate();
            $address_model->validate();

            if (!$person_model->hasErrors() && !$user_model->hasErrors() && !$address_model->hasErrors()) {
                $person_model->save(FALSE);
                $user_model->id = $person_model->id;
                $user_model->save(FALSE);
                $address_model->person_id = $person_model->id;
                $address_model->save(FALSE);

                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl($this->createUrl('view', array('id' => $user_model->id))));
            }
        }

        $user_model->timezone = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, SettingsModuleConstants::SETTINGS_DEFAULT_TIMEZONE, SettingsTimezone::DEFAULT_TIME_ZONE);
        if (!empty($user_level)) {
            $user_model->user_level = $user_level;
        }

        $this->render('create', array(
            'user_model' => $user_model,
            'person_model' => $person_model,
            'address_model' => $address_model,
        ));
    }

    public function actionUpdate($id) {
        $model = UsersView::model()->loadModel($id);
        $user_model = Users::model()->loadModel($id);
        $person_model = Person::model()->loadModel($id);
        if (!Users::isMyAccount($id)) {
            $user_model->checkPrivilege($this, Acl::ACTION_UPDATE, true);
        }
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' Profile');

        $person_model_class_name = $person_model->getClassName();
        $user_model_class_name = $user_model->getClassName();

        //address model
        $address_model = PersonAddress::model()->find('`person_id`=:t1', array(':t1' => $id));
        if (NULL === $address_model) {
            $address_model = new PersonAddress();
            $address_model->person_id = $id;
        }
        $address_model_class_name = $address_model->getClassName();

        //Signature model
        $signature_model = PersonSignature::model()->find('`person_id`=:t1', array(':t1' => $id));
        if (NULL === $signature_model) {
            $signature_model = new PersonSignature();
            $signature_model->person_id = $id;
        }
        $signature_model_class_name = $signature_model->getClassName();


        if (Yii::app()->request->isPostRequest) {
            $person_model->attributes = $_POST[$person_model_class_name];
            $user_model->attributes = $_POST[$user_model_class_name];
            $address_model->attributes = $_POST[$address_model_class_name];
            $signature_model->attributes = $_POST[$signature_model_class_name];

            $person_model->validate();
            $user_model->validate();
            $address_model->validate();
            //$signature_model->validate();

            if (!$person_model->hasErrors() && !$user_model->hasErrors() && !$signature_model->hasErrors() && !$address_model->hasErrors()) {
                $person_model->save(FALSE);
                $user_model->save(FALSE);
                $address_model->save(FALSE);
                $signature_model->save(FALSE);

                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl($this->createUrl('view', array('id' => $user_model->id))));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'user_model' => $user_model,
            'person_model' => $person_model,
            'address_model' => $address_model,
            'signature_model' => $signature_model,
        ));
    }

    public function actionResetPassword($id) {
        $model = UsersView::model()->loadModel($id);
        $user_model = Users::model()->loadModel($id);
        if (!Users::isMyAccount($id)) {
            $user_model->checkPrivilege($this, Acl::ACTION_UPDATE, true);
        }
        $this->pageTitle = Lang::t('Reset password');

        $user_model->setScenario(Users::SCENARIO_RESET_PASSWORD);
        $user_model->password = NULL;
        $model_class_name = $user_model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $user_model->attributes = $_POST[$model_class_name];
            if ($user_model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl($this->createUrl('view', array('id' => $id))));
            }
        }

        $this->render('resetPassword', array(
            'model' => $model,
            'user_model' => $user_model,
        ));
    }

    public function actionChangePassword($id) {

        if (!Users::isMyAccount($id))
            throw new CHttpException(403, Lang::t('403_error'));
        $this->pageTitle = Lang::t('Change your password');

        $model = UsersView::model()->loadModel($id);
        $user_model = Users::model()->loadModel($id);
        $user_model->setScenario(Users::SCENARIO_CHANGE_PASSWORD);
        $user_model->pass = $user_model->password;
        $user_model->password = null;
        $model_class_name = $user_model->getClassName();

        if (isset($_POST[$model_class_name])) {

            $user_model->attributes = $_POST[$model_class_name];
            $user_model->user_to_change_password = 0;
            $currentPass = $user_model->currentPassword;
            if ($user_model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl($this->createUrl('/auth/default/logout')));
            }

            $user_model->currentPassword = $currentPass;
        }

        $this->render('changePassword', array(
            'model' => $model,
            'user_model' => $user_model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = Users::model()->loadModel($id);
        $model->checkPrivilege($this, Acl::ACTION_DELETE, true);
        $model->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $searchModel = UsersView::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'username', Users::model()->getFetchCondition());
        $this->render('index', array(
            'model' => $searchModel,
        ));
    }

    public function actionChangeStatus($id, $status) {
        $model = Users::model()->loadModel($id);

        $model->checkPrivilege($this, Acl::ACTION_UPDATE, true);
        $valid_status = Users::statusOptions();
        if (!in_array($status, $valid_status)) {
            throw new CHttpException(400, Lang::t('400_error'));
        }
        $model->status = $status;
        $model->save(FALSE);

        Yii::app()->user->setFlash('success', Lang::t('Success'));

        echo CJSON::encode(array('redirect_url' => $this->createUrl('view', array('id' => $model->id))));
    }

    public function actionActivityLog($id) {
        $this->resource = UsersModuleConstants::RES_USER_ACTIVITY;
        $model = Users::model()->loadModel($id);
        $this->hasPrivilege(Acl::ACTION_VIEW);

        $this->pageTitle = Lang::t('Audit Trail');

        $this->renderPartial('log/_activity', array(
            'model' => UserActivityLog::model()->searchModel(array('user_id' => $model->id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'date_created asc'),
                ), FALSE, TRUE);
    }

    public function actionDeleteLog() {
        $this->resource = UsersModuleConstants::RES_USER_ACTIVITY;
        $this->hasPrivilege(Acl::ACTION_DELETE);
        if ($ids = filter_input(INPUT_POST, 'ids')) {
            UserActivityLog::model()->deleteMany($ids);
        }
    }

    public function actionDisplaySavedImage($id) {
        $model = PersonSignature::model()->find('person_id=:t1', array(':t1' => $id));
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Transfer-Encoding: base64');
        header('Content-length: ' . $model->file_size);
        header('Content-Type: ' . $model->file_type);
        header('Content-Disposition: attachment; filename=' . $model->file_name);

        echo $model->file_content;
    }

}
