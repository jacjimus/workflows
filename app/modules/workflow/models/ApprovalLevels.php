<?php

/**
 * This is the model class for table "wf_approval_levels".
 *
 * The followings are the available columns in table 'wf_approval_levels':
 * @property integer $id
 * @property string $level_name
 * @property string $leve_desc
 * @property integer $order_no
 * @property string $status_color
 * @property string $date_created
 * @property integer $created_by
 */
class ApprovalLevels extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_approval_levels';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('level_name, order_no, date_created, created_by', 'required'),
            array('order_no, created_by', 'numerical', 'integerOnly' => true),
            array('level_name', 'length', 'max' => 64),
            array('leve_desc', 'length', 'max' => 255),
            array('status_color', 'length', 'max' => 128),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, level_name, leve_desc, order_no, status_color, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'level_name' => 'Level Name',
            'leve_desc' => 'Leve Desc',
            'order_no' => 'Order No',
            'status_color' => 'Status Color',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search56565() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('level_name', $this->level_name, true);
        $criteria->compare('leve_desc', $this->leve_desc, true);
        $criteria->compare('order_no', $this->order_no);
        $criteria->compare('status_color', $this->status_color, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ApprovalLevels the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
            array('level_name', self::SEARCH_FIELD, true, 'OR'),
            array('leve_desc', self::SEARCH_FIELD, true, 'OR'),
            'order_no',
            'id',
        );
    }

}
