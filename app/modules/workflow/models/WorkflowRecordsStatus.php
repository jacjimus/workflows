<?php

/**
 * This is the model class for table "wf_workflow_records_status".
 *
 * The followings are the available columns in table 'wf_workflow_records_status':
 * @property integer $id
 * @property integer $item_id
 * @property string $resource_id
 * @property string $action
 * @property string $date_created
 * @property integer $created_by
 * @property string $last_modified
 * @property string $last_modified_by
 */
class WorkflowRecordsStatus extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_workflow_records_status';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('item_id, resource_id, action', 'required'),
            array('item_id, created_by', 'numerical', 'integerOnly' => true),
            array('resource_id, action', 'length', 'max' => 128),
            array('last_modified_by', 'length', 'max' => 10),
            array('last_modified', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, item_id,action_desc, resource_id, action, date_created, created_by, last_modified, last_modified_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'item_id' => 'Item',
            'resource_id' => 'Resource',
            'action' => 'Action',
            'action_desc' => 'Action Description',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'last_modified' => 'Last Modified',
            'last_modified_by' => 'Last Modified By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search34() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('resource_id', $this->resource_id, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('last_modified', $this->last_modified, true);
        $criteria->compare('last_modified_by', $this->last_modified_by, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'id',
            'item_id',
            'resource_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WorkflowRecordsStatus the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
