<?php

/**
 * This is the model class for table "wf_first_levelof_approval".
 *
 * The followings are the available columns in table 'wf_first_levelof_approval':
 * @property integer $id
 * @property integer $workflow_id
 * @property string $emp_id
 * @property integer $level_id
 * @property integer $lsa
 * @property integer $final
 * @property string $disp_message
 * @property string $date_created
 * @property integer $created_by
 * @property integer $order_no
 */
class WfFirstLevelofApproval extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_first_levelof_approval';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('workflow_id, level_id, created_by, order_no', 'required'),
            array('id, workflow_id, level_id, lsa, final, created_by, order_no', 'numerical', 'integerOnly' => true),
            array('emp_id', 'length', 'max' => 130),
            array('disp_message', 'length', 'max' => 245),
            array('date_created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, workflow_id, emp_id, level_id, lsa, final, disp_message, date_created, created_by, order_no', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'workflow_id' => 'Workflow',
            'emp_id' => 'Emp',
            'level_id' => 'Level',
            'lsa' => 'Lsa',
            'final' => 'Final',
            'disp_message' => 'Disp Message',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'order_no' => 'Order No',
        );
    }

    public function searchParams() {
        return array(
            array('disp_message', self::SEARCH_FIELD, true, 'OR'),
            'workflow_id',
            'final',
            'workflow_id',
            'id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WfFirstLevelofApproval the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
