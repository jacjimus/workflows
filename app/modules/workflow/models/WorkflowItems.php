<?php

/**
 * This is the model class for table "wf_workflow_items".
 *
 * The followings are the available columns in table 'wf_workflow_items':
 * @property integer $id
 * @property integer $workflow_id
 * @property integer $item_id
 * @property string $status
 * @property string $last_modified
 * @property integer $closed
 * @property string $date_created
 * @property integer $created_by
 */
class WorkflowItems extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_workflow_items';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('workflow_id,item_id', 'required'),
            array('workflow_id, item_id, closed, created_by', 'numerical', 'integerOnly' => true),
            array('workflow_id+item_id', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator', 'caseSensitive' => true),
            array('status', 'length', 'max' => 25),
            array('last_modified', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, workflow_id, item_id, status, last_modified, closed, date_created, created_by', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'workflow_id' => Lang::t('Workflow'),
            'item_id' => Lang::t('Item'),
            'status' => Lang::t('Status'),
            'last_modified' => Lang::t('Last Modified'),
            'closed' => Lang::t('Closed'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search45454() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('workflow_id', $this->workflow_id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('last_modified', $this->last_modified, true);
        $criteria->compare('closed', $this->closed);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'status',
            'workflow_id',
            'id',
            'closed',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WorkflowItems the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
