<?php

/**
 * This is the model class for table "wf_workflow_link".
 *
 * The followings are the available columns in table 'wf_workflow_link':
 * @property integer $id
 * @property integer $workflow_id
 * @property string $emp_id
 * @property integer $level_id

 * @property integer $lsa
 * @property integer $final
 * @property string $date_created
 * @property integer $created_by
 */
class WorkflowLink extends ActiveRecord {

    const WKF_ID_GET_PARAM = 'workflowid';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_workflow_link';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('workflow_id, level_id, emp_id, disp_message', 'required'),
            array('workflow_id, level_id, lsa, final, created_by', 'numerical', 'integerOnly' => true),
            array('workflow_id+level_id', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator', 'caseSensitive' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, workflow_id,disp_message, emp_id, level_id, lsa, final, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'workflow_id' => 'Workflow',
            'emp_id' => 'Employees',
            'level_id' => 'Level',
            'disp_message' => 'Display Status',
            'lsa' => 'Lsa',
            'final' => 'Final',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search454() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('workflow_id', $this->workflow_id);
        $criteria->compare('emp_id', $this->emp_id, true);
        $criteria->compare('level_id', $this->level_id);
        $criteria->compare('lsa', $this->lsa);
        $criteria->compare('final', $this->final);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('created_by', $this->created_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchParams() {
        return array(
            'id',
            'workflow_id',
        );
    }

    public function workflowFilter($controller) {
        $values = Workflow::model()->getListData('workflow_id', 'workflow_name', TRUE);
        return $values;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WorkflowLink the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterSave() {
        $modeldel = LinkUsers::model()->deleteAll(array("condition" => "link_id='$this->id'"));
        $linkusers = explode(',', $this->emp_id);
        foreach ($linkusers as $value) {
            $modellinkusers = new LinkUsers();
            $modellinkusers->link_id = $this->id;
            $modellinkusers->emp_id = $value;
            $modellinkusers->save();
        }
        return parent::afterSave();
    }

}
