<?php

/**
 * This is the model class for table "wf_workflow".
 *
 * The followings are the available columns in table 'wf_workflow':
 * @property integer $workflow_id
 * @property string $workflow_name
 * @property string $workflow_desc
 * @property string $resource_id
 * @property string $is_valid
 * @property string $date_created
 * @property integer $created_by
 */
class Workflow extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wf_workflow';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('workflow_name, resource_id', 'required'),
            array('workflow_id, created_by', 'numerical', 'integerOnly' => true),
            array('workflow_name, resource_id', 'length', 'max' => 80),
            array('is_valid', 'length', 'max' => 1),
            array('workflow_desc', 'filter', 'filter' => 'strip_tags'),
            array('workflow_desc, date_created', 'safe'),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'workflow_id' => 'Workflow',
            'workflow_name' => 'Workflow Name',
            'workflow_desc' => 'Workflow Desc',
            'resource_id' => 'Resource',
            'is_valid' => 'Is Valid',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    public function searchParams() {
        return array(
            array('workflow_name', self::SEARCH_FIELD, true, 'OR'),
            array('workflow_desc', self::SEARCH_FIELD, true, 'OR'),
            'workflow_id',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Workflow the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
