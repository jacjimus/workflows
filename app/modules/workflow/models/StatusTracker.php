<?php

/**
 * This is the model class for table "status_tracker".
 *
 * The followings are the available columns in table 'status_tracker':
 * @property integer $id
 * @property integer $item_id
 * @property string $resource_id
 * @property string $comment
 * @property string $date_created
 * @property integer $created_by
 */
class StatusTracker extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'status_tracker';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date_created', 'required'),
            array('item_id,path,latest, created_by', 'numerical', 'integerOnly' => true),
            array('resource_id', 'length', 'max' => 128),
            array('comment', 'length', 'max' => 256),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, item_id,path,latest, resource_id, comment, date_created, created_by', 'safe', 'on' => 'search'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'item_id' => 'Item',
            'resource_id' => 'Resource',
            'comment' => 'Comment',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
        );
    }

    public function searchParams() {
        return array(
            array('resource_id', self::SEARCH_FIELD, true, 'OR'),
            'item_id',
            'id',
            'path',
            'latest',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
