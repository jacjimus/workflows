<?php

/*
 * This class does all that pertains to workflow i.e inserting an item to workflow and tracking its status
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of workflowcontrol
 *
 * @author Felix
 */
class workflowcontrol {

    const approved = 1;
    const rejected = 2;
    const cancelled = 3;
    const returned = 4;

    //put your code here
    //Insert the item into workflow
    public static function insertintoworkflow($resourceid, $itemid, $table) {
        //Get resource so that you can retrieve the workflow it belongs to

        $resourcemodel = Workflow::model()->find('resource_id=:t1', array(':t1' => $resourceid));

        //Get the initial status of this particular workflow
        $initialstatemodel = Getinitialworkflowstate::model()->find('workflow_id=:workflow_id AND order_no=0'
                , array(':workflow_id' => $resourcemodel->workflow_id));

        //if ($initialstatemodel !== NULL) {
        $model = new WorkflowItems();
        $model->workflow_id = $resourcemodel->workflow_id;
        $model->item_id = $itemid;
        //$model->status = $initialstatemodel->id;
        $model->status = 0;
        $model->closed = 0;
        $model->save();

        //Get the initial Status
        $initialstatus = WorkflowLink::model()->getColumnData('disp_message', 'id=0');


        //Update the initial status
        Yii::app()->db->createCommand()
                ->update($table, array(
                    'status' => $initialstatus[0],
                    'submitted' => 1,
                    'submitted_at' => new CDbExpression('NOW()'),
                        ), '`id`=:t1', array(':t1' => $itemid));
        //}
        //This gives a record first status

        $modelrecord = new WorkflowRecordsStatus;

        $modelrecord->resource_id = $resourceid;
        $modelrecord->action = Lang::t("Initialized");

        $modelrecord->item_id = $itemid;
        $modelrecord->action_desc = Lang::t("Initialized");
        $modelrecord->save(FALSE);
    }

    public static function getStatus($itemid) {
        //Check the current status
        $modelstatus = Getstatus::model()->find('item_id=:itemid', array(':itemid' => $itemid));
        if (NULL !== $modelstatus)
            return $modelstatus->level_name;
        return FALSE;
    }

    //This function is for updating status of a workflow item
    // If it is approved, move it to the next level if it is not in the finale level
    public static function updatestatus($itemid, $actionid, $approver, $reason, $resourceid, $table, $currstatus) {

        $resourcemodel = Workflow::model()->find('resource_id=:resid', array(':resid' => $resourceid));

        switch ($actionid) {
            case 1:
                $modelupdatestatus = GetMyLevelDetails::model()->find(
                        'item_id=:item_id AND emp_id=:emp_id AND resource_id=:t3 AND id>:t1', array(':item_id' => $itemid,
                    ':emp_id' => $approver, ':t1' => $currstatus, ':t3' => $resourceid));


                if ($modelupdatestatus->final == FALSE) {
                    self::updaterecordstatus($itemid, $resourcemodel->workflow_id, $modelupdatestatus->id, $table, FALSE);
                    //self::insertactionhistory($itemid, $actionid, $reason, $approver, $resourceid);
                    Yii::app()->db->createCommand()
                            ->update($table, array(
                                'display_status' => $modelupdatestatus->disp_message,
                                    ), '`id`=:t1', array(':t1' => $itemid));
                } else {

                    self::updaterecordstatus($itemid, $resourcemodel->workflow_id, $modelupdatestatus->id, $table, TRUE);
                    Yii::app()->db->createCommand()
                            ->update($table, array(
                                'approved' => TRUE,
                                'display_status' => $modelupdatestatus->disp_message,
                                    ), '`id`=:t1', array(':t1' => $itemid));
                }
                //self::insertactionhistory($itemid, $actionid, $reason, $approver, $resourceid);
                //Send notifications- Coming soon
                break;
            case 3:
                self::updaterecordstatus($itemid, $resourcemodel->workflow_id, $modelupdatestatus->id, $table);
                //self::insertactionhistory($itemid, $actionid, $reason, $approver, $resourceid);
                //Send notifications- Coming soon
                break;
            case 2:
                self::updaterecordstatus($itemid, $resourcemodel->workflow_id, 0, $table);
                $approvername = Employeeslist::model()->find('id=:t1', array(':t1' => $approver));
                Yii::app()->db->createCommand()
                        ->update($table, array(
                            'rejected' => TRUE,
                            'reject_reason' => 'Rejected by ' . $approvername->empname,
                            'display_status' => 'Rejected by ' . $approvername->empname,
                                ), '`id`=:t1', array(':t1' => $itemid));
                /// self::insertactionhistory($itemid, $actionid, $reason, $approver, $resourceid);
                //Send notifications- Coming soon
                break;
            case 4:
                //self::insertactionhistory($itemid, $actionid, $reason, $approver, $resourceid);
                //Send notifications- Coming soon
                break;
            default:
                break;
        }
    }

    public static function insertactionhistory($itemid, $action_id, $reason, $approverid, $resourceid) {
        $modelhistory = new Actionhistory;
        $modelhistory->action_id = $action_id;
        $modelhistory->item_id = $itemid;
        $modelhistory->reason = $reason;
        $modelhistory->approver_id = $approverid;
        $modelhistory->resource_id = $resourceid;
        $modelhistory->save();
    }

    public function check_if_am_approver($resource_id) {
        $approvallevel = GetlevelBelowMe::model()->find('resource_id=:t1 AND emp_id=:t2', array(':t1' => $resource_id, ':t2' => Yii::app()->user->id));
        if (empty($approvallevel))
            return FALSE;
        return TRUE;
    }

    public static function updaterecordstatus($itemid, $wkflowid, $status, $tablename, $final = FALSE) {
        //if this is the first approval by the supervisor, then map it to the first one in the row
        if ($status === NULL) {
            $status = WfFirstLevelofApproval::model()->getColumnData('id', 'workflow_id=' . $wkflowid);
        }
        $status_update = WorkflowLink::model()->getColumnData('disp_message', 'id=' . $status);
        Yii::app()->db->createCommand()
                ->update('wf_workflow_items', array(
                    'status' => $status, 'closed' => $final,
                        ), '`item_id`=:t1 AND workflow_id=:wkflowid', array(':t1' => $itemid,
                    ':wkflowid' => $wkflowid));
        Yii::app()->db->createCommand()
                ->update($tablename, array(
                    'status' => $status_update[0],
                        ), '`id`=:t1', array(':t1' => $itemid));
        return TRUE;
    }

    public static function getMyworkflowsequence($resourceid, $userid) {

        $myordernumberapproval = Yii::app()->db->createCommand()
                ->select('wf_approval_levels.order_no AS order_no')
                ->from('wf_workflow_link')
                ->join('wf_approval_levels', 'wf_workflow_link.level_id = wf_approval_levels.id')
                ->join('wf_workflow', 'wf_workflow_link.workflow_id = wf_workflow.workflow_id')
                ->join('wf_link_users', 'wf_workflow_link.id = wf_link_users.link_id')
                ->where('wf_workflow.resource_id=:t1 AND wf_workflow_link.emp_id=:t2', array(':t2' => $userid, ':t1' => $resourceid))
                ->queryRow();


        if (empty($myordernumberapproval))
            return 0;


        return $myordernumberapproval['order_no'];
    }

    public static function getMyworkflowsequencebyresource($resourceid, $userid, $currstate) {

        $myordernumberapproval = Yii::app()->db->createCommand()
                ->select('wf_approval_levels.order_no AS order_no')
                ->from('wf_workflow_link')
                ->join('wf_approval_levels', 'wf_workflow_link.level_id = wf_approval_levels.id')
                ->join('wf_workflow', 'wf_workflow_link.workflow_id = wf_workflow.workflow_id')
                ->join('wf_link_users', 'wf_workflow_link.id = wf_link_users.link_id')
                ->where('wf_workflow.resource_id=:t1 AND wf_workflow_link.emp_id=:t2 AND wf_workflow_link.id > :t3', array(':t2' => $userid, ':t1' => $resourceid, ':t3' => $currstate))
                ->queryRow();

        if (empty($myordernumberapproval))
            return 0;

        return $myordernumberapproval['order_no'];
    }

    public static function getitemLevelbelowminebyresource($resourceid, $userid, $currstate) {

        $orderno = self::getMyworkflowsequencebyresource($resourceid, $userid, $currstate) > 0 ? self::getMyworkflowsequencebyresource($resourceid, $userid, $currstate) - 1 : 0;

        $levebelow = Yii::app()->db->createCommand()
                ->select('wf_workflow_link.id AS id,wf_approval_levels.level_name AS level_name'
                        . ',wf_approval_levels.leve_desc AS leve_desc'
                        . ',wf_workflow_items.item_id AS item_id'
                        . ',wf_workflow.resource_id AS resource_id'
                        . ',wf_link_users.emp_id AS emp_id'
                        . ',wf_approval_levels.order_no AS order_no')
                ->from('wf_workflow_link')
                ->join('wf_approval_levels', 'wf_workflow_link.level_id = wf_approval_levels.id')
                ->join('wf_workflow_items', 'wf_workflow_link.workflow_id = wf_workflow_items.workflow_id')
                ->join('wf_workflow', 'wf_workflow_link.workflow_id = wf_workflow.workflow_id')
                ->join('wf_link_users', 'wf_workflow_link.id = wf_link_users.link_id')
                ->where('resource_id=:t1 AND wf_approval_levels.order_no=:t2 AND wf_workflow_link.id <= :t3', array(':t2' => $orderno, ':t1' => $resourceid, ':t3' => $currstate))
                ->queryRow();

        if (empty($levebelow)) {
            $myval = 0;
        } else {
            $myval = $levebelow['id'];
        }

        return $myval;
    }

    public static function getitemLevelaboveminebyresource($resourceid, $userid) {
        $orderno = self::getMyworkflowsequencebyresource($resourceid, $userid) > 0 ? self::getMyworkflowsequencebyresource($resourceid, $userid) + 1 : 0;
        $levelabove = Yii::app()->db->createCommand()
                ->select('wf_workflow_link.id AS id,wf_approval_levels.level_name AS level_name'
                        . ',wf_approval_levels.leve_desc AS leve_desc'
                        . ',wf_workflow_items.item_id AS item_id'
                        . ',wf_workflow.resource_id AS resource_id'
                        . ',wf_link_users.emp_id AS emp_id'
                        . ',wf_approval_levels.order_no AS order_no')
                ->from('wf_workflow_link')
                ->join('wf_approval_levels', 'wf_workflow_link.level_id = wf_approval_levels.id')
                ->join('wf_workflow_items', 'wf_workflow_link.workflow_id = wf_workflow_items.workflow_id')
                ->join('wf_workflow', 'wf_workflow_link.workflow_id = wf_workflow.workflow_id')
                ->join('wf_link_users', 'wf_workflow_link.id = wf_link_users.link_id')
                ->where('resource_id=:t1 AND wf_approval_levels.order_no=:t2', array(':t2' => $orderno, ':t1' => $resourceid))
                ->queryRow();

        return $levelabove['id'];
    }

    public static function getapproversabovemine($resourceid, $userid, $currstate) {

        if (!empty($userid)) {
            $orderno = self::getMyworkflowsequencebyresource($resourceid, $userid, $currstate) > 0 ? self::getMyworkflowsequencebyresource($resourceid, $userid, $currstate) + 1 : 0;
        } else {
            $orderno = 1;
        }
        //This will be changed to $orderno+1 to take care of sequencial notification
        $approversabove = ApproversAboveMe::model()->getColumnData('id', 'resource_id=' . "'" . $resourceid . "'" . ' AND order_no >' . $orderno);

        return $approversabove;
    }

    public static function getcurrentstatusaction($resourceid, $itemid) {
        $currentstatus = Yii::app()->db->createCommand()
                ->select('status')
                ->from('wf_workflow_items')
                ->join('wf_workflow', 'wf_workflow_items.workflow_id = wf_workflow.workflow_id')
                ->where('wf_workflow.resource_id=:t1 AND wf_workflow_items.item_id=:t2', array(':t1' => $resourceid, ':t2' => $itemid))
                ->queryRow();
        return $currentstatus['status'];
    }

    public static function getcurrentstatus($resourceid) {
        $currentstatus = Yii::app()->db->createCommand()
                ->select('status')
                ->from('wf_workflow_items')
                ->join('wf_workflow', 'wf_workflow_items.workflow_id = wf_workflow.workflow_id')
                ->where('wf_workflow.resource_id=:t1 ', array(':t1' => $resourceid))
                ->queryRow();
        return $currentstatus['status'];
    }

    public static function getitemLevelbelowmine($itemid, $resourceid, $userid) {
        $orderno = self::getMyworkflowsequence($itemid, $resourceid, $userid) > 0 ? self::getMyworkflowsequence($itemid, $resourceid, $userid) - 1 : 0;
        $levebelow = GetlevelBelowMe::model()->find(
                'item_id=:t1 AND resource_id=:t2 AND order_no=:t3', array(':t1' => $itemid, ':t2' => $resourceid, ':t3' => $orderno));
        return $levebelow->order_no;
    }

    public static function getImmediateBossEmail() {
        $employee = Yii::app()->db->createCommand()
                ->select('id , emp_code, hire_date, job_title_id , department_id
                        , manager_id , branch_id , work_hours_perday  , employment_class_id , employment_cat_id
                        , pay_type_id , currency_id , salary  , employment_status  , email    , mobile
                        , company_phone , company_phone_ext , country_id , paygroup_id')
                ->from('hr_employees u')
                ->where('id=:id', array(':id' => Yii::app()->user->id))
                ->queryRow();


        $manager = Yii::app()->db->createCommand()
                ->select('id , emp_code, hire_date, job_title_id , department_id
                        , manager_id , branch_id , work_hours_perday  , employment_class_id , employment_cat_id
                        , pay_type_id , currency_id , salary  , employment_status  , email    , mobile
                        , company_phone , company_phone_ext , country_id , paygroup_id')
                ->from('hr_employees m')
                ->where('id=:id', array(':id' => $employee['manager_id']))
                ->queryRow();
        $result = array(
            'email' => $manager['email'],
            'id' => $manager['id'],
        );

        return $result;
    }

    public static function submitforapproval($id, $table) {
        Yii::app()->db->createCommand()
                ->update($table, array(
                    'submit' => 1), '`id`=:t1', array(':t1' => $id));
    }

}
