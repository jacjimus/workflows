<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'workflow_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'workflow_id', Workflow::model()->getListData('workflow_id', 'workflow_name', false), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'emp_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'emp_id', Employeeslist::model()->getListData('id', 'empname', false), array('multiple' => 'multiple', 'class' => 'chzn-select  chzn-ltr span3 chzn-select-deselect form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'level_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'level_id', ApprovalLevels::model()->getListData('id', 'level_name', false), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'lsa', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->textField($model, 'lsa', array('class' => 'form-control', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'disp_message', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->textArea($model, 'disp_message', array('rows' => 2, 'cols' => 30)); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-6">
            <label class="checkbox">
                <?php echo $form->checkBox($model, 'final', array('class' => 'icheck')); ?>
                &nbsp;<?php echo $model->getAttributeLabel('final') ?>
            </label>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.css', $media = 'screen')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.jquery.js', CClientScript::POS_END)
        ->registerScript('workflow.workflowlink._form', "$('.chzn-select').chosen();");
?>