<?php

$grid_id = 'wf-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => '<i class="fa fa-list"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'workflow_id',
            array(
                'name' => 'workflow_name',
                'value' => '$data->workflow_name'
            ),
            array(
                'name' => 'is_valid',
                'value' => 'Common::getBoolDesc($data->is_valid)'
            ),
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->workflow_id))',
                        'visible' => '$this->grid->owner->showLink("' . WorkflowModuleConstants::RES_WORKFLOW . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("workflow_id"=>$data->workflow_id))',
                        'visible' => '$this->grid->owner->showLink("' . WorkflowModuleConstants::RES_WORKFLOW . '", "' . Acl::ACTION_DELETE . '")&&$data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>


