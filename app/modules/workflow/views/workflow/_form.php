<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'resource_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->dropDownList($model, 'resource_id', UserResources::model()->getListData('id', 'description', true, 'approveable=:approveable', array(':approveable' => true)), array('class' => 'form-control')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'workflow_name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'workflow_name', array('class' => 'form-control', 'maxlength' => 255)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'workflow_desc', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textArea($model, 'workflow_desc', array('class' => 'form-control', 'rows' => 4)); ?>
                </div>
        </div>
        <div class="form-group">
                <div class="col-md-offset-3 col-md-6">
                        <label class="checkbox">
                                <?php echo $form->checkBox($model, 'is_valid', array('class' => 'icheck')); ?>
                                &nbsp;<?php echo $model->getAttributeLabel('is_valid') ?>
                        </label>
                </div>
        </div>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>