
<?php if (ModulesEnabled::model()->isModuleEnabled(WorkflowModuleConstants::MOD_WORKFLOW)): ?>
    <?php if ($this->showLink(WorkflowModuleConstants::RES_WORKFLOW)): ?>
<li class="<?php echo $this->activeMenu === WorkflowModuleController::MENU_WORKFLOW ? 'active' : '' ?>">
            <a href="<?php echo Yii::app()->createUrl('default/index') ?>"><i class="fa fa-lg fa-fw fa-barcode"></i> <span class="menu-item-parent"><?php echo Lang::t('Projects Manager') ?></span></a>
        </li>
    <?php endif; ?>
<?php endif; ?>