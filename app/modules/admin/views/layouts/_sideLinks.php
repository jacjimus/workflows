<?php if (ModulesEnabled::model()->isModuleEnabled(AdminModuleConstants::MOD_ADMIN)): ?>
      <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-male"></i> <span class="menu-item-parent"><?php echo Lang::t('Admin') ?></span></a>
            <ul>
                  <?php if ($this->showLink(ReqModuleConstants::RES_ADMIN_TRAVEL)): ?>
                        <li class="<?php echo $this->activeMenu === AdminModuleController::MENU_TRAVEL ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('travelrequisitionHead/index') ?>"><i class="icon-double-angle-right"></i> <?php echo Lang::t('Travel Requisition') ?></a></li>
                        <?php endif; ?>
            </ul>
      </li>
<?php endif; ?>