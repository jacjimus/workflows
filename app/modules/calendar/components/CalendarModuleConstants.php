<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class CalendarModuleConstants {

      const MOD_EVENTS = 'events';
      //resource constants
      const RES_EVENT_REMINDERS = 'EVENT_REMINDERS';
      //menu constants
      const MENU_EVENT = 'MENU_EVENT';
      const TAB_EVENT = 'TAB_EVENT';
      const TAB_SETTINGS = 'TAB_SETTINGS';

}
