<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property string $id
 * @property string $event_type_id
 * @property string $name
 * @property string $description
 * @property string $initial_start_date
 * @property string $initial_end_date
 * @property string $repeated
 * @property string $user_id
 * @property integer $is_active
 * @property integer $location_id
 * @property string $color_class
 * @property string $date_created
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property EventType $eventType
 * @property EventOccurrence[] $eventOccurrences
 */
class MeEvent extends ActiveRecord implements IMyActiveSearch, IMyNotifManager {

    const REPEATED_NONE = 'None';
    const REPEATED_DAILY = 'Daily';
    const REPEATED_WEEKLY = 'Weekly';
    const REPEATED_MONTHLY = 'Monthly';
    const REPEATED_YEARLY = 'Yearly';
    const REPEATED_QUATERLY = 'Quarterly';

    /**
     * activity states
     */
    const STATE_PENDING = 'PENDING';
    const STATE_ONGOING = 'ONGOING';
    const STATE_RESCHEDULED = 'RESCHEDULED';
    const STATE_COMPLETED = 'COMPLETED';
    const STATE_CANCELLED = 'CANCELLED';

    /**
     *
     * @var type
     */
    public $current_initial_start_date;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'me_event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('event_type_id, name, initial_start_date,supervisor_id,country_project_id,activity_type_id,location_id,person_responsible_id,status', 'required'),
            array('user_id, country_project_id,location_id,created_by,activity_type_id', 'length', 'max' => 11),
            array('name,description', 'length', 'max' => 128),
            array('repeated', 'length', 'max' => 7),
            array('initial_end_date,actual_date, is_active,location_id,color_class,activity_group', 'safe'),
            //array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'eventType' => array(self::BELONGS_TO, 'EventType', 'event_type_id'),
            'eventOccurrences' => array(self::HAS_MANY, 'EventOccurrence', 'event_id'),
            'location' => array(self::BELONGS_TO, 'MeCountryLocationProject', 'location_id'),
            'countryProject' => array(self::BELONGS_TO, 'MeCountryProjectsView', 'country_project_id'),
            'personResponsible' => array(self::BELONGS_TO, 'Users', 'person_responsible_id'),
            'supervisor' => array(self::BELONGS_TO, 'Users', 'supervisor_id'),
            'activityType' => array(self::BELONGS_TO, 'MeActivityTypes', 'activity_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Lang::t('ID'),
            'event_type_id' => Lang::t('Event Type'),
            'name' => Lang::t('Title'),
            'initial_start_date' => Lang::t('Scheduled Start Date'),
            'initial_end_date' => Lang::t('Scheduled End Date'),
            'repeated' => Lang::t('Repeated'),
            'user_id' => Lang::t('User'),
            'date_created' => Lang::t('Date Created'),
            'created_by' => Lang::t('Created By'),
            'is_active' => Lang::t('Active'),
            'activity_type_id' => Lang::t('Activity Type'),
            'supervisor_id' => Lang::t('Supervisor'),
            'person_responsible_id' => Lang::t('Person Responsible'),
            'status' => Lang::t('Status'),
            'location_id' => Lang::t('Location'),
            'color_class' => Lang::t('Select Event Color'),
            'description' => Lang::t('Short description'),
            'actual_date' => Lang::t('Actual Date'),
            'country_project_id' => Lang::t('Project Name'),
            'activity_group' => Lang::t('Allowed Users'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Event the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        $this->setColor();
        return parent::beforeSave();
    }

    public function afterSave() {
        if ($this->isNewRecord || $this->status == self::STATE_RESCHEDULED) {
            EventOccurrence::model()->addEventOccurrence($this->id, $this->initial_start_date, $this->initial_end_date);
        }
        if ($this->activity_group != null)
            $this->updateEventGroups();
        return parent::afterSave();
    }

    public function updateEventGroups() {
        $users = explode(',', $this->activity_group);

        foreach ($users as $key => $value) {
            $user = EventGroups::model()->find('`user_id`=:t1 AND `event_id`=:t2', array(':t1' => $value, ':t2' => $this->id));
            if (empty($user)) {
                $user = new EventGroups();
                $user->user_id = $value;
                $user->event_id = $this->id;
                $user->save(FALSE);
            }
        }
    }

    public function afterFind() {
        $this->current_initial_start_date = $this->initial_start_date;
        return parent::afterFind();
    }

    public function searchParams() {
        return array(
            array('name', self::SEARCH_FIELD, true, 'OR'),
            array('status', self::SEARCH_FIELD, true, 'OR'),
            'event_type_id',
            'repeated',
            'user_id',
            'is_active',
            'location_id',
            'person_responsible_id',
            'supervisor_id',
            'actual_date',
            'activity_group',
        );
    }

    public static function eventColorOptions() {

        return array(
            'bg-color-darken' => 'bg-color-darken txt-color-white',
            'bg-color-blue' => 'bg-color-blue txt-color-white',
            'bg-color-orange' => 'bg-color-orange txt-color-white',
            'bg-color-greenLight' => 'bg-color-greenLight txt-color-white',
            'bg-color-blueLight' => 'bg-color-blueLight txt-color-white',
            'bg-color-red' => 'bg-color-red txt-color-white',
        );
    }

    public function setColor() {
        if ($this->status == self::STATE_CANCELLED)
            $this->color_class = 'bg-color-red txt-color-white';
        elseif ($this->status == self::STATE_COMPLETED)
            $this->color_class = 'bg-color-blue txt-color-white';
        elseif ($this->status == self::STATE_ONGOING)
            $this->color_class = 'bg-color-greenLight txt-color-white';
        elseif ($this->status == self::STATE_PENDING)
            $this->color_class = 'bg-color-yellow txt-color-white';
        elseif ($this->status == self::STATE_RESCHEDULED)
            $this->color_class = 'bg-color-orange txt-color-white';
        else
            $this->color_class = 'bg-color-darken txt-color-white';
    }

    public function getStatusLabel() {
        $code = $this->status;
        if ($code == self::STATE_ONGOING)
            return 'label label-success';
        elseif ($code == self::STATE_RESCHEDULED)
            return 'label label-warning';
        elseif ($code == self::STATE_CANCELLED)
            return 'label label-danger';
        elseif ($code == self::STATE_COMPLETED)
            return 'label label-default';
        elseif ($code == self::STATE_PENDING)
            return 'label label-primary';
    }

    public static function repeatedOptions() {
        return array(
            self::REPEATED_NONE => Lang::t(self::REPEATED_NONE),
            self::REPEATED_DAILY => Lang::t(self::REPEATED_DAILY),
            self::REPEATED_WEEKLY => Lang::t(self::REPEATED_WEEKLY),
            self::REPEATED_MONTHLY => Lang::t(self::REPEATED_MONTHLY),
            self::REPEATED_YEARLY => Lang::t(self::REPEATED_YEARLY),
            self::REPEATED_QUATERLY => Lang::t(self::REPEATED_QUATERLY),
        );
    }

    public function setStatus() {
        if ($this->initial_start_date < date('Y-m-d')) {
            $this->status = self::STATE_PENDING;
        }
        return $this->save(FALSE);
    }

    public static function statusOptions() {
        return array(
            self::STATE_CANCELLED => Lang::t(self::STATE_CANCELLED),
            self::STATE_COMPLETED => Lang::t(self::STATE_COMPLETED),
            self::STATE_ONGOING => Lang::t(self::STATE_ONGOING),
            self::STATE_PENDING => Lang::t(self::STATE_PENDING),
            self::STATE_RESCHEDULED => Lang::t(self::STATE_RESCHEDULED),
        );
    }

    /**
     * Get events
     * @return type
     */
    public function getEvents() {
        $conditions = '(`status` !=:t1 AND  `status` !=:t2 AND `supervisor_id` =:user_id OR `person_responsible_id` =:user_id OR `created_by`=:user_id) ';
        $params = array(':user_id' => Yii::app()->user->id, ':t1' => 'CANCELLED', ':t2' => 'COMPLETED');
        return $this->getData('*', $conditions, $params);
    }

    public function processNotifEmailTemplate($email_template, $item_id) {
        //placeholders supported: {{event_name}},{{date}}
        $row = EventOccurrence::model()->getRow('event_id,date_from', '`id`=:id', array(':id' => $item_id));
        if (!empty($row)) {
            $event_name = $this->get($row['event_id'], 'name');
            return Common::myStringReplace($email_template, array(
                        '{{event_name}}' => CHtml::link(CHtml::encode($event_name), Yii::app()->createAbsoluteUrl('event/default/index')),
                        '{{date}}' => MyYiiUtils::formatDate($row['date_from'], 'M j, Y'),
            ));
        }
        return $email_template;
    }

    public function processNotifTemplate($template, $item_id) {
        //placeholders supported: {{event_name}},{{date}}
        $row = EventOccurrence::model()->getRow('event_id,date_from', '`id`=:id', array(':id' => $item_id));
        if (!empty($row)) {
            $event_name = $this->get($row['event_id'], 'name');
            return Common::myStringReplace($template, array(
                        '{{event_name}}' => CHtml::link(CHtml::encode($event_name), Yii::app()->createUrl('event/default/index')),
                        '{{date}}' => MyYiiUtils::formatDate($row['date_from'], 'M j, Y'),
            ));
        }
        return $template;
    }

    public function updateNotification() {
        if (ModulesEnabled::model()->isModuleEnabled(EventModuleConstants::MOD_EVENTS)) {
            //vehicle servicing notification
            $notification_date = Notif::model()->getNotificationDate(EventType::NOTIF_EVENTS_REMINDER);
            $data = EventOccurrence::model()->getData('id,event_id,date_from', 'DATE(`date_from`)=DATE(:t1) AND `notified`=:t2', array(':t1' => $notification_date, ':t2' => 0));
            if (!empty($data)) {
                foreach ($data as $row) {
                    Notif::model()->pushNotif(EventType::NOTIF_EVENTS_REMINDER, $row['id']);
                    //mark as notified
                    EventOccurrence::model()->markAsNotified($row['id']);
                    //create the next occurrence
                    EventOccurrence::model()->createNextOccurrence($row['event_id'], $row['date_from']);
                }
            }
        }
    }

}
