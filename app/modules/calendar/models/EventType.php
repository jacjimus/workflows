<?php

/**
 * This is the model class for table "event_type".
 *
 * The followings are the available columns in table 'event_type':
 * @property string $id
 * @property string $name
 * @property string $notif_type_id
 * @property string $date_created
 * @property integer $is_one_day_event
 * @property string $created_by
 *
 * The followings are the available model relations:
 * @property Event[] $events
 * @property NotifTypes $notifType
 */
class EventType extends ActiveRecord implements IMyActiveSearch {

      //notif types
      const NOTIF_EVENTS_REMINDER = 'events_reminder';
      //event types
      const TYPE_UTILITY_PAYMENTS = 'utility_payments_reminder';

      public function init() {
            parent::init();
            $this->notif_type_id = self::NOTIF_EVENTS_REMINDER;
      }

      /**
       * @return string the associated database table name
       */
      public function tableName() {
            return 'event_type';
      }

      /**
       * @return array validation rules for model attributes.
       */
      public function rules() {
            return array(
                array('id,name, notif_type_id', 'required'),
                array('is_one_day_event', 'numerical', 'integerOnly' => true),
                array('name', 'length', 'max' => 128),
                array('notif_type_id', 'length', 'max' => 60),
                array('created_by', 'length', 'max' => 11),
                array('name', 'unique', 'message' => Lang::t('{value} already exists.')),
                array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
            );
      }

      /**
       * @return array relational rules.
       */
      public function relations() {
            return array(
                'events' => array(self::HAS_MANY, 'Event', 'event_type_id'),
                'notifType' => array(self::BELONGS_TO, 'NotifTypes', 'notif_type_id'),
            );
      }

      /**
       * @return array customized attribute labels (name=>label)
       */
      public function attributeLabels() {
            return array(
                'id' => Lang::t('ID'),
                'name' => Lang::t('Name'),
                'notif_type_id' => Lang::t('Notif Type'),
                'date_created' => Lang::t('Date Created'),
                'is_one_day_event' => Lang::t('1 day event'),
                'created_by' => Lang::t('Created By'),
            );
      }

      /**
       * Returns the static model of the specified AR class.
       * Please note that you should have this exact method in all your CActiveRecord descendants!
       * @param string $className active record class name.
       * @return EventType the static model class
       */
      public static function model($className = __CLASS__) {
            return parent::model($className);
      }

      public function searchParams() {
            return array(
                array('name', self::SEARCH_FIELD, true),
                'notif_type_id',
                'is_one_day_event',
            );
      }

}
