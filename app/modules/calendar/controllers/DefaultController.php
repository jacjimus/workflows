<?php

class DefaultController extends EventModuleController {

    public function init() {
        $this->resource = CalendarModuleConstants::RES_EVENT_REMINDERS;
        $this->resourceLabel = 'Activity Reminder';
        $this->activeTab = CalendarModuleConstants::TAB_EVENT;
        $this->activeMenu = MeModuleConstants::MENU_PROGRAM;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete,addOccurrence',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'addOccurrence', 'view'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     *
     * @param type $id
     * displays a given model
     */
    public function actionView($id) {
        $this->hasPrivilege();

        $model = $this->loadModel($id);
        $this->pageTitle = $model->name;
        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $model = new MeEvent();
        $model->event_type_id = 'Activity';
        $model->status = MeEvent::STATE_ONGOING;
        $model_class_name = $model->getClassName();
        $this->pageTitle = 'Create new activity reminder';

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), false, true);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $model = MeEvent::model()->loadModel($id);
        $model_class_name = $model->getClassName();
        $this->pageTitle = 'Update activity reminder';

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                echo CJSON::encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(FALSE);
                echo CJSON::encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), false, true);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->hasPrivilege(Acl::ACTION_DELETE);
        MeEvent::model()->loadModel($id)->delete();
        echo CJSON::encode(array('success' => true, 'redirectUrl' => $this->createUrl('index')));
    }

    public function actionIndex() {

        $this->hasPrivilege();


        $this->render('index', array(
        ));
    }

    public function actionAddOccurrence() {
        if (isset($_POST['event_id']) && isset($_POST['from'])) {
            MeEventOccurrence::model()->addEventOccurrence($_POST['event_id'], $_POST['from']);
        }
    }

    public function actionDynamicLocationProjects() {
        $country_project_id = $_POST['Event']['country_project_id'];

        $data = MeCountryLocationProject::model()->findAll(
                array(
                    'condition' => 'country_project_id=' . $country_project_id,
                )
        );

        $data = CHtml::listData($data, 'id', 'name');
        foreach ($data as $value => $name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
        }
    }

    public function loadModel($id) {
        $model = MeEvent::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
