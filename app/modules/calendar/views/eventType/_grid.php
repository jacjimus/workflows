<?php

$grid_id = 'event-type-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => '<i class="fa fa-calendar"></i>',
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'id',
            'name',
            'description',
            array(
                'name' => 'is_one_day_event',
                'value' => 'MyYiiUtils::decodeBoolean($data->is_one_day_event)',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{notif}{update}{delete}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'notif' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-bell fa-2x"></i>',
                        'url' => 'Yii::app()->createUrl("notif/notifTypes/update", array("id" => $data->notif_type_id, UrlManager::GET_PARAM_RETURN_URL => Yii::app()->createUrl($this->grid->owner->route, $this->grid->owner->actionParams)))',
                        'visible' => '$this->grid->owner->showLink("' . SettingsModuleConstants::RES_SETTINGS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'title' => Lang::t('Notification settings'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . EventModuleConstants::RES_EVENT_REMINDERS . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . EventModuleConstants::RES_EVENT_REMINDERS . '", "' . Acl::ACTION_DELETE . '")&&$data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
