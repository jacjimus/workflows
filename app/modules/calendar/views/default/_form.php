<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <fieldset>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'country_project_id', array('class' => 'col-md-3 control-label')); ?>

            <div class="col-md-6">
                <?php if ($model->isNewRecord && !isset($model->country_project_id)): ?>
                    <?php
                    echo CHtml::activeDropDownList($model, 'country_project_id', MeCountryProjectsView::model()->getListData('id', 'country_project_name'), array(
                        'ajax' => array(
                            'type' => 'POST', //get request type
                            'url' => CController::createUrl('dynamicLocationProjects'),
                            'update' => '#' . CHtml::activeId($model, 'location_id'),
                            'class' => 'form-control',
                    )));
                    ?>
                <?php else: ?>
                    <?php echo CHtml::activeDropDownList($model, 'country_project_id', MeCountryProjectsView::model()->getListData('id', 'country_project_name'), array('class' => 'form-control', 'disabled' => 'disabled')); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'location_id', array('class' => 'col-md-3 control-label')); ?>


            <div class="col-md-6">
                <?php if ($model->isNewRecord): ?>
                    <?php
                    $model_all = CHtml::listData(MeCountryLocationProject::model()->findall(), 'id', 'name');
                    $locationdata = !$model->isNewRecord ? CHtml::listData(MeCountryLocationProject::model()->findAll('country_project_id=:t1', array(':t1' => $model->country_project_id)), 'id', 'name') : $model_all;

                    echo $form->dropDownlist($model, 'location_id', $locationdata, array('prompt' => 'Select a Location Project'));
                    ?>
                <?php else: ?>
                    <?php echo CHtml::activeDropDownList($model, 'location_id', MeCountryLocationProject::model()->getListData('id', 'name'), array('class' => 'form-control', 'disabled' => 'disabled')); ?>
                <?php endif; ?>
            </div>

        </div>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 60)); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'person_responsible_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php if ($model->isNewRecord): ?>
                    <?php
                    $level = Yii::app()->user->user_level;

                    $users = UsersView::model()->findAll();

                    $user_data = CHtml::listData($users, 'id', 'name');

                    echo CHtml::activeDropDownList($model, 'person_responsible_id', $user_data, array('class' => 'form-control'));
                    ?>
                <?php else: ?>
                    <?php echo CHtml::activeDropDownList($model, 'person_responsible_id', UsersView::model()->getListData('id', 'name'), array('class' => 'form-control', 'disabled' => 'disabled')); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'supervisor_id', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php if ($model->isNewRecord): ?>
                    <?php
                    $users = UsersView::model()->findAll();

                    $user_data = CHtml::listData($users, 'id', 'name');

                    echo CHtml::activeDropDownList($model, 'supervisor_id', $user_data, array('class' => 'form-control'));
                    ?>
                <?php else: ?>
                    <?php echo CHtml::activeDropDownList($model, 'supervisor_id', UsersView::model()->getListData('id', 'name'), array('class' => 'form-control', 'disabled' => 'disabled')); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'rows' => 2, 'maxlength' => 100)); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'initial_start_date', array('label' => Lang::t('Date'), 'class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <div class="input-group">
                    <?php echo CHtml::activeTextField($model, 'initial_start_date', array('class' => 'form-control show-datepicker')); ?>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'repeated', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php echo CHtml::activeDropDownList($model, 'repeated', Event::repeatedOptions(), array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group hidden ">
            <div class="checkbox">
                <label>
                    <?php echo CHtml::activeCheckBox($model, 'is_active', array()); ?>&nbsp;<?php echo Event::model()->getAttributeLabel('is_active') ?>
                </label>
            </div>
        </div>
        <?php if (!$model->isNewRecord): ?>

            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'status', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                    <?php echo CHtml::activeDropDownList($model, 'status', MeEvent::statusOptions(), array('class' => 'form-control')); ?>
                </div>
            </div>
        <?php endif; ?>
    </fieldset>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create Activity' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
