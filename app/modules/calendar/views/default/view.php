<div class="row">
    <div class="col-md-3">
        <p><a class="btn btn-default btn-sm btn-block show_modal_form" href="<?php echo $this->createUrl('create') ?>"><i class="fa fa-calendar"></i> <?php echo Lang::t('Add Activity Item') ?></a></p>
        <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
            <p><a class="btn btn-default btn-sm btn-block" href="<?php echo $this->createUrl('eventType/index') ?>"><i class="fa fa-cog"></i> <?php echo Lang::t('Event Types') ?></a></p>
        <?php endif; ?>
        <p><a class="btn btn-default btn-sm btn-block" href="<?php echo Yii::app()->createUrl('notif/notifTypes/update', array('id' => EventType::NOTIF_EVENTS_REMINDER, UrlManager::GET_PARAM_RETURN_URL => Yii::app()->createUrl($this->route, $this->actionParams))) ?>"><i class="fa fa-bell"></i> <?php echo Lang::t('Notification Settings') ?></a></p>
        <div class="well well-sm" id="event-container">
            <form>
                <fieldset>
                    <ul id="external-events" class="list-unstyled">
                        <?php foreach (MeEvent::model()->getEvents() as $r): ?>
                            <li>
                                <span class="<?php echo $r['color_class'] ?> draggable-event" data-description="<?php echo CHtml::encode($r['description']) ?>" data-event-id="<?php echo $r['id'] ?>" data-ajax-url="<?php echo $this->createUrl('addOccurrence'); ?>">
                                    <?php echo CHtml::encode($r['name']) ?>
                                </span>
                                <p class="manage-event hidden">
                                    <a class="text-notice" href="<?php echo $this->createUrl('view', array('id' => $r['id'])) ?>"><?php echo Lang::t('View') ?> <i class="fa fa-eye"></i></a>
                                    <a class="text-success show_modal_form" href="<?php echo $this->createUrl('update', array('id' => $r['id'])) ?>"><?php echo Lang::t('Edit') ?> <i class="fa fa-edit"></i></a>
                                    <a class="text-danger delete-event" href="javascript:void(0)" data-ajax-url="<?php echo $this->createUrl('delete', array('id' => $r['id'])) ?>" data-confirm-message="<?php echo Lang::t('DELETE_CONFIRM') ?>"><?php echo Lang::t('Delete') ?> <i class="fa fa-trash-o"></i></a>
                                </p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </fieldset>
            </form>
        </div>
        <div id="event-form-container">
            <p class="text-center hidden" id="event-form-loading"><i class="fa fa-spinner fa-spin fa-5x text-warning"></i></p>
        </div>
    </div>

    <div class="col-md-9">
        <div class="well well-light " id="printable">
            <div class="row">
                <div class="col-sm-10">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-fw fa-support"></i> <?php echo CHtml::encode($this->pageTitle) ?>
                    </h1>
                </div>
                <div class="col-sm-2 padding-top-10">
                    <a class="btn btn-danger pull-right"   href="<?php echo $this->createUrl('index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
                </div>
            </div>
            <div class="panel panel-primary">
                <?php if ($this->showLink(MeModuleConstants::RES_ACTIVITIES, Acl::ACTION_CREATE)): ?><a class="btn btn-primary margin-top-5 pull-right show_modal_form " href="<?php echo Yii::app()->createUrl('me/projectActivities/create', array('id' => $model->id)) ?>"><i class="fa fa-plus-circle fa-edit"></i> <?php echo Lang::t('Update Activity Occurrence') ?></a><?php endif; ?>
                <input type="button" id="printer"  class="btn btn-primary margin-top-5 pull-right fa fa-print" onclick="printDiv('printable')" class="fa fa-print" value="Print Report">
                <div class="panel-heading">

                    <h3 class="panel-title"><?php echo Lang::t('|') ?></h3>

                </div>
                <?php
                $this->widget('zii.widgets.CDetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        array(
                            'name' => 'name',
                            'filter' => FALSE
                        ),
                        array(
                            'name' => 'country_project_id',
                            'filter' => false,
                            'value' => $model->countryProject->country_project_name
                        ),
                        array(
                            'name' => 'location_id',
                            'filter' => false,
                            'value' => $model->location->location->name
                        ),
                        array(
                            'name' => 'supervisor_id',
                            'filter' => false,
                            'value' => $model->supervisor->name
                        ),
                        array(
                            'name' => 'person_responsible_id',
                            'filter' => false,
                            'value' => $model->personResponsible->name
                        ),
                        array(
                            'name' => 'status',
                            'value' => CHtml::tag('span', array('class' => $model->getStatusLabel()), $model->status),
                            'type' => 'raw',
                        ),
                        array(
                            'name' => 'initial_start_date',
                            'value' => MyYiiUtils::formatDate($model->initial_start_date, "jS M Y"),
                            'filter' => FALSE,
                        ),
                        array(
                            'name' => 'initial_end_date',
                            'value' => MyYiiUtils::formatDate($model->initial_end_date, "jS M Y"),
                            'filter' => FALSE,
                        ),
                        array(
                            'name' => 'actual_date',
                            'value' => MyYiiUtils::formatDate($model->actual_date, "jS M Y"),
                            'filter' => FALSE,
                            'visible' => $model->status == MeEvent::STATE_COMPLETED ? TRUE : FALSE,
                        ),
                        'description',
                    ),
                ));
                ?>
            </div>
        </div>

    </div>

</div>
<!-- end widget -->
<?php
$events = CJSON::encode(MeEventOccurrence::model()->getCalendarEvents());
Yii::app()->clientScript
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/fullcalendar/jquery.fullcalendar.min.js', CClientScript::POS_END)
        ->registerScript('event.default.index', "EventModule.Event.init(" . $events . ");")
?>