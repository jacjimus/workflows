<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'install-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model, 'upload_file', array('class' => 'col-md-3 control-label')); ?>
            <div class="col-md-6">
                <?php echo CHtml::activeFileField($model, 'upload_file', array('class' => 'form-control', 'maxlength' => 128)); ?>
            </div>
        </div>

    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl(Yii::app()->createUrl('default/index')) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" <?php echo!$this->showLink($this->resource, Acl::ACTION_UPDATE) ? 'disabled="disabled"' : null ?> type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Upload') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
