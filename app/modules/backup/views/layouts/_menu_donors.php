
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><i class="fa fa-bars"></i> <?php echo Lang::t('Donors Menu') ?></div>
    <!-- List group -->
    <div class="list-group">
        <?php if ($this->showLink(MeModuleConstants::RES_PROJECT_DONORS)): ?><a href="<?php echo Yii::app()->createUrl('me/donors/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_PROJECT_DONORS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Donors') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_PROJECT_DONATIONS)): ?><a href="<?php echo Yii::app()->createUrl('me/projectDonations/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_PROJECT_DONATIONS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Donations') ?></a><?php endif; ?>
    </div>

</div>
