
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><i class="fa fa-bars"></i> <?php echo Lang::t('M&amp;E') ?></div>
    <!-- List group -->
    <div class="list-group">
        <?php if ($this->showLink(MeModuleConstants::RES_PROGRAM)): ?><a href="<?php echo Yii::app()->createUrl('me/programs/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_PROGRAM ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Programs') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_PROJECTS)): ?><a href="<?php echo Yii::app()->createUrl('me/projects/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_PROJECTS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Projects') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_COUNTRY_PROJECTS)): ?><a href="<?php echo Yii::app()->createUrl('me/countryProjects/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_COUNTRY_PROJECTS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Country Projects') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_STRATEGIC_OBJECTIVES)): ?><a href="<?php echo Yii::app()->createUrl('me/strategicObjectives/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_STRATEGIC_OBJECTIVES ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Strategic Objectives') ?></a><?php endif; ?>

        <?php if ($this->showLink(MeModuleConstants::RES_BUDGETS)): ?><a href="<?php echo Yii::app()->createUrl('me/projectBudgets/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_BUDGETS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Project Budgets') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_INDICATORS)): ?><a href="<?php echo Yii::app()->createUrl('me/indicators/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_INDICATORS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Indicators') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_ACTIVITIES)): ?><a href="<?php echo Yii::app()->createUrl('me/projectActivities/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_ACTIVITIES ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Project Activities') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_ACTIVITIES)): ?><a href="<?php echo Yii::app()->createUrl('Event/default/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_ACTIVITY_CALENDER ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Activities Calendar') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_PROJECT_EXPENDITURE)): ?><a href="<?php echo Yii::app()->createUrl('me/projectExpenditure/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_PROJECT_EXPENDITURE ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Project Expenses') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_INDICATOR_TRANSACTIONS)): ?><a href="<?php echo Yii::app()->createUrl('me/indicatorTransactions/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_INDICATOR_TRANSACTIONS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Indicators Update') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_REPORTS)): ?><a href="<?php echo Yii::app()->createUrl('me/reports/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_REPORTS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Reports') ?></a><?php endif; ?>

    </div>

</div>
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-bars"></i> <?php echo Lang::t('M&amp;E Settings') ?></div>
    <!-- List group -->
    <div class="list-group">
        <?php if ($this->showLink(MeModuleConstants::RES_COUNTRY)): ?><a href = "<?php echo Yii::app()->createUrl('me/country/index') ?>" class = "list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_COUNTRY ? ' active' : '' ?>"><i class = "fa fa-angle-double-right"></i> <?php echo Lang::t('M&amp;E Countries') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_COUNTRY_LOCATIONS)): ?><a href="<?php echo Yii::app()->createUrl('me/countryLocations/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_COUNTRY_LOCATIONS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Country Locations') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_ACTIVITY_TYPES)): ?><a href="<?php echo Yii::app()->createUrl('me/activityTypes/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_ACTIVITY_TYPES ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Activity Types') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_INDICATOR_TYPES)): ?><a href="<?php echo Yii::app()->createUrl('me/indicatorTypes/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_INDICATOR_TYPES ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Indicator Types') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_EXPENSE_ITEMS)): ?><a href="<?php echo Yii::app()->createUrl('me/expenseItems/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_EXPENSE_ITEMS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Expense Items') ?></a><?php endif; ?>
        <?php if ($this->showLink(MeModuleConstants::RES_BUDGETS_PERIODS)): ?><a href="<?php echo Yii::app()->createUrl('me/budgetPeriod/index') ?>" class="list-group-item<?php echo $this->activeTab === MeModuleConstants::TAB_BUDGETS_PERIODS ? ' active' : '' ?>"><i class="fa fa-angle-double-right"></i> <?php echo Lang::t('Budget Periods') ?></a><?php endif; ?>

    </div>
</div>