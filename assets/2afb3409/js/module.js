/*
 *Module based js functions
 *@auuthor Fred <mconyango@gmail.com>
 */
var UsersModule = {
        Roles: {
                init: function() {
                        'use strict';
                        var $this = this;
                        $this.toggleCheckAll();
                },
                toggleCheckAll: function() {
                        $('button.my-select-all').on('click', function(event) {
                                event.preventDefault();
                                var checkbox = $('input:checkbox.my-roles-checkbox')
                                        , isChecked = checkbox.is(':checked');
                                if (isChecked) {
                                        checkbox.prop('checked', false);
                                        $(this).text('Uncheck all');
                                } else {
                                        checkbox.prop('checked', true);
                                        $(this).text('Check all');
                                }
                        });
                }
        },
        User: {
                init: function() {
                        'use strict';
                },
                initForm: function() {
                        'use strict';
                        var $this = this;
                        $this.toggle_role();
                },
                toggle_role: function() {
                        var selector = '#Users_user_level'
                                , toggle_role = function(e) {
                                        var role_selector = '#Users_role_wrapper';
                                        if ($(e).val() === $(e).data('show-role')) {
                                                $(role_selector).removeClass('hidden');
                                        }
                                        else {
                                                $(role_selector).addClass('hidden');
                                        }
                                };

                        //on page load
                        toggle_role(selector);
                        //on change
                        $(selector).on('change', function() {
                                toggle_role(this);
                        });
                }
        },
        UserLevels: {},
};


