/*
 *Module based js functions
 *@auuthor Fred <mconyango@gmail.com>
 */
var NotifModule = {
    NotifTypes: {
        initForm: function() {
            'use strict';
            var $this = this;
            $this.toggle_email_template();
            $this.toggle_notify_users();
        },
        toggle_email_template: function() {
            var selector = '#NotifTypes_send_email'
                    , email_temp_wrapper = $('#email_template_wrapper')
                    , toggle_email_template = function(e) {
                        if ($(e).is(':checked')) {
                            email_temp_wrapper.removeClass('hidden');
                        }
                        else {
                            email_temp_wrapper.addClass('hidden');
                        }
                    };
            //on page load
            toggle_email_template(selector);
            //click event
            $(selector).on('click', function() {
                toggle_email_template(this);
            });
        },
        toggle_notify_users: function() {
            var selector = '#NotifTypes_notify'
                    , toggle_notify_users = function(e) {
                        var val = $(e).val()
                                , show_users = $(e).data('show-users')
                                , show_users_wrapper = $('#notify_users');
                        if (val === show_users) {
                            show_users_wrapper.removeClass('hidden');
                        }
                        else {
                            show_users_wrapper.addClass('hidden');
                        }
                    };
            //on page load
            toggle_notify_users(selector);
            //on change
            $(selector).on('change', function() {
                toggle_notify_users(this);
            });
        },
    },
    Notif: {
        options: {
            checkDelay: 30000, //30 secs
            selector: '#activity',
            check_notif_url: null,
            mark_as_seen_url: null,
            mark_as_read_url: null,
            mark_all_as_read_id: 'mark_all_notif_as_read',
            notif_item_selector: '.notification-body .notif-item',
            refresh_notif_selector: '#refresh_notif',
        },
        init: function(options) {
            'use strict';
            var $this = this;
            $this.options = $.extend({}, $this.options, options || {});

            if (MyUtils.empty($this.options.check_notif_url)) {
                $this.options.check_notif_url = $($this.options.selector).data('check-notif-url');
            }
            if (MyUtils.empty($this.options.mark_as_seen_url)) {
                $this.options.mark_as_seen_url = $($this.options.selector).data('mark-as-seen-url');
            }
            if (MyUtils.empty($this.options.mark_as_read_url)) {
                $this.options.mark_as_read_url = $($this.options.selector).data('mark-as-read-url');
            }
            $this.show_notification();
            $this.get_notification();
            $this.mark_all_as_read();
            $this.mark_as_read();
            $this.refresh_notif();
        },
        show_notification: function() {
            var $this = this
                    , selector = $this.options.selector
                    , mark_as_seen = function() {
                        var url = $(selector).data('mark-as-seen-url');
                        $.ajax({
                            type: "GET",
                            url: url,
                            dataType: 'json'
                        });
                    };
            $(selector).on('click', function(event) {
                var $elem = $(this);

                if ($elem.find('.badge').hasClass('bg-color-red')) {
                    $elem.find('.badge').removeClassPrefix('bg-color-');
                    $elem.find('.badge').hide();
                }

                if (!$elem.next('.ajax-dropdown').is(':visible')) {
                    $elem.next('.ajax-dropdown').fadeIn(150);
                    $elem.addClass('active');
                } else {
                    $elem.next('.ajax-dropdown').fadeOut(150);
                    $elem.removeClass('active')
                }
                mark_as_seen();
                event.preventDefault();
            });
        },
        get_notification: function() {
            var $this = this
                    , container = $('.ajax-notifications')
                    , url = $($this.options.selector).data('check-notif-url');

            $this.loadURL(url, container);
        },
        loadURL: function(url, container, show_loading) {
            var $this = this
                    , show_bubble = function(unseen) {
                        var bubble = $('#activity > .badge');
                        if (parseInt(unseen) > 0) {
                            bubble.text(unseen).addClass("bg-color-red bounceIn animated");
                            bubble.removeClass('hidden');
                        }
                        else {
                            bubble.addClass('hidden');
                        }
                    },
                    update_total_notif = function(total) {
                        $('span.total-notif').text('(' + total + ')');
                    };
            if (typeof show_loading === 'undefined')
                show_loading = true;
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                cache: true, // (warning: this will cause a timestamp and will call the request twice)
                beforeSend: function() {
                    // cog placed
                    if (show_loading)
                        container.html('<h1 class="text-center" style="margin-top:50px;"><i class="fa fa-spinner fa-spin"></i> Loading...</h1>');
                },
                success: function(data) {
                    if (show_loading) {
                        container.css({
                            opacity: '0.0'
                        }).html(data.html).delay(50).animate({
                            opacity: '1.0'
                        }, 300);
                    } else {
                        container.html(data.html);
                    }
                    show_bubble(data.unseen);
                    update_total_notif(data.total);

                    setTimeout(function() {
                        $this.loadURL(url, container, false);
                    }, $this.options.checkDelay);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                },
                async: false
            });
        },
        mark_as_read: function() {
            var $this = this
                    , selector = $this.options.notif_item_selector + '.unread a'
                    , mark_as_read = function(elem) {
                        var notif_item = $(elem).closest('.notif-item')
                                , url = notif_item.data('mark-as-read-url')
                                , target_url = $(elem).attr('href');
                        $.ajax({
                            type: "GET",
                            url: url,
                            dataType: 'json',
                            complete: function() {
                                notif_item.removeClass('unread');
                                MyUtils.reload(target_url);
                            }
                        });
                    };
            $('#header').on('click', selector, function(event) {
                mark_as_read(this);
                return false;
            });
        },
        mark_all_as_read: function() {
            var $this = this
                    , mark_all_as_read = function(elem) {
                        var url = $(elem).data('ajax-url');
                        $.ajax({
                            type: "GET",
                            url: url,
                            dataType: 'json',
                            success: function(response) {
                                $($this.options.notif_item_selector).removeClass('unread');
                            }
                        });
                    };
            $('#' + $this.options.mark_all_as_read_id).on('click', function(event) {
                mark_all_as_read(this);
                event.preventDefault();
            });
        },
        refresh_notif: function() {
            var $this = this
                    , selector = $this.options.refresh_notif_selector
                    , refresh_notif = function() {
                        $this.get_notification();
                    };
            $(selector).on('click', function() {
                var btn = $(this)
                btn.button('loading');
                refresh_notif();
                btn.button('reset');
            });
        },
    }
};


