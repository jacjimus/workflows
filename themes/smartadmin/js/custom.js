/*
 * Custom page setup js bootstrap
 * @author Fred <mconyango@gmail.com>
 */
jQuery(function($) {
    MyYiiApp.init();
});

//common functions
var MyYiiApp = {
    init: function() {
        'use strict';
        try {
            var $this = this;
            $this.theme_setup();
            $this.change_status();
            $this.grid_delete_multiple();
            $this.post_selected_grid_ids();
            $this.update_grid_view();
            $this.link_table_rows();
            $this.ajax_delete();
            $this.show_colorbox();
            $("body").delegate("input[type='text'].show-datepicker,.show-datepicker input[type='text']", "focusin", function() {
                $this.show_datepicker(this);
            });
            //timeago
            $("time.timeago").timeago();
            //bs modal form
            MyModalForm.init();
            $this.activate_tabs();
            //initialize notification
            NotifModule.Notif.init();

        } catch (e) {
            console.log(e);
        }
    },
    link_table_rows: function() {
        var selector = 'tr.linked-table-row'
                , href = $(selector).data('href');
        //onclick
        $('body').on('click', selector, function() {
            MyUtils.reload(href);
        });

    },
    ajax_delete: function() {
        var ajax_delete = function(e) {
            var url = $(e).data('ajax-url');
            var confirm_msg = $(e).data('confirm-message');
            if (MyUtils.empty(confirm_msg)) {
                confirm_msg = 'Are you sure you want to delete this item?';
            }
            BootstrapDialog.confirm(confirm_msg, function(result) {
                if (result) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        success: function(data) {
                            if (!MyUtils.empty(data.redirect_url))
                                MyUtils.reload(data.redirect_url);
                            else
                                MyUtils.reload();
                        }
                        ,
                        beforeSend: function() {
                            MyUtils.startBlockUI('Please wait...');
                        }
                        ,
                        complete: function() {
                            MyUtils.stopBlockUI();
                        }
                        ,
                        error: function(XHR) {
                            var message = XHR.responseText;
                            MyUtils.showAlertMessage(message, 'error');
                            return false;
                        }
                    });
                }
            });
        };
        //event
        $('.ajax-delete').on('click', function(e) {
            e.preventDefault();
            ajax_delete(this);
        });

    },
    change_status: function() {
        var post_request = function(e) {
            var url = $(e).data('ajax-url');
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                success: function(data) {
                    if (!MyUtils.empty(data.redirect_url))
                        MyUtils.reload(data.redirect_url);
                    else
                        MyUtils.reload();
                }
                ,
                beforeSend: function() {
                    MyUtils.startBlockUI('Please wait...');
                }
                ,
                complete: function() {
                    MyUtils.stopBlockUI();
                }
                ,
                error: function(XHR) {
                    var message = XHR.responseText;
                    MyUtils.showAlertMessage(message, 'error');
                }
            });
        };
        //event
        $('.change_status').on('click', function() {
            post_request(this);
            return false;
        });
    },
    grid_delete_multiple: function() {
        $('.grid_delete_multiple').on('click', function() {
            MyUtils.GridView.deleteMultiple(this);
            return false;
        });
    },
    post_selected_grid_ids: function() {
        $('.post_selected_grid_ids').on('click', function() {
            var grid_id = $(this).data('grid_id')
                    , selected_ids = MyUtils.GridView.getSelectedIds(grid_id);
            if (MyUtils.empty(selected_ids))
                return false;
            var url = MyUtils.addParameterToURL($(this).data('ajax-url'), 'ids', selected_ids)
                    , is_colorbox = $(this).data('colorbox');
            if (typeof is_colorbox === 'undefined')
                MyUtils.reload(url);
            else
                MyUtils.showColorbox(url);
        });
    },
    update_grid_view: function() {
        'use strict';
        var update_grid = function(e) {

            var grid_id = $(e).data('grid_id')
                    , url = $(e).data('ajax-url')
                    , confirm_msg = $(e).data('confirm');
            if (!MyUtils.empty(confirm_msg)) {
                BootstrapDialog.confirm(confirm_msg, function(result) {
                    if (result) {
                        MyUtils.GridView.submitGridView(grid_id, url);
                    }
                });
            }
            else {
                MyUtils.GridView.submitGridView(grid_id, url);
            }
        };

        $('body').on('click', '.my-update-grid', function(e) {
            e.preventDefault();
            return update_grid(this);
        });
    },
    show_datepicker: function(selector) {
        $(selector).datepicker({
            dateFormat: 'yy-mm-dd hh:ii:ss',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
        });
    },
    show_colorbox: function() {
        $('body').on('click', 'a.show-colorbox', function() {
            MyUtils.showColorbox($(this).attr('href'), true);
            return false;
        });
    },
    theme_setup: function() {
        pageSetUp();
        //settings
        $('#sadmin-setting').click(function() {
            $('#ribbon .sadmin-options').toggleClass('activate');
        });
    },
    activate_tabs: function() {
        //activate tabs
        $('ul.my-nav li>a').each(function() {
            if ($($(this))[0].href == String(window.location)) {
                $(this).parent().addClass('active');
            }
        });
        //activate list-group links
        $('div.my-list-group>a').each(function() {
            if ($($(this))[0].href == String(window.location)) {
                $(this).addClass('active');
            }
        });
    }
}



