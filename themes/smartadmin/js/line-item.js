/**
 * LineItem Script
 * @author Fred <mconyango@gmail.com>
 * Timestamp: 2nd Jun 2014 at 1803hr
 */

var MyLineItem = {
    Head: {
        optionsDefault: {
            formContainer: 'line_item_form_wrapper',
            itemsTableCssId: undefined,
            addNewRowOnSave: true,
            headIdInputCssId: '',
            addNewRowSelector: undefined,
            cancelSelector: '#cancel_line_item_button',
            submitSelector: '#submit_line_item_button',
            makeorderSelector: '#makeorder_line_item_button',
            notifSelector: '#line_items_notif_wrapper',
            headInputsWrapper: 'line_items_head_inputs_wrapper',
            autoAddRowOnSaveSelector: '#auto_add_new_row_on_save',
            beforeFinish: function(e) {
            },
            afterFinish: function($this, response) {
                $('#' + $this.options.formContainer).html(response.html);
                MyUtils.showAlertMessage(response.message, 'success', $this.options.notifSelector);
            },
            
            beforeMakeorder: function(e) {
            },
            afterMakeorder: function($this, response) {
                $('#' + $this.options.formContainer).html(response.html);
                MyUtils.showAlertMessage(response.message, 'success', $this.options.notifSelector);
            },
        },
        options: {},
        init: function(options) {
            'use strict';
            var $this = this;
            $this.options = $.extend({}, $this.optionsDefault, options || {});
            $this.addRow();
            $this.cancel();
            $this.finish();
            $this.makeorder();
        },
        addRow: function() {
            var $this = this;
            var addRow = function(e) {
                var url = $(e).data('ajax-url');
                var index = $(MyLineItem.Row.options.rowCssSelector).length + 1;
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'index=' + index,
                    success: function(html) {
                        var container = $('#' + $this.options.itemsTableCssId).find('tbody');
                        container.append(html);
                    }
                });
            };
            //onclick
            $($this.options.addNewRowSelector).off('click').on('click', function(e) {
                e.preventDefault();
                addRow(this);
            });
        },
        cancel: function() {
            var $this = this;
            var cancel = function(e) {
                var url = $(e).data('ajax-url')
                        , id = $('#' + $this.options.headIdInputCssId).val();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'id=' + id,
                    success: function(html) {
                        $('#' + $this.options.formContainer).html(html);
                    }
                });
            };
            //on click
            $($this.options.cancelSelector).off('click').on('click', function(e) {
                e.preventDefault();
                cancel(this);
            });
        },
        finish: function() {
            var $this = this;
            var submit = function(e) {
                $this.options.beforeFinish(e);
                var url = $(e).data('ajax-url')
                        , data = $('#' + $this.options.formContainer).find('form').serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            $this.options.afterFinish($this, response);
                        } else {
                            MyUtils.display_model_errors(response.message, false, true);
                        }
                    },
                    beforeSend: function() {
                        MyUtils.startBlockUI();
                    },
                    complete: function() {
                        MyUtils.stopBlockUI();
                    },
                    error: function(XHR) {
                        console.log(XHR.responseText);
                    }
                });
            };
            //on click
            $($this.options.submitSelector).off('click').on('click', function(e) {
                e.preventDefault();
                submit(this);
            });
           
        },
        makeorder: function() {
        var $this = this;
        var submit = function(e) {
            $this.options.beforeMakeorder(e);
            var url = $(e).data('ajax-url')
                    , data = $('#' + $this.options.formContainer).find('form').serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        $this.options.afterMakeorder($this, response);
                    } else {
                        MyUtils.display_model_errors(response.message, false, true);
                    }
                },
                beforeSend: function() {
                    MyUtils.startBlockUI();
                },
                complete: function() {
                    MyUtils.stopBlockUI();
                },
                error: function(XHR) {
                    console.log(XHR.responseText);
                }
            });
        };
        //on click
        $($this.options.makeorderSelector).off('click').on('click', function(e) {
            e.preventDefault();
            submit(this);
        });
    },
        

    },
    Row: {
        optionsDefault: {
            rowIdInputCssClass: undefined,
            rowParentIdInputCssClass: undefined,
            saveCssClass: 'save-line-item',
            deleteCssClass: 'delete-line-item',
            rowCssSelector: 'tr.line-item',
            beforeSave: function(rowTag) {
            },
            afterSave: function(rowTag, response) {
            },
            beforeDelete: function(rowTag) {
            },
            afterDelete: function(rowTag, response) {
            }
        },
        options: {},
        init: function(options) {
            'use strict';
            var $this = this;
            $this.options = $.extend({}, $this.optionsDefault, options || {});
            $this.deleteRow();
            $this.saveRow();
        },
        saveRow: function() {
            var $this = this;
            var input_error_class = 'my-form-error';
            var show_error = function(rowTag, input_class) {
                rowTag.find('.' + input_class).addClass(input_error_class);
                rowTag.addClass('bg-danger');
            };
            var hide_error = function(rowTag) {
                rowTag.find('.' + input_error_class).removeClass(input_error_class);
                rowTag.removeClass('bg-danger');
            };
            var saved_css_class = 'text-success'
                    , unsaved_css_class = 'text-warning';
            var mark_as_saved = function(rowTag) {
                rowTag.find('.' + $this.options.saveCssClass).removeClass(unsaved_css_class).addClass(saved_css_class);
            };
            var post = function(e) {
                //event callback
                var rowTag = $(e).closest('tr');
                $this.options.beforeSave(rowTag);
                //set the parent_id of the row
                var row_parent_id_selector = '.' + $this.options.rowParentIdInputCssClass;
                var head_id_input = $('#' + MyLineItem.Head.options.headIdInputCssId);
                if (MyUtils.empty(rowTag.find(row_parent_id_selector).val())) {
                    $(row_parent_id_selector).val(head_id_input.val());
                }
                var url = rowTag.data('save-url')
                        , data = rowTag.find('input,select,textarea').serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            //event callback
                            $this.options.afterSave(rowTag, response);
                            if (!MyUtils.empty(response.data.parent_id) && MyUtils.empty(head_id_input.val())) {
                                head_id_input.val(response.data.parent_id);
                            }
                            hide_error(rowTag);
                            var id_input_selector = '.' + $this.options.rowIdInputCssClass;
                            var addNewRowOnSave = $(MyLineItem.Head.options.autoAddRowOnSaveSelector).is(':checked');
                            if (addNewRowOnSave && MyUtils.empty(rowTag.find(id_input_selector).val())) {
                                $(MyLineItem.Head.options.addNewRowSelector).trigger('click');
                            }
                            rowTag.find(id_input_selector).val(response.data.id);
                            mark_as_saved(rowTag);
                        }
                        else {
                            console.log(response.message);
                            hide_error(rowTag);
                            //show error
                            var jsonData = $.parseJSON(response.message);
                            $.each(jsonData, function(i) {
                                show_error(rowTag, i);
                            });
                            MyUtils.display_model_errors(response.message, false, true);
                        }
                    },
                    error: function(XHR) {
                        console.log(XHR.responseText);
                    }
                });
            };
            //on click
            $('.' + $this.options.saveCssClass).off('click').on('click', function(e) {
                e.preventDefault();
                post(this);
            });
        },
        deleteRow: function() {
            'use strict';
            var $this = this;
            var deleteRow = function(e) {
                var rowTag = $(e).closest('tr');
                $this.options.beforeDelete(rowTag);
                var url = rowTag.data('delete-url')
                        , id = rowTag.find('.' + $this.options.rowIdInputCssClass).val();
                if (MyUtils.empty(id)) {
                    rowTag.remove();
                    return false;
                }
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'id=' + id,
                    dataType: 'json',
                    success: function(response) {
                        rowTag.remove();
                        $this.options.afterDelete(rowTag, response);
                    }
                });
            };
            //on click
            $('.' + $this.options.deleteCssClass).off('click').on('click', function(e) {
                e.preventDefault();
                var $this2 = this;
                var confirm_msg = $($this2).data('delete-confirm');
                if (MyUtils.empty(confirm_msg)) {
                    deleteRow($this2);
                } else {
                    BootstrapDialog.confirm(confirm_msg, function(result) {
                        if (result) {
                            deleteRow($this2);
                        }
                    })
                }
            });
        },
    }

};


