/*
 *  Handles the modal form submit
 */

var MyModalForm = {
    options: {
        notif_id: 'my-modal-notif',
        form_id: 'my-modal-form',
        success_class: 'alert-success',
        error_class: 'alert-danger',
    },
    init: function(options) {
        'use strict';
        var $this = this
                , modal_id = 'my_bs_modal';
        $this.options = $.extend({}, $this.options, options || {});

        $('#content').on('click', '.show_modal_form', function(e) {
            e.preventDefault();
            var modal_size = $(this).data('modal-size');
            var refresh = $(this).data('refresh');
            if (!MyUtils.empty(modal_size)) {
                $('#' + modal_id).find('.modal-dialog').addClass(modal_size);
            }
            var grid_id = $(this).data('grid-id');
            var url = $(this).attr('href');
            var modal = $('#' + modal_id);
            modal
                    .on('shown.bs.modal', function() {
                        $('#' + $this.options.form_id).attr('data-grid-id', grid_id);
                    })
                    .on('hidden.bs.modal', function() {
                        if (!MyUtils.empty(refresh)) {
                            MyUtils.reload();
                        } else {
                            $(this).removeData('bs.modal');
                        }
                    })
                    .on('show.bs.modal', function() {
                        var loading = '<div class="row"><div class="col-md-12"><div class="content-loading"><i class="fa fa-spinner fa-5x fa-spin text-warning"></i></div></div></div>';
                        $('#' + modal_id).find('.modal-content').html(loading);
                    })
                    .on('loaded.bs.modal', function() {
                    })
                    .modal({
                        remote: url,
                        backdrop: 'static',
                        refresh: true,
                    });
        });
        $('#' + modal_id).on("click", '#' + $this.options.form_id + ' button[type="submit"]', function() {
            var grid_id = $('#' + $this.options.form_id).data('grid-id');
            $this.submit_form(this, grid_id);
            return false;
        });
    }
    ,
    submit_form: function(e, grid_id) {
        var $this = this
                , form = $('#' + $this.options.form_id)
                , data = form.serialize()
                , action = form.attr('action')
                , method = form.attr('method') || 'POST'
                , originalButtonHtml = $(e).html();

        $.ajax({
            type: method,
            url: action,
            data: data,
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    var message = '<i class=\"fa fa-check\"></i> ';
                    message += response.message;
                    MyUtils.showAlertMessage(message, 'success', '#' + $this.options.notif_id);
                    if (response.forceRedirect) {
                        MyUtils.reload(response.redirectUrl, 1000);
                    }
                    else if (MyUtils.empty(grid_id)) {
                        if (typeof response.redirectUrl !== 'undefined' && response.redirectUrl) {
                            MyUtils.reload(response.redirectUrl, 1000);
                        }
                    } else {
                        setTimeout(function() {
                            $('#my_bs_modal').modal('hide');
                            MyUtils.GridView.updateGridView(grid_id);
                        }, 1000);
                    }
                }
                else {
                    MyUtils.display_model_errors(response.message, '#' + $this.options.notif_id);
                }
            },
            beforeSend: function() {
                $(e).attr('disabled', 'disabled').html('Please wait....');
            },
            complete: function() {
                $(e).html(originalButtonHtml).removeAttr('disabled');
            },
            error: function(XHR) {
                MyUtils.showAlertMessage(XHR.responseText, 'error', '#' + $this.options.notif_id);
            }
        });
    }
};